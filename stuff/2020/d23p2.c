#include <stdio.h>
#include <stdlib.h>
#define N 1000000
#define R 10000000
int main(int argc, char** argv) {
	int a, c, i, *m, r1, r2, r3; char *s;
	if (argc < 2) fprintf(stderr, "Usage: %s <input>\n", argv[0]), exit(EXIT_FAILURE);
	m = malloc(N * sizeof(int));
	for (i = 0; i < N; i++) m[i] = (i + 1) % N;
	s = argv[1], m[0] = s[0] - '0';
	for (i = 0; s[i] != 0; i++) m[s[i] - '0'] = s[i + 1] == 0 ? i + 2 : s[i + 1] - '0';
	c = m[0];
	for (i = 0; i < R; i++) {
		r1 = m[c], r2 = m[r1], r3 = m[r2], m[c] = m[r3], a = c;
		do a = (a == 0 ? N : a) - 1; while (a == r1 || a == r2 || a == r3);
		m[r3] = m[a], m[a] = r1, c = m[c];
	}
	printf("%lld\n", (long long) (m[1] == 0 ? N : m[1]) * (m[m[1]] == 0 ? N : m[m[1]]));
	free(m);
	return 0;
}
