package com.szendo.aoc.y2017old

object D05 {
    val instr = mutableListOf<Int>(
        0,
        3,
        0,
        1,
        -3
    )
}

fun main() {
    var count = 0
    try {
        var ptr = 0
        while (true) {
            val offset = D05.instr[ptr]
            D05.instr[ptr]++
            ptr += offset
            count++
        }
    } catch (e: IndexOutOfBoundsException) {
        println(count)
    }


}