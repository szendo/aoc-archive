package com.szendo.aoc.y2017old

import com.szendo.aoc.Helper

fun main() {

    Helper.getResourceAsStream("2017/2.txt").bufferedReader().useLines {
        var checksum = 0

        for (line in it) {
            val values = line.split("\t").map(String::toInt)
            val max = values.maxOrNull() ?: 0
            val min = values.minOrNull() ?: 0
            checksum += max - min
        }

        println(checksum)
    }

}