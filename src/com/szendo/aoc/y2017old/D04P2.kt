package com.szendo.aoc.y2017old

fun main() {
    println(D04.passphrases.map { it: String -> it.split(' ').map { x -> x.codePoints().sorted().toArray().asList() } }
        .filter { it.toList().size == it.toSet().size }
        .count())
}