package com.szendo.aoc.y2017old

fun main() {
    println(getDistance(368078))
}

fun getDistance(n: Int): Int {
    var rs = 1
    var r = 1

    while (n > rs + 8 * r) {
        rs += 8 * r
        r++
    }

    return if (n == 1) 0 else (r + Math.abs(r - (n - rs) % (2 * r)))
}