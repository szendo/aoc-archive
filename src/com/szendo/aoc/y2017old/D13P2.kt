package com.szendo.aoc.y2017old

fun main() {
    val firewalls = mapOf(
        0 to 3,
        1 to 2,
        2 to 5,
        4 to 4,
        6 to 6,
        8 to 4,
        10 to 8,
        12 to 8,
        14 to 6,
        16 to 8,
        18 to 6,
        20 to 6,
        22 to 8,
        24 to 12,
        26 to 12,
        28 to 8,
        30 to 12,
        32 to 12,
        34 to 8,
        36 to 10,
        38 to 9,
        40 to 12,
        42 to 10,
        44 to 12,
        46 to 14,
        48 to 14,
        50 to 12,
        52 to 14,
        56 to 12,
        58 to 12,
        60 to 14,
        62 to 14,
        64 to 12,
        66 to 14,
        68 to 14,
        70 to 14,
        74 to 24,
        76 to 14,
        80 to 18,
        82 to 14,
        84 to 14,
        90 to 14,
        94 to 17
    )
    val max = firewalls.keys.maxOf { it }

    var delay = 0
    while (true) {
        var sev = 0
        for (i in 0..max) {
            val pos = getPos(firewalls, i, delay + i)
            if (pos == 0) {
                sev += i * firewalls[i]!! + 1
            }
        }

        if (sev == 0) {
            println(delay)
            return
        }
        delay++
    }

}
