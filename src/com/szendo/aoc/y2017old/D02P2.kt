package com.szendo.aoc.y2017old

import com.szendo.aoc.Helper

fun main() {


    Helper.getResourceAsStream("2017/2.txt").bufferedReader().useLines {
        var sum = 0

        for (line in it) {
            val values = line.split("\t").map(String::toInt)

            for (i in values) {
                values
                    .filter { i % it == 0 && i != it }
                    .forEach { sum += i / it }
            }
        }

        println(sum)
    }

}