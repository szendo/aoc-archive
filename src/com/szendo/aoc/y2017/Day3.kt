package com.szendo.aoc.y2017

import com.szendo.aoc.minus
import com.szendo.aoc.plus
import com.szendo.aoc.x
import com.szendo.aoc.y
import kotlin.math.abs

fun main() {
    val input = 368078

    println("Answer 1: ${answer1(input)}")
    println("Answer 2: ${answer2(input)}")
}

private fun answer1(input: Int): Int {
    val squares = mutableMapOf(1 to (0 to 0))

    var k = 1
    var i = 1
    var coord = 0 to 0
    while (input !in squares) {
        repeat(k) {
            coord += 1 to 0
            squares[++i] = coord
        }
        repeat(k) {
            coord -= 0 to 1
            squares[++i] = coord
        }
        ++k
        repeat(k) {
            coord -= 1 to 0
            squares[++i] = coord
        }
        repeat(k) {
            coord += 0 to 1
            squares[++i] = coord
        }
        ++k
    }

    return squares[input]?.let { abs(it.x) + abs(it.y) }!!
}

private fun answer2(input: Int): Int {
    val squares = mutableMapOf(1 to (0 to 0))
    val sums = mutableMapOf((0 to 0) to 1)

    var k = 1
    var i = 1
    var coord = 0 to 0
    while (true) {
        repeat(k) {
            coord += 1 to 0
            squares[++i] = coord

            (-1..1).sumOf { dx -> (-1..1).sumOf { dy -> sums[coord + (dx to dy)] ?: 0 } }.let {
                if (it > input) return it
                sums[coord] = it
            }
        }
        repeat(k) {
            coord -= 0 to 1
            squares[++i] = coord

            (-1..1).sumOf { dx -> (-1..1).sumOf { dy -> sums[coord + (dx to dy)] ?: 0 } }.let {
                if (it > input) return it
                sums[coord] = it
            }
        }
        ++k
        repeat(k) {
            coord -= 1 to 0
            squares[++i] = coord

            (-1..1).sumOf { dx -> (-1..1).sumOf { dy -> sums[coord + (dx to dy)] ?: 0 } }.let {
                if (it > input) return it
                sums[coord] = it
            }
        }
        repeat(k) {
            coord += 0 to 1
            squares[++i] = coord

            (-1..1).sumOf { dx -> (-1..1).sumOf { dy -> sums[coord + (dx to dy)] ?: 0 } }.let {
                if (it > input) return it
                sums[coord] = it
            }
        }
        ++k
    }
}