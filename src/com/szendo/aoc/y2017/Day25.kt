package com.szendo.aoc.y2017

import com.szendo.aoc.Helper

fun main() {
    var state: Char
    var checksumAfter: Int
    var cursor = 0
    var tape = mutableSetOf<Int>()

    val writeRules: MutableMap<Char, MutableMap<Boolean, Boolean>> = mutableMapOf()
    val moveRules: MutableMap<Char, MutableMap<Boolean, Boolean>> = mutableMapOf()
    val stateRules: MutableMap<Char, MutableMap<Boolean, Char>> = mutableMapOf()

    Helper.getResourceAsStream("2017/25.txt")
        .bufferedReader()
        .useLines { it.toList() }
        .let { lines ->
            state = lines[0].dropLast(1).last()
            checksumAfter = Regex("\\d+").find(lines[1])!!.value.toInt()

            var ruleState = '-'
            var ruleValue = false
            lines.drop(3).forEach { line ->
                if (line.startsWith("In state")) {
                    ruleState = line.dropLast(1).last()
                } else if (line.startsWith("  If the current value")) {
                    ruleValue = line.dropLast(1).last() == '1'
                } else if (line.startsWith("    - Write ")) {
                    writeRules.computeIfAbsent(ruleState) { mutableMapOf() }[ruleValue] =
                        line.split(" ").last().startsWith("1")
                } else if (line.startsWith("    - Move")) {
                    moveRules.computeIfAbsent(ruleState) { mutableMapOf() }[ruleValue] =
                        line.split(" ").last().startsWith("right")
                } else if (line.startsWith("    - Continue")) {
                    stateRules.computeIfAbsent(ruleState) { mutableMapOf() }[ruleValue] = line.dropLast(1).last()
                }
            }
        }

    repeat(checksumAfter) {
        val value = cursor in tape

        if (writeRules[state]!![value]!!) {
            tape.add(cursor)
        } else {
            tape.remove(cursor)
        }

        if (moveRules[state]!![value]!!) {
            cursor++
        } else {
            cursor--
        }

        state = stateRules[state]!![value]!!
    }

    println(tape.size)
}