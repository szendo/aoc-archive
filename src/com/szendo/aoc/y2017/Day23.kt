package com.szendo.aoc.y2017

import com.szendo.aoc.Helper

fun main() {
    val k = Helper.getResourceAsStream("2017/23.txt")
        .bufferedReader()
        .useLines { lines -> lines.first().split(" ")[2].toInt() }


    println("Answer 1: ${answer1(k)}")
    println("Answer 2: ${answer2(k)}")
}

// (2..k).forEach { d -> (2..k).forEach { e -> if (d * e == k) f = 0 } }
private fun answer1(k: Int) = (k - 2) * (k - 2)

private fun answer2(k: Int): Int {
    val start = (k * 100) + 100000
    val end = start + 17000
    val step = 17

    return (start..end step step).count { n -> (2 until n).any { n % it == 0 } }
}