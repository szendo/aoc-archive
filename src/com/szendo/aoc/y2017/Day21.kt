package com.szendo.aoc.y2017

import com.szendo.aoc.Helper

fun main() {
    val rules: Map<List<String>, List<String>> = Helper.getResourceAsStream("2017/21.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.toList().flatMap { line ->
                val (source, target) = line.split(" => ").map { it.split("/") }
                listOf(
                    source to target,
                    source.reversed() to target,
                    source.map { it.reversed() } to target,
                    source.reversed().map { it.reversed() } to target,
                    source.rotateCw() to target,
                    source.rotateCw().reversed() to target,
                    source.rotateCcw() to target,
                    source.rotateCcw().reversed() to target
                )
            }
        }
        .toMap()

    var pattern = listOf(".#.", "..#", "###")

    repeat(5) {
        pattern = pattern.toGrid(if (pattern.size % 2 == 0) 2 else 3).map { it.map { rules[it]!! } }.toImage()
    }

    val answer1 = pattern.sumOf { it.count { c -> c == '#' } }
    println("Answer 1: $answer1")

    repeat(13) {
        pattern = pattern.toGrid(if (pattern.size % 2 == 0) 2 else 3).map { it.map { rules[it]!! } }.toImage()
    }

    val answer2 = pattern.sumOf { it.count { c -> c == '#' } }
    println("Answer 2: $answer2")
}

private fun List<String>.rotateCw() =
    mapIndexed { y, row -> row.mapIndexed { x, _ -> this[lastIndex - x][y] }.joinToString("") }

private fun List<String>.rotateCcw() =
    mapIndexed { y, r -> r.mapIndexed { x, _ -> this[x][r.lastIndex - y] }.joinToString("") }

private fun List<String>.toGrid(gridSize: Int): List<List<List<String>>> {
    return this.chunked(gridSize).map {
        it.map { it.chunked(gridSize) }.let {
            (0..it[0].lastIndex).map { i -> it.map { it[i] } }
        }
    }
}

private fun List<List<List<String>>>.toImage(): List<String> {
    return this.flatMap { (0..it[0].lastIndex).map { i -> it.joinToString("") { it[i] } } }
}