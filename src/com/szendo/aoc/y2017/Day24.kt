package com.szendo.aoc.y2017

import com.szendo.aoc.Helper

fun main() {
    val bridges = Helper.getResourceAsStream("2017/24.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { line ->
                val (p1, p2) = line.split("/").map { it.toInt() }
                BridgeD24(p1, p2, p1 + p2, 1)
            }.toList()
        }

    val answer1 = buildStrongest(BridgeD24(0, 0, 0, 0), bridges).strength
    println("Answer 1: $answer1")
    val answer2 = buildLongest(BridgeD24(0, 0, 0, 0), bridges).strength
    println("Answer 2: $answer2")
}

private fun buildStrongest(bridge: BridgeD24, remaining: List<BridgeD24>): BridgeD24 {
    if (remaining.isEmpty()) {
        return bridge
    }

    return remaining.filter { bridge.p2 == it.p1 || bridge.p2 == it.p2 }
        .map { buildStrongest(bridge + it, remaining.filter { r -> r != it }) }
        .maxByOrNull { it.strength }
        ?: bridge
}

private fun buildLongest(bridge: BridgeD24, remaining: List<BridgeD24>): BridgeD24 {
    if (remaining.isEmpty()) {
        return bridge
    }

    return remaining.filter { bridge.p2 == it.p1 || bridge.p2 == it.p2 }
        .map { buildLongest(bridge + it, remaining.filter { r -> r != it }) }
        .maxWithOrNull(compareBy({ it.length }, { it.strength }))
        ?: bridge
}

private class BridgeD24(val p1: Int, val p2: Int, val strength: Int, val length: Int = 0) {
    operator fun plus(other: BridgeD24): BridgeD24 {
        return when (p2) {
            other.p1 -> BridgeD24(p1, other.p2, strength + other.strength, length + other.length)
            other.p2 -> BridgeD24(p1, other.p1, strength + other.strength, length + other.length)
            else -> throw RuntimeException()
        }
    }
}