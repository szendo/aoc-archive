package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/1.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .split(", ").map { it[0] to it.drop(1).toInt() }

    val answer1 = run {
        var pos = 0 to 0
        var dir = 0 to 1
        input.forEach { (turn, n) ->
            dir = when (turn) {
                'L' -> dir.rotateCCW()
                'R' -> dir.rotateCW()
                else -> error("Invalid turn: $turn")
            }
            pos += n * dir
        }
        pos.distance(0 to 0)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var pos = 0 to 0
        var dir = 0 to 1
        val visited = mutableSetOf(pos)
        input.forEach { (turn, n) ->
            dir = when (turn) {
                'L' -> dir.rotateCCW()
                'R' -> dir.rotateCW()
                else -> error("Invalid turn: $turn")
            }
            repeat(n) {
                pos += dir
                if (!visited.add(pos)) return@run pos.distance(0 to 0)
            }
        }
        error("No location visited twice")
    }
    println("Part 2: $answer2")
}
