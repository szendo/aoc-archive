package com.szendo.aoc.y2016

import com.szendo.aoc.*
import kotlin.math.max
import kotlin.math.min

fun main() {
    val input = Helper.getResourceAsStream("2016/20.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.split("-").let { r -> r[0].toLong()..r[1].toLong() } }

    fun LongRange.overlaps(r: LongRange) =
        this.first <= r.last && this.last >= r.first

    fun LongRange.union(r: LongRange): LongRange =
        min(this.first, r.first)..max(this.last, r.last)

    val answer1 = run {
        input.map { it.last + 1 }
            .filter { input.none { blockRange -> it in blockRange } }
            .minOf { it }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val blockedRanges = mutableSetOf<LongRange>()

        input.sortedBy { it.first }.forEach { blockedRange ->
            val overlappingRange = blockedRanges.firstOrNull { it.overlaps(blockedRange) }
            if (overlappingRange != null) {
                blockedRanges.remove(overlappingRange)
                blockedRanges.add(overlappingRange.union(blockedRange))
            } else {
                blockedRanges.add(blockedRange)
            }
        }

        0x100000000L - blockedRanges.sumOf { it.last - it.first + 1 }
    }
    println("Part 2: $answer2")
}
