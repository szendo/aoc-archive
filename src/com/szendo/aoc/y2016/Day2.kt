package com.szendo.aoc.y2016

import com.szendo.aoc.*
import kotlin.math.max
import kotlin.math.min

fun main() {
    val input = Helper.getResourceAsStream("2016/2.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val answer1 = run {
        val keypad = (1..9).chunked(3)
        var pos = 1 to 1
        input.fold("") { code, line ->
            line.forEach { c ->
                pos = when (c) {
                    'U' -> pos.x to max(0, pos.y - 1)
                    'D' -> pos.x to min(2, pos.y + 1)
                    'L' -> max(0, pos.x - 1) to pos.y
                    'R' -> min(2, pos.x + 1) to pos.y
                    else -> error("Invalid direction: $c")
                }
            }
            code + keypad[pos]
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val keypad = "~-1-~-234-56789-ABC-~-D-~".toList().chunked(5)
        var pos = 2 to 2
        input.fold("") { code, line ->
            line.forEach { c ->
                val newPos = when (c) {
                    'U' -> pos.x to max(0, pos.y - 1)
                    'D' -> pos.x to min(4, pos.y + 1)
                    'L' -> max(0, pos.x - 1) to pos.y
                    'R' -> min(4, pos.x + 1) to pos.y
                    else -> error("Invalid direction: $c")
                }
                pos = if (keypad[newPos] == '-') pos else newPos
            }
            code + keypad[pos]
        }
    }
    println("Part 2: $answer2")
}
