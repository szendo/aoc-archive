package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/12.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.split(' ', limit = 2) }

    fun parseParam(regs: MutableList<Long>, p: String) =
        if (p[0] in "abcd") regs[p[0] - 'a'] else p.toLong()

    fun execute(initialRegs: List<Long>): Long {
        val regs = initialRegs.toMutableList()
        var ip = 0

        try {
            while (true) {
                val (instr, params) = input[ip++]
                when (instr) {
                    "cpy" -> {
                        val (p1, p2) = params.split(' ')
                        regs[p2[0] - 'a'] = parseParam(regs, p1)
                    }
                    "inc" -> regs[params[0] - 'a']++
                    "dec" -> regs[params[0] - 'a']--
                    "jnz" -> {
                        val (p1, p2) = params.split(' ')
                        if (parseParam(regs, p1) != 0L) ip = ip - 1 + p2.toInt()
                    }
                    else -> error("Unknown instruction: $instr")
                }
            }

        } catch (ignored: IndexOutOfBoundsException) {
            return regs[0]
        }
    }

    val answer1 = run {
        execute(List(4) { 0 })
    }
    println("Part 1: $answer1")

    val answer2 = run {
        execute(listOf(0L, 0L, 1L, 0L))
    }
    println("Part 2: $answer2")
}
