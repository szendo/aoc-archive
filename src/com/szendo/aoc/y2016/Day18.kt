package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/18.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .map { it == '^' }
        .toList()

    fun generateRows(firstRow: List<Boolean>) = generateSequence(firstRow) { prev ->
        (listOf(false) + prev + false).windowed(3).map { (left, center, right) ->
            left && center && !right || !left && center && right ||
                left && !center && !right || !left && !center && right
        }
    }

    val answer1 = run {
        generateRows(input).take(40).sumOf { it.count { trap -> !trap } }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        generateRows(input).take(400_000).sumOf { it.count { trap -> !trap }.toLong() }
    }
    println("Part 2: $answer2")
}
