package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/21.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val swapPos = Regex("swap position (\\d+) with position (\\d+)")
    val swapLetter = Regex("swap letter (.) with letter (.)")
    val rotateLeft = Regex("rotate left (\\d+) steps?")
    val rotateRight = Regex("rotate right (\\d+) steps?")
    val rotateBased = Regex("rotate based on position of letter (.)")
    val reverse = Regex("reverse positions (\\d+) through (\\d+)")
    val move = Regex("move position (\\d+) to position (\\d+)")

    fun scramble(password: String, operations: List<String>): String {
        return operations.fold(password) { s, operation ->
            swapPos.find(operation)?.let { mr ->
                val (p1, p2) = mr.groupValues.drop(1).map { it.toInt() }
                s.mapIndexed { i, c ->
                    when (i) {
                        p1 -> s[p2]
                        p2 -> s[p1]
                        else -> c
                    }
                }.joinToString("")
            } ?: swapLetter.find(operation)?.let { mr ->
                val (p1, p2) = mr.groupValues.drop(1).map { s.indexOf(it[0]) }
                s.mapIndexed { i, c ->
                    when (i) {
                        p1 -> s[p2]
                        p2 -> s[p1]
                        else -> c
                    }
                }.joinToString("")
            } ?: rotateLeft.find(operation)?.let { mr ->
                val steps = mr.groupValues[1].toInt() % s.length
                s.substring(steps, s.length) + s.substring(0, steps)
            } ?: rotateRight.find(operation)?.let { mr ->
                val steps = mr.groupValues[1].toInt() % s.length
                s.substring(s.length - steps, s.length) + s.substring(0, s.length - steps)
            } ?: rotateBased.find(operation)?.let { mr ->
                val index = s.indexOf(mr.groupValues[1][0])
                val steps = (1 + index + if (index >= 4) 1 else 0) % s.length
                s.substring(s.length - steps, s.length) + s.substring(0, s.length - steps)
            } ?: reverse.find(operation)?.let { mr ->
                val (p1, p2) = mr.groupValues.drop(1).map { it.toInt() }
                s.substring(0 until p1) + s.substring(p1..p2).reversed() + s.substring(p2 + 1)
            } ?: move.find(operation)?.let { mr ->
                val (p1, p2) = mr.groupValues.drop(1).map { it.toInt() }
                val c = s[p1]
                val removed = s.substring(0 until p1) + s.substring(p1 + 1)
                removed.substring(0 until p2) + c + removed.substring(p2)
            } ?: error("Unknown operation: $operation")
        }
    }

    fun unscramble(password: String, operations: List<String>): String {
        return operations.reversed().fold(password) { s, operation ->
            swapPos.find(operation)?.let { mr ->
                val (p1, p2) = mr.groupValues.drop(1).map { it.toInt() }
                s.mapIndexed { i, c ->
                    when (i) {
                        p1 -> s[p2]
                        p2 -> s[p1]
                        else -> c
                    }
                }.joinToString("")
            } ?: swapLetter.find(operation)?.let { mr ->
                val (p1, p2) = mr.groupValues.drop(1).map { s.indexOf(it[0]) }
                s.mapIndexed { i, c ->
                    when (i) {
                        p1 -> s[p2]
                        p2 -> s[p1]
                        else -> c
                    }
                }.joinToString("")
            } ?: rotateLeft.find(operation)?.let { mr ->
                val steps = mr.groupValues[1].toInt() % s.length
                s.substring(s.length - steps, s.length) + s.substring(0, s.length - steps)
            } ?: rotateRight.find(operation)?.let { mr ->
                val steps = mr.groupValues[1].toInt() % s.length
                s.substring(steps, s.length) + s.substring(0, steps)
            } ?: rotateBased.find(operation)?.let { mr ->
                val steps = s.indices.first { leftSteps ->
                    val leftS = s.substring(leftSteps, s.length) + s.substring(0, leftSteps)
                    val index = leftS.indexOf(mr.groupValues[1][0])
                    val steps = (1 + index + if (index >= 4) 1 else 0) % s.length
                    s == leftS.substring(leftS.length - steps, leftS.length) + leftS.substring(0, leftS.length - steps)
                }
                s.substring(steps, s.length) + s.substring(0, steps)
            } ?: reverse.find(operation)?.let { mr ->
                val (p1, p2) = mr.groupValues.drop(1).map { it.toInt() }
                s.substring(0 until p1) + s.substring(p1..p2).reversed() + s.substring(p2 + 1)
            } ?: move.find(operation)?.let { mr ->
                val (p2, p1) = mr.groupValues.drop(1).map { it.toInt() }
                val c = s[p1]
                val removed = s.substring(0 until p1) + s.substring(p1 + 1)
                removed.substring(0 until p2) + c + removed.substring(p2)
            } ?: error("Unknown operation: $operation")
        }
    }

    val answer1 = run {
        scramble("abcdefgh", input)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        unscramble("fbgdceah", input)
    }
    println("Part 2: $answer2")
}
