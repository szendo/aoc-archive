package com.szendo.aoc.y2016

import com.szendo.aoc.*
import java.security.MessageDigest
import java.util.*
import kotlin.math.max

fun main() {
    val passcode = Helper.getResourceAsStream("2016/17.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }

    val md5 = MessageDigest.getInstance("MD5")

    fun openDoors(passcodeAndPath: String): List<Boolean> = synchronized(md5) {
        val digest = md5.digest(passcodeAndPath.toByteArray(Charsets.US_ASCII))
        listOf(
            (digest[0].toInt() and 0xf0) > 0xa0,
            (digest[0].toInt() and 0xf) > 0xa,
            (digest[1].toInt() and 0xf0) > 0xa0,
            (digest[1].toInt() and 0xf) > 0xa,
        )
    }

    val answer1 = run {
        val queue = PriorityQueue<Pair<String, Coord>>(Comparator.comparing { it.first.length })
        queue.add("" to (0 to 0))
        while (queue.isNotEmpty()) {
            val (path, pos) = queue.remove()
            if (pos == 3 to 3) return@run path
            val (up, down, left, right) = openDoors(passcode + path)
            if (pos.y > 0 && up) queue.add(path + "U" to pos + (0 to -1))
            if (pos.y < 3 && down) queue.add(path + "D" to pos + (0 to 1))
            if (pos.x > 0 && left) queue.add(path + "L" to pos + (-1 to 0))
            if (pos.x < 3 && right) queue.add(path + "R" to pos + (1 to 0))
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val queue = PriorityQueue<Pair<String, Coord>>(Comparator.comparing { it.first.length })
        queue.add("" to (0 to 0))
        var longestPath = 0
        while (queue.isNotEmpty()) {
            val (path, pos) = queue.remove()
            if (pos == 3 to 3) {
                longestPath = max(longestPath, path.length)
            } else {
                val (up, down, left, right) = openDoors(passcode + path)
                if (pos.y > 0 && up) queue.add(path + "U" to pos + (0 to -1))
                if (pos.y < 3 && down) queue.add(path + "D" to pos + (0 to 1))
                if (pos.x > 0 && left) queue.add(path + "L" to pos + (-1 to 0))
                if (pos.x < 3 && right) queue.add(path + "R" to pos + (1 to 0))
            }
        }
        longestPath
    }
    println("Part 2: $answer2")
}
