package com.szendo.aoc.y2016

import com.szendo.aoc.*
import java.util.*

fun main() {
    val input = Helper.getResourceAsStream("2016/24.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.toList() }

    val height = input.size
    val width = input[0].size

    fun findPos(pos: Char) =
        input.flatMapIndexed { y, row ->
            row.withIndex().firstOrNull { it.value == pos }?.let { (x, _) -> listOf(x to y) } ?: emptyList()
        }.first()

    fun shortestPath(start: Char, end: Char): Int {
        val startPos = findPos(start)
        val endPos = findPos(end)
        val seenPos = mutableSetOf<Coord>()
        val queue = mutableListOf(startPos to 0)
        while (queue.isNotEmpty()) {
            val (pos, steps) = queue.removeFirst()
            if (pos == endPos) return steps
            listOf(-1 to 0, 0 to -1, 0 to 1, 1 to 0).forEach { d ->
                val (x, y) = pos + d
                if (x in (0 until width) && y in (0 until height) && input[y][x] != '#' && seenPos.add(x to y)) {
                    queue.add((x to y) to steps + 1)
                }
            }
        }
        error("No path between $start and $end")
    }

    val digits = ('0'..'9').filter { input.any { row -> it in row } }.toSet()
    val paths = digits.associateWith { start ->
        digits.filter { it != start }.associateWith { end ->
            shortestPath(start, end)
        }
    }
    val startPos = '0' to digits - '0'

    val answer1 = run {
        val seenPos = mutableMapOf<Pair<Char, Set<Char>>, Int>()
        val queue = PriorityQueue<Pair<Pair<Char, Set<Char>>, Int>>(Comparator.comparing { it.second })
        queue.add(startPos to 0)
        while (queue.isNotEmpty()) {
            val (state, steps) = queue.remove()
            if (state in seenPos) {
                val prevSteps = seenPos.getValue(state)
                if (prevSteps <= steps) continue
            }
            seenPos[state] = steps
            val (curr, remaining) = state
            if (remaining.isEmpty()) return@run steps
            remaining.forEach { next ->
                val cost = paths.getValue(curr).getValue(next)
                val nextState = next to remaining - next
                queue.add(nextState to steps + cost)
            }
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val seenPos = mutableMapOf<Pair<Char, Set<Char>>, Int>()
        val queue = PriorityQueue<Pair<Pair<Char, Set<Char>>, Int>>(Comparator.comparing { it.second })
        queue.add(startPos to 0)
        while (queue.isNotEmpty()) {
            val (state, steps) = queue.remove()
            if (state in seenPos) {
                val prevSteps = seenPos.getValue(state)
                if (prevSteps <= steps) continue
            }
            seenPos[state] = steps
            val (curr, remaining) = state
            if (remaining.isEmpty() && curr == '0') return@run steps
            if (remaining.isNotEmpty()) {
                remaining.forEach { next ->
                    val cost = paths.getValue(curr).getValue(next)
                    val nextState = next to remaining - next
                    queue.add(nextState to steps + cost)
                }
            } else {
                val cost = paths.getValue(curr).getValue('0')
                val nextState = '0' to remaining
                queue.add(nextState to steps + cost)
            }
        }
    }
    println("Part 2: $answer2")
}
