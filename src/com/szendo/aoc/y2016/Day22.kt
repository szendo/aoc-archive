package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    data class Node(val size: Int, val used: Int, val avail: Int)

    val input = Helper.getResourceAsStream("2016/22.txt")
        .bufferedReader()
        .useLines { lines -> lines.drop(2).toList() }
        .map { line ->
            val mr = Regex("/dev/grid/node-x(\\d+)-y(\\d+)\\s+(\\d+)T\\s+(\\d+)T\\s+(\\d+)T\\s+\\d+%").matchEntire(line)!!
            val (x, y, size, used, avail) = mr.groupValues.drop(1).map { it.toInt() }
            (x to y) to Node(size, used, avail)
        }.toMap()

    val answer1 = run {
        input.entries.sumOf { (ac, ad) ->
            input.entries.count { (bc, bd) ->
                ac != bc && ad.used != 0 && ad.used <= bd.avail
            }
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        (-1..24).forEach { y ->
            (-1..35).forEach { x ->
                if (y == -1) {
                    print(if (x == -1) "   " else x.toString().padStart(2).padEnd(3))
                } else if (x == -1) {
                    print(y.toString().padStart(2).padEnd(3))
                } else {
                    val node = input.getValue(x to y)
                    print(
                        if (x to y == 0 to 0)
                            if (node.used == 0) "(_)" else "(.)"
                        else if (node.used == 0) " _ "
                        else if (x to y == 35 to 0) " G "
                        else if (node.size > 100) " # "
                        else " . "
                    )
                }
            }
            println()
        }
        // TODO add proper solution
        0 + // empty node is az (22, 17)
            17 + // move empty node to (22,0): 17*L
            22 + // move empty node to (0,0): 22*U
            35 + // move empty to to (0,35) causing G to be at (0,34): 35*R
            34 * 5 // 34 * move G to left one: 34*DLLUR
    }
    println("Part 2: $answer2")
}
