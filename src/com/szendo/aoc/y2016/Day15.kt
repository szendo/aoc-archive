package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/15.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map {
            Regex("\\d+").findAll(it).map { mr -> mr.value.toInt() }.toList()
                .let { numbers -> Triple(numbers[0], numbers[1], numbers[3]) }
        }

    fun findTiming(input: List<Triple<Int, Int, Int>>) =
        generateSequence(0) { it + 1 }
            .first { t ->
                input.all { (n, pos, cur) ->
                    (cur + t + n) % pos == 0
                }
            }

    val answer1 = run {
        findTiming(input)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        findTiming(input + Triple(input.size + 1, 11, 0))
    }
    println("Part 2: $answer2")
}
