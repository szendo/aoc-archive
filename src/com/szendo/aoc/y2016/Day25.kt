package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/25.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.split(' ', limit = 2) }

    fun parseParam(regs: MutableList<Long>, p: String) =
        if (p[0] in "abcd") regs[p[0] - 'a'] else p.toLong()

    fun execute(initialRegs: List<Long>): List<Long> {
        val outRegs = mutableListOf<List<Long>>()
        val outVals = mutableListOf<Long>()

        val code = input.toMutableList()
        val regs = initialRegs.toMutableList()
        var ip = 0

        try {
            while (true) {
                val (instr, params) = code[ip++]
                when (instr) {
                    "cpy" -> {
                        val (p1, p2) = params.split(' ')
                        if (p2[0] in "abcd") {
                            regs[p2[0] - 'a'] = parseParam(regs, p1)
                        }
                    }
                    "inc" -> regs[params[0] - 'a']++
                    "dec" -> regs[params[0] - 'a']--
                    "jnz" -> {
                        val (p1, p2) = params.split(' ')
                        if (parseParam(regs, p1) != 0L) ip = ip - 1 + parseParam(regs, p2).toInt()
                    }
                    "out" -> {
                        if (outRegs.isEmpty() || outRegs[0] != regs) {
                            outRegs.add(regs.toList())
                            outVals.add(parseParam(regs, params))
                        } else {
                            return outVals
                        }
                    }
                    else -> error("Unknown instruction: $instr")
                }
            }
        } catch (ignored: IndexOutOfBoundsException) {
            error("Program stopped")
        }
    }

    val answer1 = run {
        generateSequence(0L) { it + 1 }
            .first { n ->
                execute(listOf(n, 0, 0, 0)).withIndex().all { (index, value) -> index % 2 == value.toInt() }
            }
    }
    println("Part 1: $answer1")
}
