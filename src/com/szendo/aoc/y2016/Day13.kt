package com.szendo.aoc.y2016

import com.szendo.aoc.*
import java.util.*

fun main() {
    val input = Helper.getResourceAsStream("2016/13.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .toInt()

    fun Coord.isWall(favNum: Int) =
        (x * x + 3 * x + 2 * x * y + y + y * y + favNum).toString(2).count { it == '1' } % 2 == 1

    val answer1 = run {
        val startPos = 1 to 1
        val goalPos = 31 to 39
        val seenPos = mutableSetOf<Coord>()
        val queue = PriorityQueue<Pair<Coord, Int>>(Comparator.comparing { it.second })
        queue.add(startPos to 0)
        while (queue.isNotEmpty()) {
            val (pos, steps) = queue.remove()
            if (pos == goalPos) return@run steps
            listOf(1 to 0, 0 to 1, 0 to -1, -1 to 0).forEach { d ->
                val newPos = pos + d
                if (newPos.x < 0 || newPos.y < 0) return@forEach
                if (!newPos.isWall(input) && seenPos.add(newPos)) {
                    queue.add(newPos to steps + 1)
                }
            }
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val startPos = 1 to 1
        val seenPos = mutableSetOf<Coord>()
        val queue = PriorityQueue<Pair<Coord, Int>>(Comparator.comparing { it.second })
        queue.add(startPos to 0)
        while (queue.isNotEmpty()) {
            val (pos, steps) = queue.remove()
            if (steps < 50) {
                listOf(1 to 0, 0 to 1, 0 to -1, -1 to 0).forEach { d ->
                    val newPos = pos + d
                    if (newPos.x < 0 || newPos.y < 0) return@forEach
                    if (!newPos.isWall(input) && seenPos.add(newPos)) {
                        queue.add(newPos to steps + 1)
                    }
                }
            }
        }
        seenPos.size
    }
    println("Part 2: $answer2")
}
