package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/16.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }

    fun generateData(initialData: String, len: Int): String {
        var data = initialData.toList()
        while (data.size < len) {
            data = data + '0' + data.reversed().map { if (it == '0') '1' else '0' }
        }
        return data.subList(0, len).joinToString("")
    }

    fun calculateChecksum(data: String): String {
        var checksum = data.toList()
        while (checksum.size % 2 == 0) {
            checksum = checksum.chunked(2).map {
                if (it[0] == it[1]) '1' else '0'
            }
        }
        return checksum.joinToString("")
    }

    val answer1 = run {
        calculateChecksum(generateData(input, 272))
    }
    println("Part 1: $answer1")

    val answer2 = run {
        calculateChecksum(generateData(input, 35651584))
    }
    println("Part 2: $answer2")
}
