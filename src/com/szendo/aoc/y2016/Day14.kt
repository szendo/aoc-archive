package com.szendo.aoc.y2016

import com.szendo.aoc.*
import java.security.MessageDigest

fun main() {
    val salt = Helper.getResourceAsStream("2016/14.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }

    val md5 = MessageDigest.getInstance("MD5")

    fun md5(s: String): String = synchronized(md5) {
        md5.digest(s.toByteArray(Charsets.US_ASCII)).fold(StringBuilder(32)) { sb, b ->
            sb.append(((b.toInt() and 0xff) / 16).digitToChar(16))
                .append((b.toInt() and 0xf).digitToChar(16))
        }.toString().lowercase()
    }

    fun stretchedMd5(s: String) = (0..2016).fold(s) { s1, _ -> md5(s1) }

    val threeRegex = Regex("([0-9a-f])\\1\\1")

    val answer1 = run {
        generateSequence(0) { it + 1 }
            .map { it to md5(salt + it) }
            .windowed(1001)
            .filter { hashes ->
                threeRegex.find(hashes[0].second)?.let { m ->
                    val five = "".padEnd(5, m.groupValues[1][0])
                    hashes.drop(1).any { five in it.second }
                } ?: false
            }
            .drop(63)
            .first()[0].first
    }
    println("Part 1: $answer1")

    val answer2 = run {
        generateSequence(0) { it + 1 }
            .map { it to stretchedMd5(salt + it) }
            .windowed(1001)
            .filter { hashes ->
                threeRegex.find(hashes[0].second)?.let { m ->
                    val five = "".padEnd(5, m.groupValues[1][0])
                    hashes.drop(1).any { five in it.second }
                } ?: false
            }
            .drop(63)
            .first()[0].first
    }
    println("Part 2: $answer2")
}
