package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/24.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.toList() }

    val height = input.size
    val width = input[0].size

    fun findPos(pos: Char) =
        input.flatMapIndexed { y, row ->
            row.withIndex().firstOrNull { it.value == pos }?.let { (x, _) -> listOf(x to y) } ?: emptyList()
        }.first()

    val digits = ('0'..'9').filter { input.any { row -> it in row } }.toSet()
    val paths = digits.associateWith { start ->
        digits.filter { it != start }.associateWith { end ->
            shortestPathBfs(findPos(start), findPos(end)) { pos ->
                setOf(-1 to 0, 0 to -1, 0 to 1, 1 to 0).map { pos + it }
                    .filter { (x, y) -> x in (0 until width) && y in (0 until height) && input[y][x] != '#' }
                    .toSet()
            }
        }
    }
    val startPos = '0' to digits - '0'

    val answer1 = run {
        shortestPathDijkstra(source = startPos, isTarget = { this.second.isEmpty() }) { (curr, remaining) ->
            remaining.associate { next -> (next to remaining - next) to paths.getValue(curr).getValue(next) }
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        shortestPathDijkstra(source = startPos, isTarget = { this == ('0' to setOf<Char>()) }) { (curr, remaining) ->
            if (remaining.isNotEmpty()) {
                remaining.associate { next -> (next to remaining - next) to paths.getValue(curr).getValue(next) }
            } else {
                mapOf(('0' to remaining) to paths.getValue(curr).getValue('0'))
            }
        }
    }
    println("Part 2: $answer2")
}
