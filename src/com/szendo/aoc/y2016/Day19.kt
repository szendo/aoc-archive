package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/19.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .toInt()

    data class Elf(val number: Int, var next: Elf?)

    val answer1 = run {
        val initial = Elf(1, null)
        (2..input).fold(initial) { prev, number ->
            Elf(number, null).apply { prev.next = this }
        }.next = initial

        var current = initial
        while (current.next != current) {
            val loser = current.next!!
            current.next = loser.next
            loser.next = null
            current = current.next!!
        }
        current.number
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var size = input
        val initial = Elf(1, null)
        (2..size).fold(initial) { prev, number ->
            Elf(number, null).apply { prev.next = this }
        }.next = initial

        var current = initial
        var prevAcross = generateSequence(initial) { it.next!! }.first { it.number == size / 2 }
        while (current.next != current) {
            val loser = prevAcross.next!!
            prevAcross.next = loser.next
            loser.next = null
            current = current.next!!
            if (size % 2 == 1)
                prevAcross = prevAcross.next!!
            size--
        }
        current.number
    }
    println("Part 2: $answer2")
}
