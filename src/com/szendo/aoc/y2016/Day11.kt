package com.szendo.aoc.y2016

import com.szendo.aoc.*
import java.util.*

fun main() {
    data class Floor(val generators: Set<String>, val microchips: Set<String>)

    val input = Helper.getResourceAsStream("2016/11.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { line ->
            Floor(
                Regex("([a-z]+) generator").findAll(line).map { it.groupValues[1] }.toSet(),
                Regex("([a-z]+)-compatible microchip").findAll(line).map { it.groupValues[1] }.toSet()
            )
        }

    data class State(val elevator: Int, val floors: List<Floor>) {
        fun isValid() = floors.all { floor -> floor.microchips.all { type -> floor.generators.isEmpty() || type in floor.generators } }
    }

    fun getNewStates(state: State, gensToMove: Set<String>, chipsToMove: Set<String>): List<State> {
        val result = mutableListOf<State>()

        val (elevator, floors) = state
        if (elevator < floors.lastIndex) {
            result.add(State(elevator + 1, floors.mapIndexed { floorIndex, floor ->
                when (floorIndex) {
                    elevator -> Floor(floor.generators - gensToMove, floor.microchips - chipsToMove)
                    elevator + 1 -> Floor(floor.generators + gensToMove, floor.microchips + chipsToMove)
                    else -> floor
                }
            }))
        }
        if (elevator > 0) {
            result.add(State(elevator - 1, floors.mapIndexed { floorIndex, floor ->
                when (floorIndex) {
                    elevator -> Floor(floor.generators - gensToMove, floor.microchips - chipsToMove)
                    elevator - 1 -> Floor(floor.generators + gensToMove, floor.microchips + chipsToMove)
                    else -> floor
                }
            }))
        }

        return result.filter { it.isValid() }
    }

    fun findMinimumMoveCount(initialState: State): Int {
        val seenStates = mutableSetOf(initialState)

        val queue = PriorityQueue<Pair<State, Int>>(Comparator.comparing { it.second })
        queue.add(initialState to 0)

        while (queue.isNotEmpty()) {
            val (state, steps) = queue.remove()
            val (elevator, floors) = state

            if (floors.dropLast(1).all { it.generators.isEmpty() && it.microchips.isEmpty() }) {
                return steps
            }

            val currentFloor = floors[elevator]

            val newStates = currentFloor.generators.flatMap { gen ->
                currentFloor.microchips.flatMap { chip ->
                    getNewStates(state, setOf(gen), setOf(chip))
                } + (currentFloor.generators - gen).flatMap { gen2 ->
                    getNewStates(state, setOf(gen, gen2), setOf())
                } + getNewStates(state, setOf(gen), setOf())
            } + currentFloor.microchips.flatMap { chip ->
                (currentFloor.microchips - chip).flatMap { chip2 ->
                    getNewStates(state, setOf(), setOf(chip, chip2))
                } + getNewStates(state, setOf(), setOf(chip))
            }

            newStates.forEach { if (seenStates.add(it)) queue.add(it to steps + 1) }
        }

        error("Solution not found")
    }

    val answer1 = run {
        findMinimumMoveCount(State(0, input))
    }
    println("Part 1: $answer1")

    val answer2 = run {
        // this takes too long to run
        //
        //     setOf("elerium", "dilithium").let { extraParts ->
        //         findMinimumMoveCount(
        //             State(0, input.mapIndexed { index, floor ->
        //                 if (index == 0) Floor(floor.generators + extraParts, floor.microchips + extraParts) else floor
        //             })
        //         )
        //     }
        //
        // but
        // - F1=polonium-gen+promethium-gen, F2=polonium-chip+promethium-chip, F3=empty, F4=empty takes 11 steps
        // - every extra pair of parts (gen+chip) on F1 adds 12 steps
        // so elerium and dilithium parts add 2*12=24 steps compared to part 1 of the puzzle
        answer1 + 2 * 12
    }
    println("Part 2: $answer2")
}
