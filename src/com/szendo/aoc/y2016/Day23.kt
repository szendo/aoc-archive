package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/23.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.split(' ', limit = 2) }

    fun parseParam(regs: MutableList<Long>, p: String) =
        if (p[0] in "abcd") regs[p[0] - 'a'] else p.toLong()

    fun execute(initialRegs: List<Long>): Long {
        val code = input.toMutableList()
        val regs = initialRegs.toMutableList()
        var ip = 0

        try {
            while (true) {
                val (instr, params) = code[ip++]
                when (instr) {
                    "cpy" -> {
                        val (p1, p2) = params.split(' ')
                        if (p2[0] in "abcd") {
                            regs[p2[0] - 'a'] = parseParam(regs, p1)
                        }
                    }
                    "inc" -> regs[params[0] - 'a']++
                    "dec" -> regs[params[0] - 'a']--
                    "jnz" -> {
                        val (p1, p2) = params.split(' ')
                        if (parseParam(regs, p1) != 0L) ip = ip - 1 + parseParam(regs, p2).toInt()
                    }
                    "tgl" -> {
                        val index = ip - 1 + regs[params[0] - 'a'].toInt()
                        if (index in code.indices) {
                            val oldInstr = code[index][0]
                            val newInstr = when (oldInstr) {
                                "cpy" -> "jnz"
                                "jnz" -> "cpy"
                                "inc" -> "dec"
                                "dec", "tgl" -> "inc"
                                else -> error("Unknown instruction: $instr")
                            }
                            code[index] = listOf(newInstr, code[index][1])

                        }
                    }
                    else -> error("Unknown instruction: $instr")
                }
            }
        } catch (ignored: IndexOutOfBoundsException) {
            return regs[0]
        }
    }

    fun calculateResult(n: Long) = (n downTo 5).reduce(Long::times) * 4 * 3 * 2 + 95 * 73

    val answer1 = run {
        execute(listOf(7, 0, 0, 0))
    }
    println("Part 1: $answer1")

    val answer2 = run {
        // TODO reverse engineer assembunny code
        // execute(listOf(12, 0, 0, 0))
        calculateResult(12)
    }
    println("Part 2: $answer2")
}
