package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2016/6.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val charMaps = input.fold(List(8) { mutableMapOf<Char, Int>() }) { acc, line ->
        acc.zip(line.toList()).forEach { it.first.merge(it.second, 1, Int::plus) }
        acc
    }
    val answer1 = run {
        charMaps.map { charMap -> charMap.maxByOrNull { it.value }!!.key }.joinToString("")
    }
    println("Part 1: $answer1")

    val answer2 = run {
        charMaps.map { charMap -> charMap.minByOrNull { it.value }!!.key }.joinToString("")
    }
    println("Part 2: $answer2")
}
