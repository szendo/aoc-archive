package com.szendo.aoc.y2016

import com.szendo.aoc.*
import java.security.MessageDigest

fun main() {
    val input = Helper.getResourceAsStream("2016/5.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }

    val md5 = MessageDigest.getInstance("MD5")

    fun md5(s: String): String = synchronized(md5) {
        md5.digest(s.toByteArray(Charsets.US_ASCII)).fold(StringBuilder(32)) { sb, b ->
            sb.append(((b.toInt() and 0xff) / 16).digitToChar(16))
                .append((b.toInt() and 0xf).digitToChar(16))
        }.toString().lowercase()
    }

    val answer1 = run {
        generateSequence(0) { it + 1 }
            .map { md5("$input$it") }
            .filter { it.startsWith("00000") }
            .map { it[5] }.take(8).joinToString("")
    }
    println("Part 1: $answer1")

    val answer2 = run {
        generateSequence(0) { it + 1 }
            .map { md5("$input$it") }
            .filter { it.startsWith("00000") && it[5] in '0'..'7' }
            .map { it[5].digitToInt() to it[6] }
            .runningFold("________") { s, (pos, c) ->
                if (s[pos] == '_') s[0 until pos] + c + s.substring(pos + 1)
                else s
            }
            .dropWhile { '_' in it }
            .first()
    }
    println("Part 2: $answer2")
}
