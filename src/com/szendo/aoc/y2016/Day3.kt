package com.szendo.aoc.y2016

import com.szendo.aoc.*

fun main() {
    val regex = Regex("([a-z]+(?:-[a-z]+)*)-(\\d+)\\[([a-z]+)]")
    val input = Helper.getResourceAsStream("2016/4.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { line -> regex.matchEntire(line)!!.groupValues.let { Triple(it[1], it[2].toInt(), it[3]) } }

    val realRooms = input.filter { (name, _, checksum) ->
        name.filter { it in 'a'..'z' }
            .groupingBy { it }.eachCount()
            .toList()
            .sortedWith(
                Comparator.comparingInt<Pair<Char, Int>> { -it.second }
                    .then(Comparator.comparing { it.first })
            )
            .take(5)
            .joinToString("") { it.first.toString() } == checksum
    }

    val answer1 = run {
        realRooms.sumOf { (_, sectorId) -> sectorId }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        realRooms
            .map { (name, sectorId) ->
                name.map { c ->
                    if (c in 'a'..'z') 'a' + ((c - 'a') + sectorId) % 26
                    else c
                }.joinToString("") to sectorId
            }
            .first { (name) -> "north" in name && "pole" in name }.second
    }
    println("Part 2: $answer2")
}
