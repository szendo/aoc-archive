package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/1.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .map { if (it == '(') 1 else -1 }
        .toList()

    val answer1 = run {
        input.sum()
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.runningReduce(Int::plus).indexOfFirst { it < 0 } + 1
    }
    println("Part 2: $answer2")
}
