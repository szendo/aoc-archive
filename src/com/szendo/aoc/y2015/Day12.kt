package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/12.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }

    val numberRegex = Regex("(-?\\d+)")

    fun sumNumbers(obj: String) =
        numberRegex.findAll(obj).map { it.groupValues[1].toInt() }.sum()

    val answer1 = run {
        sumNumbers(input)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val objRegex = Regex("\\{[^{}]*}")
        var json = input
        while (true) {
            val reduced = objRegex.replace(json) { mr ->
                mr.value.let { obj -> if (obj.contains(":\"red\"")) 0 else sumNumbers(obj) }.toString()
            }
            if (json == reduced) {
                break
            }
            json = reduced
        }
        sumNumbers(json)
    }
    println("Part 2: $answer2")
}
