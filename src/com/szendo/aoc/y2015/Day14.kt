package com.szendo.aoc.y2015

import com.szendo.aoc.*
import java.lang.Integer.min

fun main() {
    data class Reindeer(val flySpeed: Int, val flyTime: Int, val restTime: Int) {
        fun distance(time: Int): Int =
            (time / (flyTime + restTime)) * flyTime * flySpeed +
                min(flyTime, time % (flyTime + restTime)) * flySpeed
    }
    val input = Helper.getResourceAsStream("2015/14.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { line ->
            Regex("\\w+ can fly (\\d+) km/s for (\\d+) seconds, but then must rest for (\\d+) seconds.")
                .matchEntire(line)!!.groupValues.let {
                    Reindeer(it[1].toInt(), it[2].toInt(), it[3].toInt())
                }
        }

    val answer1 = run {
        input.maxOf { it.distance(2503) }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        (1..2503).fold(input.map { 0 }) { scores, t ->
            val distances = input.map { it.distance(t) }
            val maxDistance = distances.maxOf { it }
            scores.mapIndexed { i, score -> score + if (distances[i] == maxDistance) 1 else 0 }
        }.maxOf { it }
    }
    println("Part 2: $answer2")
}
