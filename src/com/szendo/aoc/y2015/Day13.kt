package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val regex = Regex("(\\w+) would (gain|lose) (\\d+) happiness units by sitting next to (\\w+)\\.")
    val input = Helper.getResourceAsStream("2015/13.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { line ->
            regex.matchEntire(line)!!.groupValues.let {
                it[1] to (it[4] to (if (it[2] == "gain") 1 else -1) * it[3].toInt())
            }
        }
        .groupBy({ it.first }) { it.second }
        .mapValues { it.value.toMap() }

    val answer1 = run {
        input.keys.permutations()
            .map { (it + it[0]).zipWithNext() }
            .maxOf { it.sumOf { (p1, p2) -> (input[p1]?.get(p2) ?: 0) + (input[p2]?.get(p1) ?: 0) } }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        (input.keys + "ME!").permutations()
            .map { (it + it[0]).zipWithNext() }
            .maxOf { it.sumOf { (p1, p2) -> (input[p1]?.get(p2) ?: 0) + (input[p2]?.get(p1) ?: 0) } }
    }
    println("Part 2: $answer2")
}
