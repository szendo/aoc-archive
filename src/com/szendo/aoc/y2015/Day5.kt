package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/5.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val answer1 = run {
        input.count {
            it.count { c -> c in "aeiou" } >= 3 &&
                it.zipWithNext().any { (c1, c2) -> c1 == c2 } &&
                "ab" !in it && "cd" !in it && "pq" !in it && "xz" !in it
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.count {
            it.windowed(2).withIndex().any { (i, s) -> it.indexOf(s, i + 2) != -1 }
                && it.windowed(3).any { s -> s[0] == s[2] }
        }
    }
    println("Part 2: $answer2")
}
