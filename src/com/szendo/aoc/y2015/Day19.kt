package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val (input, replacements) = Helper.getResourceAsStream("2015/19.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .split("")
        .let { lines ->
            val s = lines[1][0]
            val map = lines[0].map { it.split(" => ").let { it[0] to it[1] } }
            s to map
        }

    val answer1 = run {
        val molecules = mutableSetOf<String>()

        for ((from, to) in replacements) {
            var index = -1
            while (true) {
                index = input.indexOf(from, index + 1)
                if (index == -1) break
                molecules.add(input.replaceRange(index, index + from.length, to))
            }
        }
        molecules.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var molecule = input
        var steps = 0
        start@ do {
            var foundNewMol = false
            for ((to, from) in replacements) {
                val index = molecule.indexOf(from)
                if (index != -1) {
                    foundNewMol = true
                    molecule = molecule.replaceRange(index, index + from.length, to)
                    steps++
                    continue@start
                }
            }
        } while (foundNewMol)
        check(molecule == "e")
        steps
    }
    println("Part 2: $answer2")
}
