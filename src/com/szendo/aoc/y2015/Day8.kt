package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/8.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val regex2Char = Regex("\\\\([\\\\\"])")
    val regex4Char = Regex("\\\\(x[0-9a-fA-F]{2})")
    val answer1 = run {
        input.sumOf { 2 + regex2Char.findAll(it).count() + 3 * regex4Char.findAll(it).count() }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.sumOf { 2 + it.count { c -> c in setOf('\\', '"') } }
    }
    println("Part 2: $answer2")
}
