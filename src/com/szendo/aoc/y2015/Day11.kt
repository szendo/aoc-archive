package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/11.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }

    fun cycle(str: String): String {
        val s = str.toCharArray()
        var pos = s.lastIndex
        while (s[pos] == 'z') {
            s[pos--] = 'a'
        }
        s[pos]++
        return String(s)
    }

    fun nextPassword(password: String): String {
        var s = cycle(password)
        while (true) {
            if (s.windowed(3).any() { it[1] == it[0] + 1 && it[2] == it[1] + 1 } &&
                s.none { it in "iol" } &&
                s.windowed(2).withIndex()
                    .filter { (_, p) -> p[0] == p[1] }
                    .map { it.index }.let { it.size > 1 && it.last() - it.first() > 1 }) {
                return s
            }
            s = cycle(s)
        }
    }

    val answer1 = run {
        nextPassword(input)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        nextPassword(answer1)
    }
    println("Part 2: $answer2")
}