package com.szendo.aoc.y2015

import com.szendo.aoc.*
import java.security.MessageDigest

fun main() {
    val input = Helper.getResourceAsStream("2015/4.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }

    val md5 = MessageDigest.getInstance("MD5")

    fun md5(s: String): String = synchronized(md5) {
        md5.digest(s.toByteArray(Charsets.US_ASCII)).fold(StringBuilder(32)) { sb, b ->
            sb.append(((b.toInt() and 0xff) / 16).digitToChar(16))
                .append((b.toInt() and 0xf).digitToChar(16))
        }.toString().lowercase()
    }

    val answer1 = run {
        generateSequence(1) { it + 1 }
            .first { md5("$input$it").startsWith("00000") }

    }
    println("Part 1: $answer1")

    val answer2 = run {
        generateSequence(answer1) { it + 1 }
            .first { md5("$input$it").startsWith("000000") }
    }
    println("Part 2: $answer2")
}
