package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/7.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.split(Regex(" (?:-> )?")) }

    fun getSignals(input: List<List<String>>, overrides: Map<String, Int> = mapOf()): Map<String,Int> {
        val signals = mutableMapOf<String, Int>()
        signals += overrides

        val connections = input.toMutableList()

        while (connections.isNotEmpty()) {
            val iterator = connections.iterator()

            while (iterator.hasNext()) {
                val connection = iterator.next()

                if (connection.last() in overrides) {
                    iterator.remove()
                    continue
                }

                when (connection.size) {
                    2 -> {
                        if (connection[0][0] in 'a'..'z') {
                            if (connection[0] in signals) {
                                signals[connection[1]] = signals.getValue(connection[0])
                                iterator.remove()
                            }
                        } else {
                            signals[connection[1]] = connection[0].toInt()
                            iterator.remove()
                        }
                    }
                    3 -> {
                        check(connection[0] == "NOT")
                        if (connection[1][0] in 'a'..'z') {
                            if (connection[1] in signals) {
                                signals[connection[2]] = signals.getValue(connection[1]).inv() and 0xffff
                                iterator.remove()
                            }
                        } else {
                            signals[connection[2]] = connection[1].toInt().inv() and 0xffff
                            iterator.remove()
                        }
                    }
                    4 -> {
                        val a = if (connection[0][0] in 'a'..'z') {
                            if (connection[0] in signals) {
                                signals.getValue(connection[0])
                            } else null
                        } else {
                            connection[0].toInt()
                        }
                        val b = if (connection[2][0] in 'a'..'z') {
                            if (connection[2] in signals) {
                                signals.getValue(connection[2])
                            } else null
                        } else {
                            connection[2].toInt()
                        }

                        if (a != null && b != null) {
                            signals[connection[3]] = when (val op = connection[1]) {
                                "AND" -> a and b
                                "OR" -> a or b
                                "LSHIFT" -> (a shl b) and 0xffff
                                "RSHIFT" -> (a shr b) and 0xffff
                                else -> error("Invalid operator: $op")
                            }
                            iterator.remove()
                        }

                    }
                    else -> error("Invalid connection: $connection")
                }
            }
        }

        return signals
    }

    val answer1 = run {
        getSignals(input).getValue("a")
    }
    println("Part 1: $answer1")

    val answer2 = run {
        getSignals(input, overrides = mapOf("b" to answer1)).getValue("a")
    }
    println("Part 2: $answer2")
}
