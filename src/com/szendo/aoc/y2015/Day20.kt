package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/20.txt")
        .bufferedReader()
        .useLines { lines -> lines.first().toInt() }

    val answer1 = run {
        val houses = MutableList(input / 10 + 1) { 0 }
        (1 until houses.size).forEach { elf ->
            (elf until houses.size step elf).forEach { house ->
                houses[house] += elf * 10
            }
        }
        houses.withIndex().first { (_, presents) -> presents >= input }.index
    }
    println("Part 1: $answer1")

    val answer2 = run {
        fun countPresents(house: Int): Long {
            return (1..50).filter { house % it == 0 }.sumOf { house / it } * 11L
        }

        generateSequence(1) { it + 1 }
            .first { countPresents(it) >= input }
    }
    println("Part 2: $answer2")
}
