package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/3.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .toList()

    val answer1 = run {
        input.fold((0 to 0) to setOf(0 to 0)) { (pos, visited), dir ->
            when (dir) {
                '^' -> pos + (0 to 1)
                'v' -> pos - (0 to 1)
                '>' -> pos + (1 to 0)
                '<' -> pos - (1 to 0)
                else -> error("Invalid direction: $dir")
            }.let { newPos -> newPos to (visited + newPos) }
        }.second.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.chunked(2).fold(listOf((0 to 0), (0 to 0)) to setOf(0 to 0)) { (positions, visited), dirs ->
            positions.zip(dirs).map { (pos, dir) ->
                when (dir) {
                    '^' -> pos + (0 to 1)
                    'v' -> pos - (0 to 1)
                    '>' -> pos + (1 to 0)
                    '<' -> pos - (1 to 0)
                    else -> error("Invalid direction: $dir")
                }
            }.let { newPositions -> newPositions to (visited + newPositions) }
        }.second.size
    }
    println("Part 2: $answer2")
}
