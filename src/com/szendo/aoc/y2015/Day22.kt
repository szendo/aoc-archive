package com.szendo.aoc.y2015

import com.szendo.aoc.*
import java.util.*
import kotlin.math.max

fun main() {
    val (startingBossHp, bossAtk) = Helper.getResourceAsStream("2015/22.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { l -> l.split(": ").let { it[1].toInt() } }.toList() }

    data class State(
        val hp: Int,
        val mana: Int,
        val bossHp: Int,
        val shield: Int,
        val poison: Int,
        val recharge: Int
    ) {
        fun bossTurn(bossAtk: Int) = State(
            hp - max(bossAtk - if (shield > 0) 7 else 0, 1),
            if (recharge > 0) mana + 101 else mana,
            if (poison > 0) bossHp - 3 else bossHp,
            if (shield > 0) shield - 1 else 0,
            if (poison > 0) poison - 1 else 0,
            if (recharge > 0) recharge - 1 else 0
        )

        fun tickTimers(hardMode: Boolean) = State(
            if (hardMode) hp - 1 else hp,
            if (recharge > 0) mana + 101 else mana,
            if (poison > 0) bossHp - 3 else bossHp,
            if (shield > 0) shield - 1 else 0,
            if (poison > 0) poison - 1 else 0,
            if (recharge > 0) recharge - 1 else 0
        )
    }

    fun findLowestManaCost(initialState: State, hardMode: Boolean = false): Int {
        val queue = PriorityQueue<Pair<State, Int>>(Comparator.comparing { it.second })
        queue.add(initialState to 0)

        val seenStates = mutableSetOf<State>()

        while (queue.isNotEmpty()) {
            val (state, cost) = queue.remove()
            if (state.bossHp <= 0) return cost
            if (state.hp <= 0) continue

            val (hp, mana, bossHp, shield, poison, recharge) = state.tickTimers(hardMode)
            if (hp <= 0) continue
            if (bossHp <= 0) return cost

            if (mana >= 53) {
                val newState = State(hp, mana - 53, bossHp - 4, shield, poison, recharge).bossTurn(bossAtk)
                if (seenStates.add(newState))
                    queue.add(newState to cost + 53)
            }
            if (mana >= 73) {
                val newState = State(hp + 2, mana - 73, bossHp - 2, shield, poison, recharge).bossTurn(bossAtk)
                if (seenStates.add(newState))
                    queue.add(newState to cost + 73)
            }
            if (mana >= 113 && shield == 0) {
                val newState = State(hp, mana - 113, bossHp, 6, poison, recharge).bossTurn(bossAtk)
                if (seenStates.add(newState))
                    queue.add(newState to cost + 113)
            }
            if (mana >= 173 && poison == 0) {
                val newState = State(hp, mana - 173, bossHp, shield, 6, recharge).bossTurn(bossAtk)
                if (seenStates.add(newState))
                    queue.add(newState to cost + 173)
            }
            if (mana >= 229 && recharge == 0) {
                val newState = State(hp, mana - 229, bossHp, shield, poison, 5).bossTurn(bossAtk)
                if (seenStates.add(newState))
                    queue.add(newState to cost + 229)
            }
        }

        error("Player can't win!")
    }

    val answer1 = run {
        findLowestManaCost(State(50, 500, startingBossHp, 0, 0, 0))
    }
    println("Part 1: $answer1")

    val answer2 = run {
        findLowestManaCost(State(50, 500, startingBossHp, 0, 0, 0), hardMode = true)
    }
    println("Part 2: $answer2")
}
