package com.szendo.aoc.y2015

import com.szendo.aoc.*
import kotlin.math.max

fun main() {
    val regex = Regex("(toggle|turn off|turn on) (\\d+),(\\d+) through (\\d+),(\\d+)")
    val input = Helper.getResourceAsStream("2015/6.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { regex.matchEntire(it)!!.groupValues.let { g -> Triple(g[1], g[2].toInt()..g[4].toInt(), g[3].toInt()..g[5].toInt()) } }

    val answer1 = run {
        val grid = MutableList(1000) { MutableList(1000) { false } }

        input.forEach { (type, xRange, yRange) ->
            yRange.forEach { y ->
                xRange.forEach { x ->
                    grid[y][x] = when (type) {
                        "toggle" -> !grid[y][x]
                        "turn off" -> false
                        "turn on" -> true
                        else -> error("Invalid instruction: $type")
                    }
                }
            }
        }

        grid.sumOf { row -> row.count { it } }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val grid = MutableList(1000) { MutableList(1000) { 0 } }

        input.forEach { (type, xRange, yRange) ->
            val increase = when (type) {
                "toggle" -> 2
                "turn off" -> -1
                "turn on" -> 1
                else -> error("Invalid instruction: $type")
            }
            yRange.forEach { y ->
                xRange.forEach { x ->
                    grid[y][x] = max(0, grid[y][x] + increase)
                }
            }
        }

        grid.sumOf { row -> row.sumOf { it } }
    }
    println("Part 2: $answer2")
}
