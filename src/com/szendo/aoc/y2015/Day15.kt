package com.szendo.aoc.y2015

import com.szendo.aoc.*
import kotlin.math.max

fun main() {
    val regex = Regex("-?\\d+")
    val input: List<List<Int>> =
        Helper.getResourceAsStream("2015/15.txt")
            .bufferedReader()
            .useLines { lines -> lines.toList() }
            .map { regex.findAll(it).map { r -> r.value.toInt() }.toList() }

    val answer1 = run {
        var maxScore = 0L
        for (i in 0..100) {
            for (j in 0..(100 - i)) {
                for (k in 0..(100 - i - j)) {
                    val l = 100 - i - j - k
                    val totalScore = (0..3).map { p ->
                        val i1 = i * input[0][p]
                        val i2 = j * input[1][p]
                        val i3 = k * input[2][p]
                        val i4 = l * input[3][p]
                        max(0L, (i1 + i2 + i3 + i4).toLong())
                    }.reduce(Long::times)

                    if (totalScore > maxScore) {
                        maxScore = totalScore
                    }
                }
            }
        }
        maxScore
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var maxScore = 0L
        for (i in 0..100) {
            for (j in 0..(100 - i)) {
                for (k in 0..(100 - i - j)) {
                    val l = 100 - i - j - k

                    val calories = i * input[0][4] + j * input[1][4] + k * input[2][4] + l * input[3][4]
                    if (calories != 500) {
                        continue
                    }

                    val totalScore = (0..3).map { p ->
                        max(0L, (i * input[0][p] + j * input[1][p] + k * input[2][p] + l * input[3][p]).toLong())
                    }.reduce(Long::times)

                    if (totalScore > maxScore) {
                        maxScore = totalScore
                    }
                }
            }
        }
        maxScore
    }
    println("Part 2: $answer2")
}
