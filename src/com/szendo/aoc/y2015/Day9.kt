package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/9.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { line -> line.split(Regex(" to | = ")) }
        .flatMap { listOf(it, listOf(it[1], it[0], it[2])) }
        .groupBy({ it[0] }) { it.drop(1) }
        .mapValues { (_, v) -> v.associate { it[0] to it[1].toInt() } }

    val answer1 = run {
        input.keys.permutations().minOf { order ->
            order.zipWithNext { a, b -> input.getValue(a).getValue(b) }.sum()
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.keys.permutations().maxOf { order ->
            order.zipWithNext { a, b -> input.getValue(a).getValue(b) }.sum()
        }
    }
    println("Part 2: $answer2")
}
