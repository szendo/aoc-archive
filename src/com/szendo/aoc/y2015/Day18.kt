package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/18.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { line -> line.toCharArray().map { it == '#' }.toList() }

    fun stepAnimation(grid: Grid<Boolean>) =
        grid.indices.map { y ->
            grid[y].indices.map { x ->
                val selfAndNeighborsOn = (-1..1).sumOf { dy -> (-1..1).count { dx -> grid.getOrNull(y + dy)?.getOrNull(x + dx) ?: false } }
                if (grid[y][x]) (selfAndNeighborsOn - 1) in 2..3
                else selfAndNeighborsOn == 3
            }
        }

    val answer1 = run {
        val grid = (1..100).fold(input) { grid, _ -> stepAnimation(grid) }
        grid.sumOf { row -> row.count { it } }
    }
    println("Part 1: $answer1")

    fun forceCornersOn(grid: List<List<Boolean>>) =
        (grid.indices).map { y ->
            if (y > 0 && y < grid.lastIndex) grid[y]
            else grid[y].indices.map { x ->
                if (x > 0 && x < grid[y].lastIndex) grid[y][x]
                else true
            }
        }

    val answer2 = run {
        val grid = (1..100).fold(forceCornersOn(input)) { grid, _ -> forceCornersOn(stepAnimation(grid)) }
        grid.sumOf { row -> row.count { it } }
    }
    println("Part 2: $answer2")
}
