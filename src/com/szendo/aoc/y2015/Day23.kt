package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/23.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.split(' ', limit = 2) }.toList() }

    fun execute(initialRegs: Pair<Long, Long>): Long {
        val regs = initialRegs.toList().toMutableList()
        var ip = 0

        try {
            while (true) {
                val (instr, params) = input[ip++]
                when (instr) {
                    "hlf" -> regs[params[0] - 'a'] = regs[params[0] - 'a'] / 2
                    "tpl" -> regs[params[0] - 'a'] = regs[params[0] - 'a'] * 3
                    "inc" -> regs[params[0] - 'a'] = regs[params[0] - 'a'] + 1
                    "jmp" -> ip = ip - 1 + params.toInt()
                    "jie" -> {
                        val (p1, p2) = params.split(", ")
                        if (regs[p1[0] - 'a'] % 2 == 0L) {
                            ip = ip - 1 + p2.toInt()
                        }
                    }
                    "jio" -> {
                        val (p1, p2) = params.split(", ")
                        if (regs[p1[0] - 'a'] == 1L) {
                            ip = ip - 1 + p2.toInt()
                        }
                    }
                    else -> error("Unknown instruction: $instr")
                }

            }

        } catch (ignored: IndexOutOfBoundsException) {
            return regs[1]
        }
    }

    val answer1 = run {
        execute(0L to 0L)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        execute(1L to 0L)
    }
    println("Part 2: $answer2")
}
