package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/10.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .map { it.digitToInt() }

    val answer1 = run {
        var seq = input.asSequence()
        repeat(40) {
            seq = seq.rle().flatMap { it.toList() }
        }
        seq.count()
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var seq = input.asSequence()
        repeat(50) {
            seq = seq.rle().flatMap { it.toList() }
        }
        seq.count()
    }
    println("Part 2: $answer2")
}
