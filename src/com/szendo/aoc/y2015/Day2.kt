package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/2.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { line -> line.split('x').map { it.toInt() } }

    val answer1 = run {
        input.map { listOf(it[0] * it[1], it[0] * it[2], it[1] * it[2]) }
            .sumOf { sides -> 2 * sides.sum() + sides.minOf { it } }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.sumOf { edges -> 2 * (edges.sum() - edges.maxOf { it }) + edges.reduce(Int::times) }
    }
    println("Part 2: $answer2")
}
