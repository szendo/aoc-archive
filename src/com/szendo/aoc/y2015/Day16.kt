package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val regex = Regex("Sue \\d+: ([a-z]+): (\\d+), ([a-z]+): (\\d+), ([a-z]+): (\\d+)")
    val input = Helper.getResourceAsStream("2015/16.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { line ->
            regex.matchEntire(line)!!.let { mr ->
                mr.groupValues.drop(1).chunked(2).associate { it[0] to it[1].toInt() }
            }
        }

    val tape = mapOf(
        "children" to 3,
        "cats" to 7,
        "samoyeds" to 2,
        "pomeranians" to 3,
        "akitas" to 0,
        "vizslas" to 0,
        "goldfish" to 5,
        "trees" to 3,
        "cars" to 2,
        "perfumes" to 1
    )

    val answer1 = run {
        input.withIndex().first { (_, sue) ->
            (tape.all { (k, v) -> (sue[k]?.compareTo(v) ?: 0) == 0 })
        }.index + 1
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.withIndex().first { (_, sue) ->
            tape.all { (k, v) ->
                when (k) {
                    in listOf("cats", "trees") -> (sue[k]?.compareTo(v) ?: 1) > 0
                    in listOf("pomeranians", "goldfish") -> (sue[k]?.compareTo(v) ?: -1) < 0
                    else -> (sue[k]?.compareTo(v) ?: 0) == 0
                }
            }
        }.index + 1
    }
    println("Part 2: $answer2")
}
