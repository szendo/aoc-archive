package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val (row, col) = Helper.getResourceAsStream("2015/25.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .let { s -> Regex("\\d+").findAll(s).map { it.value.toInt() }.toList() }

    val answer1 = run {
        (1 until (row + col - 2).let { it * (it + 1) / 2 } + col)
            .fold(20151125L) { code, _ -> (code * 252533) % 33554393 }
    }
    println("Part 1: $answer1")
}
