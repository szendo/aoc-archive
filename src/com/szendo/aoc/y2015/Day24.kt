package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/24.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.toInt() }

    fun getPackages(partial: List<Int>, queue: List<Int>, remaining: Int): List<List<Int>> {
        return if (queue.sum() == remaining) {
            listOf(partial + queue)
        } else if (queue.sum() < remaining) {
            listOf()
        } else {
            val next = queue.first()
            if (next > remaining) {
                getPackages(partial, queue.drop(1), remaining)
            } else {
                getPackages(partial + next, queue.drop(1), remaining - next) +
                    getPackages(partial, queue.drop(1), remaining)
            }
        }
    }

    val answer1 = run {
        val packages = getPackages(listOf(), input.sortedDescending(), input.sumOf { it } / 3)
        val minPackageCount = packages.minOf { it.size }
        packages.filter { it.size == minPackageCount }
            .minOf { it.fold(1L, Long::times) }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val packages = getPackages(listOf(), input.sortedDescending(), input.sumOf { it } / 4)
        val minPackageCount = packages.minOf { it.size }
        packages.filter { it.size == minPackageCount }
            .minOf { it.fold(1L, Long::times) }
    }
    println("Part 2: $answer2")
}
