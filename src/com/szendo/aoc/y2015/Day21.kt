package com.szendo.aoc.y2015

import com.szendo.aoc.*
import kotlin.math.max

fun main() {
    val (bossHp, bossAtk, bossDef) = Helper.getResourceAsStream("2015/21.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { l -> l.split(": ").let { it[1].toInt() } }.toList() }

    fun doesPlayerWin(hp: Int, atk: Int, def: Int, bossHp: Int, bossAtk: Int, bossDef: Int): Boolean {
        val turns = -(-bossHp).floorDiv(max(atk - bossDef, 1))
        return hp > (turns - 1) * max(bossAtk - def, 1)
    }

    val weapons = listOf(
        8 to 4,
        10 to 5,
        25 to 6,
        40 to 7,
        74 to 8,
    )

    val armor = listOf(
        0 to 0,
        13 to 1,
        31 to 2,
        53 to 3,
        75 to 4,
        102 to 5,
    )

    val rings = listOf(
        Triple(0, 0, 0),
        Triple(0, 0, 0),
        Triple(25, 1, 0),
        Triple(50, 2, 0),
        Triple(100, 3, 0),
        Triple(20, 0, 1),
        Triple(40, 0, 2),
        Triple(80, 0, 3),
    )

    val answer1 = run {
        weapons.flatMap { (wCost, wAtk) ->
            armor.flatMap { (aCost, aDef) ->
                rings.indices.flatMap { r1i ->
                    val (r1Cost, r1Atk, r1Def) = rings[r1i]
                    rings.drop(r1i + 1).map { (r2Cost, r2Atk, r2Def) ->
                        val cost = wCost + aCost + r1Cost + r2Cost
                        val atk = wAtk + r1Atk + r2Atk
                        val def = aDef + r1Def + r2Def
                        cost to doesPlayerWin(100, atk, def, bossHp, bossAtk, bossDef)
                    }.filter { it.second }.map { it.first }
                }
            }
        }.minOf { it }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        weapons.flatMap { (wCost, wAtk) ->
            armor.flatMap { (aCost, aDef) ->
                rings.indices.flatMap { r1i ->
                    val (r1Cost, r1Atk, r1Def) = rings[r1i]
                    rings.drop(r1i + 1).map { (r2Cost, r2Atk, r2Def) ->
                        val cost = wCost + aCost + r1Cost + r2Cost
                        val atk = wAtk + r1Atk + r2Atk
                        val def = aDef + r1Def + r2Def
                        cost to doesPlayerWin(100, atk, def, bossHp, bossAtk, bossDef)
                    }.filter { !it.second }.map { it.first }
                }
            }
        }.maxOf { it }
    }
    println("Part 2: $answer2")
}
