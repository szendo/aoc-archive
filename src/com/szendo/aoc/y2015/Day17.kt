package com.szendo.aoc.y2015

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2015/17.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.toInt() }.sortedDescending()

    fun combinationCount(amount: Int = 150, containers: List<Int>): Int =
        if (containers.isEmpty()) if (amount > 0) 0 else 1
        else combinationCount(amount, containers.drop(1)) +
            if (amount >= containers[0]) combinationCount(amount - containers[0], containers.drop(1)) else 0

    val answer1 = run {
        combinationCount(150, input)
    }
    println("Part 1: $answer1")

    fun combinations(amount: Int = 150, containers: List<Int>): List<List<Int>> =
        if (containers.isEmpty()) if (amount > 0) emptyList() else listOf(emptyList())
        else combinations(amount, containers.drop(1)) +
            if (amount >= containers[0]) combinations(amount - containers[0], containers.drop(1)).map { listOf(containers[0]) + it }
            else emptyList()

    val answer2 = run {
        val combinations = combinations(150, input)
        val minContainerCount = combinations.minOf { it.size }
        combinations.count { it.size == minContainerCount }
    }
    println("Part 2: $answer2")
}
