package com.szendo.aoc.y2020

import com.szendo.aoc.Helper
import com.szendo.aoc.split

fun main() {
    val (rules, messages) = Helper.getResourceAsStream("2020/19.txt")
        .bufferedReader()
        .useLines { lines -> lines.split("").toList() }

    val ruleMap = rules.map {
        val (n, r) = it.split(": ", limit = 2)
        n.toInt() to r
    }.toMap().toMutableMap()

    val answer1 = run {
        val ruleCache = mutableMapOf<Int, String>()

        fun evalRule(n: Int): String {
            if (n in ruleCache) return ruleCache.getValue(n)

            when (val rule = ruleMap.getValue(n)) {
                "\"a\"", "\"b\"" -> {
                    ruleCache[n] = rule.substring(1, 2)
                }
                else -> {
                    ruleCache[n] = rule.split(" | ")
                        .joinToString("|", "(?:", ")") {
                            it.split(" ").joinToString("") { evalRule(it.toInt()) }
                        }
                }
            }

            return ruleCache.getValue(n)
        }

        val regex = Regex(evalRule(0))
        messages.count { regex.matches(it) }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        // both '42'-blocks and '31'-blocks have a length of 8
        // 8: 42 | 42 8 -> at least one '42'-block
        // 11: 42 31 | 42 11 31 -> some (>0) '42'-blocks followed by the same amount of '31'-blocks
        // 0: 8 11 -> some (>1) '42'-blocks followed by less (but >0) '31'-blocks

        fun getValidMessages(n: Int): List<String> {
            return when (val rule = ruleMap.getValue(n)) {
                "\"a\"", "\"b\"" -> listOf(rule.substring(1, 2))
                else -> {
                    rule.split(" | ").flatMap { part ->
                        part.split(' ')
                            .map { getValidMessages(it.toInt()) }
                            .reduce { acc, list ->
                                acc.flatMap { s1 -> list.map { s2 -> s1 + s2 } }
                            }
                    }
                }
            }
        }

        val valid42 = getValidMessages(42)
        val valid31 = getValidMessages(31)

        messages.count { msg ->
            val blockCount = msg.length / 8
            (1..(blockCount - 1) / 2).any { b31Count ->
                val b42Count = blockCount - b31Count
                (0 until b42Count).all { bIndex ->
                    msg.substring(bIndex * 8, (bIndex + 1) * 8) in valid42
                } && (b42Count until blockCount).all { bIndex ->
                    msg.substring(bIndex * 8, (bIndex + 1) * 8) in valid31
                }
            }
        }
    }
    println("Part 2: $answer2")
}
