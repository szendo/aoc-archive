package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    data class Pw(val range: IntRange, val c: Char, val pw: String)

    val pattern = Regex("(\\d+)-(\\d+) ([a-z]): ([a-z]+)")

    val input = Helper.getResourceAsStream("2020/2.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { pattern.matchEntire(it)!!.groupValues }
                .map { Pw(it[1].toInt()..it[2].toInt(), it[3][0], it[4]) }
                .toList()
        }

    val answer1 = input.count {
        it.pw.count { c -> c == it.c } in it.range
    }
    println("Part 1: $answer1")

    val answer2 = input.count {
        (it.pw.length >= it.range.first && it.pw[it.range.first - 1] == it.c) xor (it.pw.length >= it.range.last && it.pw[it.range.last - 1] == it.c)
    }
    println("Part 2: $answer2")
}
