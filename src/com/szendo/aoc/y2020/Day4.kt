package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val requiredFields = listOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")

    val input = Helper.getResourceAsStream("2020/4.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.toList()
        }

    val passports = mutableListOf<String>()
    run {
        var currentPassport = ""
        for (line in input) {
            if (line.isEmpty()) {
                passports.add(currentPassport)
                currentPassport = ""
            } else {
                currentPassport = if (currentPassport.isEmpty()) line else "$currentPassport $line"
            }
        }
        if (currentPassport.isNotEmpty()) {
            passports.add(currentPassport)
        }
    }

    val answer1 = passports.count {
        requiredFields.all { f -> it.contains("$f:") }
    }
    println("Part 1: $answer1")

    val answer2 = passports.count {
        if (!requiredFields.all { f -> it.contains("$f:") }) {
            return@count false
        }

        val pp = it.split(' ')
            .map { f ->
                val split = f.split(':', limit = 2)
                split[0] to split[1]
            }.toMap()

        return@count pp["byr"]!!.toInt() in 1920..2002 &&
                pp["iyr"]!!.toInt() in 2010..2020 &&
                pp["eyr"]!!.toInt() in 2020..2030 &&
                pp["hgt"]!!.let { hgt ->
                    Regex("(\\d+)(cm|in)").matchEntire(hgt)?.let { hgtMatch ->
                        when (hgtMatch.groupValues[2]) {
                            "cm" -> hgtMatch.groupValues[1].toInt() in 150..193
                            "in" -> hgtMatch.groupValues[1].toInt() in 59..76
                            else -> false
                        }
                    } ?: false
                } &&
                pp["hcl"]!!.matches(Regex("^#[0-9a-f]{6}$")) &&
                pp["ecl"]!! in "amb blu brn gry grn hzl oth".split(' ') &&
                pp["pid"]!!.matches(Regex("^[0-9]{9}$"))
    }
    println("Part 2: $answer2")

}
