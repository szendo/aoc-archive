package com.szendo.aoc.y2020

import com.szendo.aoc.Helper
import com.szendo.aoc.split

fun main() {
    val (ruleLines, own, nearby) = Helper.getResourceAsStream("2020/16.txt")
        .bufferedReader()
        .useLines { lines -> lines.split("").toList() }

    val rulesRegex = Regex("^([^:]+): (\\d+)-(\\d+) or (\\d+)-(\\d+)$")
    val rules = ruleLines.map { rule ->
        rulesRegex.find(rule)!!.groupValues.drop(1).let { (field, s1, e1, s2, e2) ->
            field to ((s1.toInt()..e1.toInt()) to (s2.toInt()..e2.toInt()))
        }
    }.toMap()
    val ownTicket = own[1].split(',').map { it.toInt() }
    val nearbyTickets = nearby.drop(1).map { it.split(',').map { it.toInt() } }
    val validNearbyTickets = nearbyTickets.filter { ticket ->
        ticket.none { v -> rules.none { (_, ranges) -> v in ranges.first || v in ranges.second } }
    }

    val answer1 = nearbyTickets.sumOf { ticket ->
        ticket.sumOf { v -> if (rules.none { (_, ranges) -> v in ranges.first || v in ranges.second }) v else 0 }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val possibleFieldMap = mutableMapOf<String, Set<Int>>()
        val colCount = ownTicket.size

        repeat(colCount) { col ->
            rules.forEach { (field, ranges) ->
                if (validNearbyTickets.all { ticket -> ticket[col] in ranges.first || ticket[col] in ranges.second }) {
                    possibleFieldMap.merge(field, setOf(col)) { s1, s2 -> s1 + s2 }
                }

            }
        }

        val fieldMap = mutableMapOf<String, Int>()
        while (possibleFieldMap.isNotEmpty()) {
            possibleFieldMap.filterValues { it.size == 1 }.entries.forEach { (field, cols) ->
                fieldMap[field] = possibleFieldMap.remove(field)!!.toList()[0]
                possibleFieldMap.keys.forEach { otherField ->
                    possibleFieldMap[otherField] = possibleFieldMap[otherField]!! - cols
                }
            }

        }

        fieldMap.keys.filter { it.startsWith("departure ") }
            .map { cls -> ownTicket[fieldMap[cls]!!].toLong() }
            .reduce { acc, i -> acc * i }
    }
    println("Part 2: $answer2")
}
