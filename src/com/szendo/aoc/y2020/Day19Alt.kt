package com.szendo.aoc.y2020

import com.szendo.aoc.Helper
import com.szendo.aoc.split

fun main() {
    val (rules, messages) = Helper.getResourceAsStream("2020/19.txt")
        .bufferedReader()
        .useLines { lines -> lines.split("").toList() }

    val ruleMap = rules.map {
        val (n, r) = it.split(": ", limit = 2)
        n.toInt() to r
    }.toMap().toMutableMap()

    // part 2 solution "backported" to part 1
    val answer1 = run {
        // 8: 42
        // 11: 42 31
        // 0: 8 11 -> 42 42 31

        fun getValidMessages(n: Int): List<String> {
            return when (val rule = ruleMap.getValue(n)) {
                "\"a\"", "\"b\"" -> listOf(rule.substring(1, 2))
                else -> {
                    rule.split(" | ").flatMap { part ->
                        part.split(' ')
                            .map { getValidMessages(it.toInt()) }
                            .reduce { acc, list ->
                                acc.flatMap { s1 -> list.map { s2 -> s1 + s2 } }
                            }
                    }
                }
            }
        }

        val valid42 = getValidMessages(42)
        val valid31 = getValidMessages(31)

        // `msg in valid0` also works but is much slower
        messages.count { msg ->
            msg.length == 24 &&
                    msg.substring(0, 8) in valid42 &&
                    msg.substring(8, 16) in valid42 &&
                    msg.substring(16, 24) in valid31
        }
    }
    println("Part 1: $answer1")
}
