package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/15.txt")
        .bufferedReader()
        .useLines { lines -> lines.first().split(',').map { it.toInt() } }

    run {
        val lastSpoken = hashMapOf(*input.mapIndexed { index, value ->
            value to (index + 1)
        }.toTypedArray())

        var lastNumber = input.last()
        var turn = input.size + 1
        while (turn <= 30_000_000) {
            val newNumber = turn - 1 - lastSpoken.getOrDefault(lastNumber, turn - 1)
            lastSpoken[lastNumber] = turn - 1
            lastNumber = newNumber
            turn++
        }

        lastNumber
    }.let { println("Part 2: $it") }

    run {
        val lastSpoken = IntArray(30_000_000).apply {
            input.forEachIndexed { index, value -> this[value] = index + 1 }
        }

        var lastNumber = input.last()
        var turn = input.size + 1
        while (turn <= 30_000_000) {
            val newNumber = turn - 1 - lastSpoken[lastNumber].let { if (it == 0) turn - 1 else it }
            lastSpoken[lastNumber] = turn - 1
            lastNumber = newNumber
            turn++
        }

        lastNumber
    }.let { println("Part 2: $it") }
}
