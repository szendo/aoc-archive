package com.szendo.aoc.y2020

import com.szendo.aoc.*
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

fun main() {
    val input = Helper.getResourceAsStream("2020/20.txt")
        .bufferedReader()
        .useLines { lines -> lines.split("").toList() }

    val tiles: Map<Int, Grid<Char>> = input.map {
        it.first().substring(5, 9).toInt() to it.drop(1).map { it.toList() }
    }.toMap()

    run {
        fun applyFlipRot(tile: Grid<Char>, flip: Boolean, rot: Int) = tile.let {
            if (flip) it.map { it.reversed() } else it
        }.let {
            when (rot) {
                0 -> it
                1 -> (0 until it.size).map { y ->
                    (0 until it.size).map { x ->
                        it[it.lastIndex - x][y]
                    }
                }
                2 -> it.reversed().map { it.reversed() }
                3 -> (0 until it.size).map { y ->
                    (0 until it.size).map { x ->
                        it[x][it.lastIndex - y]
                    }
                }
                else -> error("Unknown rotation: $rot")
            }
        }

        fun getRightEdge(tile: Grid<Char>) = tile.map { it.last() }
        fun getBottomEdge(tile: Grid<Char>) = tile.last()
        fun getLeftEdge(tile: Grid<Char>) = tile.map { it.first() }
        fun getTopEdge(tile: Grid<Char>) = tile.first()

        val edgePairs = listOf(
            ::getRightEdge to ::getLeftEdge,
            ::getBottomEdge to ::getTopEdge,
            ::getLeftEdge to ::getRightEdge,
            ::getTopEdge to ::getBottomEdge
        )

        val offCoords = setOf(
            ::getRightEdge to (1 to 0),
            ::getBottomEdge to (0 to 1),
            ::getLeftEdge to (-1 to 0),
            ::getTopEdge to (0 to -1)
        ).toMap()


        val startingTile = tiles.keys.first()

        val tilesLeft = tiles.keys.toMutableSet()
        tilesLeft.remove(startingTile)

        val tileIndices = mutableMapOf(
            startingTile to ((0 to 0) to false to 0) // tile id to coord to flip to rotateCwTimes
        )

        val tilesQueued = mutableListOf(startingTile)


        while (tilesQueued.isNotEmpty()) {
            val refTileId = tilesQueued.removeAt(0)
            val (_refCoordFlip, refRot) = tileIndices.getValue(refTileId)
            val (refCoord, refFlip) = _refCoordFlip
            val refTile = applyFlipRot(tiles.getValue(refTileId), refFlip, refRot)

            edgePairs.forEach { (refGetter, otherGetter) ->
                val deltaCoord = offCoords.getValue(refGetter)

                val refRightEdge = refGetter(refTile)
                val tileToRemove = mutableSetOf<Int>()

                tilesLeft.forEach { otherTileId ->
                    val otherTile = tiles.getValue(otherTileId)
                    listOf(false, true).forEach { flip ->
                        (0..3).forEach { rot ->
                            val otherLeftEdge = otherGetter(applyFlipRot(otherTile, flip, rot))
                            if (refRightEdge == otherLeftEdge) {
                                val otherCoord = refCoord.plus(deltaCoord)
                                tileToRemove.add(otherTileId)
                                tileIndices[otherTileId] = (otherCoord to flip to rot)
                                tilesQueued.add(otherTileId)
                            }
                        }
                    }
                }
                tilesLeft.removeAll(tileToRemove)
            }
        }

        val minX = tileIndices.values.minOf { it.first.first.x }
        val maxX = tileIndices.values.maxOf { it.first.first.x }
        val minY = tileIndices.values.minOf { it.first.first.y }
        val maxY = tileIndices.values.maxOf { it.first.first.y }

        val image = (minY..maxY).flatMap { y ->
            (minX..maxX).map { x ->
                val e = tileIndices.filterValues { it.first.first == (x to y) }
                val (tileId, _data) = e.entries.first()
                val (_data2, rot) = _data
                val (_coord, flip) = _data2
                val tile = applyFlipRot(tiles.getValue(tileId), flip, rot)
                tile.drop(1).dropLast(1).map { it.drop(1).dropLast(1) }
            }.reduce { rows, newTile -> rows.zip(newTile).map { (a, b) -> a.plus(b) } }
        }

        //                  #
        //#    ##    ##    ###
        // #  #  #  #  #  #
        val monsterPattern = listOf(
            18 to 0,
            0 to 1,
            5 to 1,
            6 to 1,
            11 to 1,
            12 to 1,
            17 to 1,
            18 to 1,
            19 to 1,
            1 to 2,
            4 to 2,
            7 to 2,
            10 to 2,
            13 to 2,
            16 to 2
        )

        fun getMonsterCoords(image: Grid<Char>) =
            (0 until image.size - monsterPattern.maxOf { it.y }).flatMap { y ->
                (0 until image[0].size - monsterPattern.maxOf { it.x }).flatMap { x ->
                    val coord = x to y
                    if (monsterPattern.all { image[coord + it] == '#' }) {
                        monsterPattern.map { it + coord }
                    } else {
                        emptyList()
                    }
                }

            }

        listOf(false, true).forEach { flip ->
            (0..3).forEach { rot ->
                val correctImage = applyFlipRot(image, flip, rot)
                val monsterCoords = getMonsterCoords(correctImage)
                if (monsterCoords.isNotEmpty()) {
                    val bufImg = BufferedImage(correctImage[0].size, correctImage.size, BufferedImage.TYPE_INT_RGB)
                    image.forEachIndexed { y, row ->
                        row.forEachIndexed { x, cell ->
                            bufImg.setRGB(
                                x, y,
                                when {
                                    (x to y) in monsterCoords -> 0xff0000
                                    cell == '#' -> 0x000099
                                    else -> 0x000ff
                                }
                            )
                        }
                    }
                    ImageIO.write(bufImg, "png", File("20.png"))
                    return
                }
            }
        }
    }
}
