package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/5.txt")
        .bufferedReader()
        .useLines { lines ->
            lines
                .map {
                    // Integer.parseInt(it.replace(Regex("[FL]"), "0").replace(Regex("[BR]"), "1"), 2)
                    it.fold(0) { v, c ->
                        2 * v + when (c) {
                            'F', 'L' -> 0
                            'B', 'R' -> 1
                            else -> error("Invalid characher: $c")
                        }

                    }
                }
                .toList()
        }

    val answer1 = input.maxOf { it }
    println("Part 1: $answer1")

    val answer2 = input.sorted().zipWithNext().first { it.first + 1 != it.second }.first + 1
    println("Part 2: $answer2")
}
