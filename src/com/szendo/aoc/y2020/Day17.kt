package com.szendo.aoc.y2020

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2020/17.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val answer1 = run {
        var activeCells: Set<Coord3D> = input.flatMapIndexed { y, row ->
            row.mapIndexed { x, c ->
                Coord3D(x, y, 0) to c
            }.filter { it.second == '#' }.map { it.first }
        }.toSet()

        repeat(6) { _ ->
            if (activeCells.isEmpty()) {
                return@repeat
            }

            val xRange = activeCells.minOf { it.x } - 1..activeCells.maxOf { it.x } + 1
            val yRange = activeCells.minOf { it.y } - 1..activeCells.maxOf { it.y } + 1
            val zRange = activeCells.minOf { it.z } - 1..activeCells.maxOf { it.z } + 1

            val newActiveCells = mutableSetOf<Coord3D>()

            xRange.forEach { x ->
                yRange.forEach { y ->
                    zRange.forEach { z ->
                        val coord = Coord3D(x, y, z)
                        val activeCountWithSelf = (-1..1).sumOf { dx ->
                            (-1..1).sumOf { dy ->
                                (-1..1).count { dz ->
                                    (coord + Coord3D(dx, dy, dz)) in activeCells
                                }
                            }
                        }
                        if (coord in activeCells) {
                            if (activeCountWithSelf in (3..4)) {
                                newActiveCells.add(coord)
                            }
                        } else if (activeCountWithSelf == 3) {
                            newActiveCells.add(coord)
                        }
                    }
                }
            }
            activeCells = newActiveCells
        }

        activeCells.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var activeCells: Set<Coord4D> = input.flatMapIndexed { y, row ->
            row.mapIndexed { x, c ->
                Coord4D(x, y, 0, 0) to c
            }.filter { it.second == '#' }.map { it.first }
        }.toSet()

        repeat(6) { _ ->
            if (activeCells.isEmpty()) {
                return@repeat
            }

            val xRange = activeCells.minOf { it.x } - 1..activeCells.maxOf { it.x } + 1
            val yRange = activeCells.minOf { it.y } - 1..activeCells.maxOf { it.y } + 1
            val zRange = activeCells.minOf { it.z } - 1..activeCells.maxOf { it.z } + 1
            val wRange = activeCells.minOf { it.m } - 1..activeCells.maxOf { it.m } + 1

            val newActiveCells = mutableSetOf<Coord4D>()

            xRange.forEach { x ->
                yRange.forEach { y ->
                    zRange.forEach { z ->
                        wRange.forEach { w ->
                            val coord = Coord4D(x, y, z, w)
                            val activeCountWithSelf = (-1..1).sumOf { dx ->
                                (-1..1).sumOf { dy ->
                                    (-1..1).sumOf { dz ->
                                        (-1..1).count { dw ->
                                            (coord + Coord4D(dx, dy, dz, dw)) in activeCells
                                        }
                                    }
                                }
                            }
                            if (coord in activeCells) {
                                if (activeCountWithSelf in (3..4)) {
                                    newActiveCells.add(coord)
                                }
                            } else if (activeCountWithSelf == 3) {
                                newActiveCells.add(coord)
                            }
                        }
                    }
                }
            }
            activeCells = newActiveCells
        }

        activeCells.size
    }
    println("Part 2: $answer2")
}
