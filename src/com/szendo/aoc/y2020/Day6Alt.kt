package com.szendo.aoc.y2020

import com.szendo.aoc.Helper
import com.szendo.aoc.split

fun main() {
    val input = Helper.getResourceAsStream("2020/6.txt").bufferedReader()
        .useLines { lines -> lines.split("").map { it.map(String::toSet) } }
    println("Part 1: ${input.sumOf { it.reduce(Set<Char>::plus).size }}")
    println("Part 2: ${input.sumOf { it.reduce(Set<Char>::intersect).size }}")
}
