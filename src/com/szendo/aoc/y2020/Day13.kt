package com.szendo.aoc.y2020

import com.szendo.aoc.*

fun main() {
    val (earliestTs, buses) = Helper.getResourceAsStream("2020/13.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList().let { it[0].toInt() to it[1].split(',') } }

    val answer1 = run {
        val routes = buses.filter { it != "x" }.map { it.toInt() }
        var ts = earliestTs
        while (routes.none { ts % it == 0 }) ts++
        (ts - earliestTs) * routes.single { ts % it == 0 }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val reqs = buses.asSequence().withIndex()
            .filter { it.value != "x" }
            .map { IndexedValue(it.index, it.value.toLong()) }
            .groupingBy { it.index % it.value }
            .fold(1L) { acc, e -> lcm(acc, e.value) }

        val (offset, skipTs) = reqs.maxByOrNull { it.value }!!
        // var currTs =  skipTs - offset // for example input
        var ts = skipTs - offset + (100000000000000 / skipTs) * skipTs
        while (!reqs.all { (ts + it.key) % it.value == 0L }) ts += skipTs
        ts
    }
    println("Part 2: $answer2")
}
