package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/18.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val answer1 = run {
        fun eval(expr: String, startPos: Int = 0): Pair<Long, Int> {
            var pos = startPos
            var value = 0L
            var lastOp = '+'

            fun applyOp(op: Char, arg1: Long, arg2: Long) = when (op) {
                '+' -> arg1 + arg2
                '*' -> arg1 * arg2
                else -> error("Unknown operator: $op")
            }

            while (pos < expr.length) {
                when (val token = expr[pos]) {
                    ' ' -> {
                        pos++
                    }
                    in '0'..'9' -> {
                        value = applyOp(lastOp, value, (token - '0').toLong())
                        pos++
                    }
                    '+', '*' -> {
                        lastOp = token
                        pos++
                    }
                    '(' -> {
                        val (subExprValue, endPos) = eval(expr, pos + 1)
                        value = applyOp(lastOp, value, subExprValue)
                        pos = endPos + 1
                    }
                    ')' -> {
                        return value to pos
                    }
                    else -> error("Unknown token: $token")
                }
            }

            return value to pos
        }

        input.map { eval(it).first }.sum()
    }
    println("Part 1: $answer1")

    val answer2 = run {
        fun eval(expr: String, startPos: Int = 0): Pair<Long, Int> {
            var pos = startPos
            val tokens = mutableListOf<Any>()

            fun evalOp(op: Char, opFn: (Long, Long) -> Long) {
                while (true) {
                    val opIndex = tokens.indexOf(op)
                    if (opIndex == -1) {
                        break
                    }

                    val value = opFn(tokens[opIndex - 1] as Long, tokens[opIndex + 1] as Long)
                    tokens.removeAt(opIndex + 1)
                    tokens.removeAt(opIndex)
                    tokens.removeAt(opIndex - 1)
                    tokens.add(opIndex - 1, value)
                }
            }

            fun evalTokens(): Long {
                evalOp('+', Long::plus)
                evalOp('*', Long::times)
                return tokens[0] as Long
            }

            while (pos < expr.length) {
                when (val token = expr[pos]) {
                    ' ' -> {
                        pos++
                    }
                    in '0'..'9' -> {
                        tokens.add((token - '0').toLong())
                        pos++
                    }
                    '+', '*' -> {
                        tokens.add(token)
                        pos++
                    }
                    '(' -> {
                        val (value, endPos) = eval(expr, pos + 1)
                        tokens.add(value)
                        pos = endPos + 1
                    }
                    ')' -> {
                        return evalTokens() to pos
                    }
                    else -> error("Unknown token: $token")
                }
            }

            return evalTokens() to pos
        }

        input.map { eval(it).first }.sum()
    }
    println("Part 2: $answer2")
}
