package com.szendo.aoc.y2020

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2020/12.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it[0] to it.substring(1).toInt() }.toList() }

    val answer1 = run {
        var pos = 0 to 0
        var dir = 1 to 0

        input.forEach { (action, value) ->
            when (action) {
                'N' -> pos += (0 to 1) * value
                'S' -> pos += (0 to -1) * value
                'E' -> pos += (1 to 0) * value
                'W' -> pos += (-1 to 0) * value
                'L' -> dir = when (value % 360) {
                    90 -> dir.rotateCCW()
                    180 -> -dir
                    270 -> dir.rotateCW()
                    else -> error("Unknown value: $value")
                }
                'R' -> dir = when (value % 360) {
                    90 -> dir.rotateCW()
                    180 -> -dir
                    270 -> dir.rotateCCW()
                    else -> error("Unknown value: $value")
                }
                'F' -> pos += dir * value
                else -> error("Unknwon action: $action")
            }
        }

        pos.distance(0 to 0)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var pos = 0 to 0
        var wp = 10 to 1

        input.forEach { (action, value) ->
            when (action) {
                'N' -> wp += (0 to 1) * value
                'S' -> wp += (0 to -1) * value
                'E' -> wp += (1 to 0) * value
                'W' -> wp += (-1 to 0) * value
                'L' -> wp = when (value % 360) {
                    90 -> wp.rotateCCW()
                    180 -> -wp
                    270 -> wp.rotateCW()
                    else -> error("Unknown value: $value")
                }
                'R' -> wp = when (value % 360) {
                    90 -> wp.rotateCW()
                    180 -> -wp
                    270 -> wp.rotateCCW()
                    else -> error("Unknown value: $value")
                }
                'F' -> pos += wp * value
                else -> error("Unknwon action: $action")
            }
        }

        pos.distance(0 to 0)
    }
    println("Part 2: $answer2")
}
