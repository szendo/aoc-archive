package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/1.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.toInt() }.toList() }

    val answer1 = input.flatMap { n1 ->
        input.filter { n1 < it }
            .filter { n2 -> n1 + n2 == 2020 }.map { n2 -> n1 * n2 }
    }.first()
    println("Part 1: $answer1")

    val answer2 = input.flatMap { n1 ->
        input.filter { n1 < it }
            .flatMap { n2 ->
                input.filter { n2 < it }
                    .filter { n3 -> n2 < n3 && n1 + n2 + n3 == 2020 }
                    .map { n3 -> n1 * n2 * n3 }
            }
    }.first()
    println("Part 2: $answer2")
}
