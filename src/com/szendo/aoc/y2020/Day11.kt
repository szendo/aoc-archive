package com.szendo.aoc.y2020

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2020/11.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.toList() }.toList() }

    fun Grid<Char>.stepUntilStable(emptyThreshold: Int, getSeatInDir: Grid<Char>.(Coord, Coord) -> Char): Grid<Char> {
        fun Grid<Char>.step(emptyThreshold: Int, seatInDir: Grid<Char>.(Coord, Coord) -> Char) =
            this.indices.map { y ->
                this[y].indices.map { x ->
                    val pos = x to y
                    when (this[pos]) {
                        '.' -> '.'
                        'L' -> {
                            if ((-1..1).all { dy ->
                                    (-1..1).all { dx ->
                                        seatInDir(pos, dx to dy) != '#'
                                    }
                                }) '#' else 'L'
                        }
                        '#' -> {
                            if ((-1..1).sumOf { dy ->
                                    (-1..1).count { dx ->
                                        seatInDir(pos, dx to dy) == '#'
                                    }
                                } > emptyThreshold) 'L' else '#'
                        }
                        else -> error("Invalid grid item: ${this[pos]}")
                    }
                }
            }

        var curr = this
        do {
            val prev = curr
            curr = prev.step(emptyThreshold, getSeatInDir)
        } while (prev != curr)
        return curr
    }

    val answer1 = input.stepUntilStable(4) { pos, dir ->
        try {
            this[pos + dir]
        } catch (e: IndexOutOfBoundsException) {
            '.'
        }
    }.sumOf { row -> row.count { c -> c == '#' } }
    println("Part 1: $answer1")

    val answer2 = input.stepUntilStable(5) { pos, dir ->
        if (dir == 0 to 0) this[pos.y][pos.x]
        else try {
            var p = pos + dir
            while (this[p] == '.') {
                p += dir
            }
            this[p]
        } catch (e: IndexOutOfBoundsException) {
            '.'
        }
    }.sumOf { row -> row.count { c -> c == '#' } }
    println("Part 2: $answer2")
}
