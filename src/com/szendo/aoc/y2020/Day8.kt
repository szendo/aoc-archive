package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/8.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map {
                val (instr, arg) = it.split(' ')
                return@map instr to arg.toInt()
            }
                .toList()
        }

    val answer1 = run {
        var ip = 0
        var acc = 0
        val executedInstrs = mutableSetOf<Int>()

        while (executedInstrs.add(ip)) {
            val (instr, arg) = input[ip]
            when (instr) {
                "acc" -> {
                    acc += arg
                    ip++
                }
                "jmp" -> {
                    ip += arg
                }
                "nop" -> {
                    ip++
                }
                else -> error("Unknown instruction: $instr $arg")
            }
        }

        acc
    }
    println("Part 1: $answer1")

    val answer2 = input.filter { it.first != "acc" }.indices
        .map { corruptedIp ->
            var ip = 0
            var acc = 0

            val executedInstrs = mutableSetOf<Int>()
            while (ip in input.indices && executedInstrs.add(ip)) {
                val (instr, arg) = input[ip]
                when (instr) {
                    "acc" -> {
                        acc += arg
                        ip++
                    }
                    "jmp" -> {
                        ip += if (ip == corruptedIp) 1 else arg
                    }
                    "nop" -> {
                        ip += if (ip == corruptedIp) arg else 1
                    }
                    else -> error("Unknown instruction: $instr $arg")
                }
            }

            (ip !in input.indices) to acc
        }.first { it.first }.second
    println("Part 2: $answer2")
}
