package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/21.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val allIngredients = mutableMapOf<String, Int>()
    val allergenMap = mutableMapOf<String, MutableSet<String>>()

    run {
        val regex = Regex("([a-z ]+) \\(contains ([a-z, ]+)\\)")

        input.forEach {
            val (ingredients, allergens) = regex.matchEntire(it)!!.groupValues.drop(1).let { groups ->
                groups[0].split(" ") to groups[1].split(", ")
            }

            ingredients.forEach { ingredient ->
                allIngredients.merge(ingredient, 1, Int::plus)
            }

            allergens.forEach { allergen ->
                if (allergenMap.containsKey(allergen)) {
                    allergenMap.getValue(allergen).retainAll(ingredients)
                } else {
                    allergenMap[allergen] = ingredients.toMutableSet()
                }
            }
        }

        while (allergenMap.any { it.value.size > 1 }) { // for part 2
            allergenMap.filterValues { it.size == 1 }.forEach { (_, ingredientsToRemove) ->
                allergenMap.filterValues { it.size > 1 }.forEach { (_, ingredients) ->
                    ingredients.removeAll(ingredientsToRemove)
                }
            }
        }
    }

    val answer1 = allergenMap.values.flatten().toSet().let { ingredientsWithAllergen ->
        allIngredients.filterKeys { ingredient -> ingredient !in ingredientsWithAllergen }.values.sum()
    }
    println("Part 1: $answer1")

    val answer2 = allergenMap.toList()
        .sortedBy { it.first }
        .joinToString(",") { it.second.first() }
    println("Part 2: $answer2")
}
