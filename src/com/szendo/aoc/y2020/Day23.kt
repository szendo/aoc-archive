package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/23.txt")
        .bufferedReader()
        .useLines { lines -> lines.first().map { it - '0' } }

    val answer1 = run {
        val cups = input.toMutableList()

        repeat(100) {
            val currentCup = cups[0]
            val removedCups = cups.subList(1, 4).toList()
            cups.removeAt(1)
            cups.removeAt(1)
            cups.removeAt(1)

            var afterCup = currentCup
            do {
                afterCup = if (afterCup == 1) 9 else afterCup - 1
            } while (afterCup !in cups)

            val insertAfterIndex = cups.indexOf(afterCup)
            cups.add(insertAfterIndex + 1, removedCups[2])
            cups.add(insertAfterIndex + 1, removedCups[1])
            cups.add(insertAfterIndex + 1, removedCups[0])

            cups.add(cups.removeAt(0))
        }

        val oneCupIndex = cups.indexOf(1)

        (cups.subList(oneCupIndex + 1, cups.size) + cups.subList(0, oneCupIndex)).joinToString("")
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val cups = List(1_000_000) { if (it > input.lastIndex) (it + 1) else input[it] }

        val cupMap = HashMap((cups.zipWithNext() + (1_000_000 to cups[0])).toMap())

        var currentCup = cups[0]

        repeat(10_000_000) {
            val removed1 = cupMap.getValue(currentCup)
            val removed2 = cupMap.getValue(removed1)
            val removed3 = cupMap.getValue(removed2)
            cupMap[currentCup] = cupMap.getValue(removed3)

            var afterCup = currentCup
            do {
                afterCup = if (afterCup == 1) cups.size else afterCup - 1
            } while (afterCup in setOf(removed1, removed2, removed3))

            cupMap[removed3] = cupMap.getValue(afterCup)
            cupMap[afterCup] = removed1

            currentCup = cupMap.getValue(currentCup)
        }

        val star1 = cupMap.getValue(1)
        val star2 = cupMap.getValue(star1)
        star1.toLong() * star2.toLong()
    }
    println("Part 2: $answer2")
}
