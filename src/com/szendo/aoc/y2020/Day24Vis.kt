package com.szendo.aoc.y2020

import com.szendo.aoc.Helper
import com.szendo.aoc.plus
import java.awt.Color
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

fun main() {
    val regex = Regex("e|se|sw|w|nw|ne")
    val input = Helper.getResourceAsStream("2020/24.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { line -> regex.findAll(line).map { it.value }.toList() }.toList() }

    val dirs = mapOf(
        "e" to (1 to 0),
        "se" to (0 to 1),
        "sw" to (-1 to 1),
        "w" to (-1 to 0),
        "nw" to (0 to -1),
        "ne" to (1 to -1)
    )

    val initialBlackTiles = input.map { dirList ->
        dirList.fold(0 to 0) { coord, dir ->
            coord + dirs.getValue(dir)
        }
    }.groupingBy { it }.eachCount().filter { it.value % 2 == 1 }.keys

    val image = BufferedImage(736, 688, BufferedImage.TYPE_INT_RGB)
    val w = 95
    val h = 80
    val g = image.createGraphics()

    drawTiles(g, w, h, initialBlackTiles)
    ImageIO.write(image, "png", File("24/%08d.png".format(0)))


    var blackTiles = initialBlackTiles
    repeat(100) {
        blackTiles = blackTiles.flatMap { refCoord ->
            dirs.values.map { dir -> refCoord + dir }.filter { coord ->
                dirs.values.count { (it + coord) in blackTiles } in (if (coord in blackTiles) 1..2 else 2..2)
            }
        }.toSet()

        drawTiles(g, w, h, blackTiles)
        ImageIO.write(image, "png", File("24/%08d.png".format(it + 1)))
    }
}

private fun drawTiles(
    g: Graphics2D,
    w: Int,
    h: Int,
    blackTiles: Set<Pair<Int, Int>>
) {
    g.color = Color.WHITE
    g.fillRect(0, 0, 736, 688)
    (-h..h).forEach { y ->
        val yOff = 349 - 4 + y * 5
        ((-w - Math.floorDiv(y, 2))..(w - Math.floorDiv(y, 2))).forEach { x ->
            val xOff = 376 - 2 + 6 * x + 3 * y

            g.color = Color.GRAY
            g.fillRect(xOff, yOff, 1, 1)
            g.fillRect(xOff - 2, yOff + 1, 2, 1)
            g.fillRect(xOff + 1, yOff + 1, 2, 1)
            g.fillRect(xOff - 3, yOff + 2, 1, 4)
            g.fillRect(xOff + 3, yOff + 2, 1, 4)
            g.fillRect(xOff - 2, yOff + 6, 2, 1)
            g.fillRect(xOff + 1, yOff + 6, 2, 1)
            g.fillRect(xOff, yOff + 7, 1, 1)

            if ((x to y) in blackTiles) {
                g.color = Color.BLACK
                g.fillRect(xOff, yOff + 1, 1, 6)
                g.fillRect(xOff - 2, yOff + 2, 5, 4)
            }
        }
    }
}
// convert -delay 10 000000*.png -delay 200 00000100.png -loop 0 ../24.gif

