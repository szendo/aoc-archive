package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/14.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val answer1 = run {
        val memory = mutableMapOf<Long, Long>()
        var andMask = 0L
        var orMask = 0L

        input.forEach { line ->
            if (line.startsWith("mask = ")) {
                val mask = Regex("[01X]+$").find(line)!!.value
                andMask = mask.map { if (it == 'X') '1' else it }.joinToString("").toLong(2)
                orMask = mask.map { if (it == 'X') '0' else it }.joinToString("").toLong(2)

            } else {
                val (_, addr, value) = Regex("mem\\[(\\d+)] = (\\d+)").find(line)!!.groupValues
                memory[addr.toLong()] = (value.toLong() or orMask) and andMask
            }
        }

        memory.values.reduce { acc, l -> acc + l }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val memory = mutableMapOf<Long, Long>()
        var mask = ""

        input.forEach { line ->
            if (line.startsWith("mask = ")) {
                mask = Regex("[01X]+$").find(line)!!.value

            } else {
                val (_, addr, value) = Regex("mem\\[(\\d+)] = (\\d+)").find(line)!!.groupValues

                val addrs = mutableListOf(
                    addr.toLong().toString(2).padStart(36, '0')
                        .zip(mask).map { (a, m) -> if (m == '0') a else m }
                        .joinToString("")
                )

                while (addrs[0].contains('X')) {
                    val a = addrs.removeAt(0)
                    addrs += a.replaceFirst('X', '0')
                    addrs += a.replaceFirst('X', '1')
                }

                addrs.forEach { a ->
                    memory[a.toLong(2)] = value.toLong()
                }
            }
        }

        memory.values.reduce { acc, l -> acc + l }
    }
    println("Part 2: $answer2")
}
