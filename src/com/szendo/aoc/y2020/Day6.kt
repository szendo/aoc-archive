package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/6.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList().map { it.toSet() } }

    val answer1 = run {
        val groups = mutableListOf<Set<Char>>()
        var currentGroup = mutableSetOf<Char>()
        for (line in input) {
            if (line.isEmpty()) {
                groups.add(currentGroup)
                currentGroup = mutableSetOf()
            } else {
                currentGroup.addAll(line)
            }
        }
        groups.add(currentGroup)
        groups.sumOf { it.size }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val groups = mutableListOf<Set<Char>>()
        var currentGroup = ('a'..'z').toMutableSet()
        for (line in input) {
            if (line.isEmpty()) {
                groups.add(currentGroup)
                currentGroup = ('a'..'z').toMutableSet()
            } else {
                currentGroup.retainAll(line)
            }
        }
        groups.add(currentGroup)
        groups.sumOf { it.size }
    }
    println("Part 2: $answer2")
}
