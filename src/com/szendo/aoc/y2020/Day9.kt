package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/9.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.toLong() }.toList() }

    val answer1 = input.windowed(26).first { numbers ->
        (0 until 25).all { i ->
            ((i + 1)..25).all { j ->
                numbers[25] != numbers[i] + numbers[j]
            }
        }
    }.last()
    println("Part 1: $answer1")

    val answer2 = run {
        var start = 0
        while (start < input.size) {
            var i = start
            var sum = input[i]
            var min = input[i]
            var max = input[i]
            do {
                i++
                sum += input[i]
                if (input[i] < min) min = input[i]
                if (input[i] > max) max = input[i]
            } while (sum < answer1)

            if (sum == answer1) {
                return@run min + max
            }
            start++
        }
    }
    println("Part 2: $answer2")
}
