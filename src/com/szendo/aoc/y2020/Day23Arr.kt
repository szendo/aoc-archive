package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/23.txt")
        .bufferedReader()
        .useLines { lines -> lines.first().map { it - '0' } }

    val answer2 = run {
        val cups = IntArray(1_000_000) { n -> (n + 1) % 1_000_000 }

        (listOf(0) + input + (input.size + 1)).zipWithNext().forEach { (curr, next) ->
            cups[curr] = next
        }

        var currentCup = cups[0]
        repeat(10_000_000) {
            val removed1 = cups[currentCup]
            val removed2 = cups[removed1]
            val removed3 = cups[removed2]
            cups[currentCup] = cups[removed3]

            var afterCup = currentCup
            do {
                afterCup = (if (afterCup == 0) cups.size else afterCup) - 1
            } while (afterCup == removed1 || afterCup == removed2 || afterCup == removed3)

            cups[removed3] = cups[afterCup]
            cups[afterCup] = removed1

            currentCup = cups[currentCup]
        }

        (if (cups[1] == 0) cups.size else cups[1]).toLong() * (if (cups[cups[1]] == 0) cups.size else cups[cups[1]])
    }
    println("Part 2: $answer2")
}
