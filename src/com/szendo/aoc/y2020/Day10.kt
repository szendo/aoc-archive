package com.szendo.aoc.y2020

import com.szendo.aoc.Helper
import com.szendo.aoc.rle

fun main() {
    val input = Helper.getResourceAsStream("2020/10.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.toInt() }.toList() }

    val answer1 = (input + listOf(0, input.maxOf { it } + 3))
        .sorted().zipWithNext { a, b -> b - a }
        .let { diffs -> diffs.count { it == 1 } * diffs.count { it == 3 } }
    println("Part 1: $answer1")

    val answer2 = (input + listOf(0, input.maxOf { it } + 3))
        .sorted().asSequence().zipWithNext { a, b -> b - a }.rle()
        .onEach { if (it.second !in listOf(1, 3)) error("Unsupported difference: ${it.second}") }
        .filter { it.second == 1 }
        .map {
            when (it.first) {
                1 -> 1L
                2 -> 2L
                3 -> 4L
                4 -> 7L
                else -> error("Unsupported run-length of 1s: ${it.first}")
            }
        }
        .reduce { acc, l -> acc * l }
    println("Part 2: $answer2")
}
