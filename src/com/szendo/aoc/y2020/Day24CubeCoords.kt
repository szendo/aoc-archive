package com.szendo.aoc.y2020

import com.szendo.aoc.Coord3D
import com.szendo.aoc.Helper
import com.szendo.aoc.plus

fun main() {
    val regex = Regex("e|se|sw|w|nw|ne")
    val input = Helper.getResourceAsStream("2020/24.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { line -> regex.findAll(line).map { it.value }.toList() }.toList() }

    val dirs = mapOf(
        "e" to Coord3D(1, -1, 0),
        "se" to Coord3D(0, -1, 1),
        "sw" to Coord3D(-1, 0, 1),
        "w" to Coord3D(-1, 1, 0),
        "nw" to Coord3D(0, 1, -1),
        "ne" to Coord3D(1, 0, -1)
    )

    val initialBlackTiles = input.map { dirList ->
        dirList.fold(Coord3D(0,0,0)) { coord, dir ->
            coord + dirs.getValue(dir)
        }
    }.groupingBy { it }.eachCount().filter { it.value % 2 == 1 }.keys

    val answer1 = initialBlackTiles.size
    println("Part 1: $answer1")

    val answer2 = (0..99).fold(initialBlackTiles) { blackTiles, _ ->
        blackTiles.flatMap { refCoord ->
            dirs.values.map { dir -> refCoord + dir }.filter { coord ->
                dirs.values.count { (it + coord) in blackTiles } in (if (coord in blackTiles) 1..2 else 2..2)
            }
        }.toSet()

    }.size
    println("Part 2: $answer2")
}
