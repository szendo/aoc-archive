package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val (pk1, pk2) = Helper.getResourceAsStream("2020/25.txt")
        .bufferedReader()
        .useLines { lines -> lines.take(2).map { it.toLong() }.toList() }

    val answer1 = run {
        fun getLoopSize(target: Long, subjectNumber: Long): Int {
            var value = 1L
            var loopSize = 0
            while (value != target) {
                value = (value * subjectNumber) % 20201227
                loopSize++
            }
            return loopSize
        }

        fun transform(subjectNumber: Long, loopSize: Int): Long {
            var value = 1L
            repeat(loopSize) {
                value = (value * subjectNumber) % 20201227
            }
            return value
        }

        transform(pk2, getLoopSize(pk1, 7))
    }
    println("Part 1: $answer1")
}
