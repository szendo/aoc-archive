package com.szendo.aoc.y2020

import com.szendo.aoc.Coord4D
import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/17.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    run {
        var activeCells: Set<Coord4D> = input.flatMapIndexed { y, row ->
            row.mapIndexed { x, c ->
                Coord4D(x, y, 0, 0) to c
            }.filter { it.second == '#' }.map { it.first }
        }.toSet()

        repeat(6) {
            if (activeCells.isEmpty()) {
                return@repeat
            }

            val newActiveCells = mutableSetOf<Coord4D>()

            activeCells.forEach { base ->
                (-1..1).forEach { ow ->
                    (-1..1).forEach { oz ->
                        (-1..1).forEach { oy ->
                            (-1..1).forEach { ox ->
                                val coord = base + Coord4D(ox, oy, oz, ow)
                                val activeCountWithSelf = (-1..1).sumOf { dw ->
                                    (-1..1).sumOf { dz ->
                                        (-1..1).sumOf { dy ->
                                            (-1..1).count { dx ->
                                                (coord + Coord4D(dx, dy, dz, dw)) in activeCells
                                            }
                                        }
                                    }
                                }

                                if (coord in activeCells) {
                                    if (activeCountWithSelf in (3..4)) {
                                        newActiveCells.add(coord)
                                    }
                                } else {
                                    if (activeCountWithSelf == 3) {
                                        newActiveCells.add(coord)
                                    }
                                }

                            }
                        }
                    }
                }
            }
            activeCells = newActiveCells
        }

        activeCells.size
    }.let { println("Part 2: $it") }
}
