package com.szendo.aoc.y2020

import com.szendo.aoc.Helper
import com.szendo.aoc.split

fun main() {
    val (p1, p2) = Helper.getResourceAsStream("2020/22.txt")
        .bufferedReader()
        .useLines { lines -> lines.split("").map { it.drop(1).map { it.toInt() } }.toList() }

    fun calculateScore(d: List<Int>) = d.indices.sumOf { i -> d[i] * (d.size - i) }

    fun play(p1: List<Int>, p2: List<Int>, recursive: Boolean, depth: Int = 0): Pair<Boolean, Int> { // p1 wins?, score
        val previousRounds = mutableSetOf<Pair<List<Int>, List<Int>>>()

        val d1 = p1.toMutableList()
        val d2 = p2.toMutableList()

        while (true) {
            if (recursive && !previousRounds.add(d1 to d2)) {
                return true to calculateScore(d1)
            }

            val c1 = d1.removeAt(0)
            val c2 = d2.removeAt(0)

            val p1Wins = if (recursive && c1 <= d1.size && c2 <= d2.size) {
                play(d1.subList(0, c1), d2.subList(0, c2), recursive, depth + 1).first
            } else {
                c1 > c2
            }

            if (p1Wins) {
                d1.addAll(listOf(c1, c2))
            } else {
                d2.addAll(listOf(c2, c1))
            }

            if (d2.isEmpty()) {
                return true to calculateScore(d1)
            } else if (d1.isEmpty()) {
                return false to calculateScore(d2)
            }
        }
    }

    val answer1 = play(p1, p2, false)
    println("Part 1: $answer1")

    val answer2 = play(p1, p2, true)
    println("Part 2: $answer2")
}
