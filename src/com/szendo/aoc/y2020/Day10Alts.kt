package com.szendo.aoc.y2020

import com.szendo.aoc.Helper
import com.szendo.aoc.rle

fun main() {
    val input = Helper.getResourceAsStream("2020/10.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.toInt() }.toList() }

    run {
        fun tribonacci(n: Int): Long = when (n) {
            0, 1 -> 1L
            2 -> 2L
            else -> tribonacci(n - 1) + tribonacci(n - 2) + tribonacci(n - 3)
        }

        (listOf(0) + input)
            .sorted().asSequence().zipWithNext { a, b -> b - a }.rle()
            .onEach { if (it.second !in listOf(1, 3)) error("Unsupported difference: ${it.second}") }
            .filter { it.second == 1 }
            .map { tribonacci(it.first) }
            .reduce { acc, l -> acc * l }
    }.let { println("Part 2: $it") }

    run {
        val arrangements = mutableMapOf(0 to 1L)
        input.sorted().forEach { n -> arrangements[n] = (1..3).map { arrangements[n - it] ?: 0L }.sum() }
        arrangements.maxByOrNull { it.key }!!.value
    }.let { println("Part 2: $it") }
}
