package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/7.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val answer1 = run {
        val accepted = mutableSetOf<String>()
        do {
            var changed = false;
            for (rule in input) {
                val (bag, contents) = rule.split(" bags contain ")
                if (contents.contains("shiny gold") || accepted.any { contents.contains(it) }) {
                    if (accepted.add(bag)) {
                        changed = true
                    }
                }
            }
        } while (changed)

        accepted.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        fun countBagsIn(bag: String): Int {
            val contents = input
                .map { it.split(" bags contain ") }
                .first { it[0] == bag }[1]

            return when (contents) {
                "no other bags." -> 0
                else -> Regex("(\\d+) ([a-z ]+) bags?[.,]").findAll(contents).sumOf {
                    val (_, count, innerBag) = it.groupValues
                    count.toInt() * (1 + countBagsIn(innerBag))
                }
            }
        }

        countBagsIn("shiny gold")
    }
    println("Part 2: $answer2")
}
