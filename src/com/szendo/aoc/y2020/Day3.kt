package com.szendo.aoc.y2020

import com.szendo.aoc.*

fun main() {
    fun countTrees(grid: List<List<Char>>, slope: Pair<Int, Int>): Int {
        var pos = slope
        var trees = 0
        while (pos.y < grid.size) {
            if (grid[pos.y][pos.x % grid[pos.y].size] == '#') {
                trees++
            }
            pos += slope
        }
        return trees
    }

    val grid = Helper.getResourceAsStream("2020/3.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.toList() }.toList() }

    val answer1 = countTrees(grid, 3 to 1)
    println("Part 1: $answer1")

    val answer2 = countTrees(grid, 1 to 1).toLong() *
            countTrees(grid, 3 to 1) *
            countTrees(grid, 5 to 1) *
            countTrees(grid, 7 to 1) *
            countTrees(grid, 1 to 2)
    println("Part 2: $answer2")
}
