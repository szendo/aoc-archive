package com.szendo.aoc.y2020

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2020/15.txt")
        .bufferedReader()
        .useLines { lines -> lines.first().split(',').map { it.toInt() } }

    fun memory(startingNumbers: List<Int>, lastTurn: Int): Int {
        val lastSpoken = hashMapOf(*startingNumbers.mapIndexed { index, value ->
            value to (index + 1 to index + 1)
        }.toTypedArray())

        var lastNumber = startingNumbers.last()
        var turn = startingNumbers.size + 1
        while (turn <= lastTurn) {
            lastNumber = lastSpoken[lastNumber]!!.let { it.second - it.first }
            lastSpoken.merge(lastNumber, turn to turn) { oldVal, newVal -> oldVal.second to newVal.first }
            turn++
        }

        return lastNumber
    }

    fun fastMemory(startingNumbers: List<Int>, lastTurn: Int): Int {
        val lastSpoken = IntArray(lastTurn).apply {
            startingNumbers.forEachIndexed { index, value -> this[value] = index + 1 }
        }

        var lastNumber = startingNumbers.last()
        var turn = startingNumbers.size + 1
        while (turn <= lastTurn) {
            val newNumber = turn - 1 - lastSpoken[lastNumber].let { if (it == 0) turn - 1 else it }
            lastSpoken[lastNumber] = turn - 1
            lastNumber = newNumber
            turn++
        }

        return lastNumber
    }

    val answer1 = memory(input, 2020)
    println("Part 1: $answer1")

    val answer2 = fastMemory(input, 30_000_000)
    println("Part 2: $answer2")
}
