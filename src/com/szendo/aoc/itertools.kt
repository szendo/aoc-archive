package com.szendo.aoc

fun <T> Set<T>.permutations(r: Int = this.size): Set<List<T>> =
    if (r > size) setOf()
    else if (r == 0) setOf(listOf())
    else this.flatMap { e -> (this - e).permutations(r - 1).map { it + e } }.toSet()
