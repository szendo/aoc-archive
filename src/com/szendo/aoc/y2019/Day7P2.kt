package com.szendo.aoc.y2019

import com.szendo.aoc.Helper
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

fun main() {

    operator fun List<Int>.get(ip: Int, paramNumber: Int): Int {
        var mode = this[ip] / 100
        repeat(paramNumber - 1) {
            mode /= 10
        }
        mode %= 10

        return when (mode) {
            0 -> this[this[ip + paramNumber]]
            1 -> this[ip + paramNumber]
            else -> error("Unknown parameter mode: $mode")
        }
    }

    operator fun MutableList<Int>.set(ip: Int, paramNumber: Int, value: Int) {
        this[this[ip + paramNumber]] = value
    }

    fun execute(readonlyProgram: List<Int>, input: BlockingQueue<Int>, output: BlockingQueue<Int>) = Thread {
        val program = readonlyProgram.toMutableList()
        var ip = 0

        while (true) {
            val instr = program[ip]
            when (val opcode = instr % 100) {
                1 -> { // add
                    program[ip, 3] = program[ip, 1] + program[ip, 2]
                    ip += 4
                }

                2 -> { // multiply
                    program[ip, 3] = program[ip, 1] * program[ip, 2]
                    ip += 4
                }

                3 -> { // input
                    program[ip, 1] = input.take()
                    ip += 2
                }

                4 -> { // output
                    output.put(program[ip, 1])
                    ip += 2
                }

                5 -> { // jump-if-true
                    ip = if (program[ip, 1] != 0) program[ip, 2] else ip + 3
                }

                6 -> { // jump-if-false
                    ip = if (program[ip, 1] == 0) program[ip, 2] else ip + 3
                }

                7 -> { // less-than
                    program[ip, 3] = if (program[ip, 1] < program[ip, 2]) 1 else 0
                    ip += 4
                }

                8 -> { // equals
                    program[ip, 3] = if (program[ip, 2] == program[ip, 1]) 1 else 0
                    ip += 4
                }

                99 -> { // halt
                    return@Thread
                }

                else -> error("Unknown opcode: $opcode")
            }
        }
    }.apply { start() }

    val readonlyProgram = Helper.getResourceAsStream("2019/7.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toInt() }


    val answer2: Int = (5..9).map { ap ->
        (5..9).filter { bp -> bp != ap }.map { bp ->
            (5..9).filter { cp -> cp !in listOf(ap, bp) }.map { cp ->
                (5..9).filter { dp -> dp !in listOf(ap, bp, cp) }.map { dp ->
                    (5..9).filter { ep -> ep !in listOf(ap, bp, cp, dp) }.map { ep ->
                        val ea = LinkedBlockingQueue<Int>(listOf(ap, 0))
                        val ab = LinkedBlockingQueue<Int>(listOf(bp))
                        val bc = LinkedBlockingQueue<Int>(listOf(cp))
                        val cd = LinkedBlockingQueue<Int>(listOf(dp))
                        val de = LinkedBlockingQueue<Int>(listOf(ep))

                        val a = execute(readonlyProgram, ea, ab)
                        val b = execute(readonlyProgram, ab, bc)
                        val c = execute(readonlyProgram, bc, cd)
                        val d = execute(readonlyProgram, cd, de)
                        val e = execute(readonlyProgram, de, ea)

                        a.join()
                        b.join()
                        c.join()
                        d.join()
                        e.join()

                        ea.remove()
                    }.maxOf { it }
                }.maxOf { it }
            }.maxOf { it }
        }.maxOf { it }
    }.maxOf { it }
    println("Part 2: $answer2")

}