package com.szendo.aoc.y2019

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2019/8.txt")
        .bufferedReader()
        .readText().trim()
        .chunked(25 * 6) { s -> s.toList() }

    val answer1 = input
        .minByOrNull { it.count { c -> c == '0' } }
        ?.let { it.count { c -> c == '1' } * it.count { c -> c == '2' } }
    println("Part 1: $answer1")

    val answer2 = input
        .reduce { a, b -> a.zip(b) { ca, cb -> if (ca == '2') cb else ca } }
        .map { c -> if (c == '0') ' ' else '█' }
        .joinToString("")
        .chunked(25)
        .joinToString("\n")
    println("Part 2:\n$answer2")
}