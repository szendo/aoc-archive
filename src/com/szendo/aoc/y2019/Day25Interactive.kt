package com.szendo.aoc.y2019

import com.szendo.aoc.*
import java.util.*

fun main() {
    val program = Helper.getResourceAsStream("2019/25.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    fun IntCodeVM.sendCommand(command: String): String {
        command.forEach { send(it.code.toBigInteger()) }
        run()
        return String(generateSequence { receive() }
            .map { it.toInt().toChar() }.toList().toCharArray())
    }

    val scanner = Scanner(System.`in`)
    val machine = IntCodeVM(program)
    print(machine.sendCommand(""))
    while (!machine.isHalted) {
        print(machine.sendCommand(scanner.nextLine() + '\n'))
    }
}
