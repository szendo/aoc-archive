package com.szendo.aoc.y2019

import com.szendo.aoc.*
import java.math.BigInteger

// I still don't understand this completely 🙁
// https://www.reddit.com/r/adventofcode/comments/ee56wh/2019_day_22_part_2_so_whats_the_purpose_of_this/fbr0vjb/
// https://www.reddit.com/r/adventofcode/comments/ee56wh/2019_day_22_part_2_so_whats_the_purpose_of_this/fc0xvt5/
fun main() {
    val input = Helper.getResourceAsStream("2019/22.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val cut = "cut "
    val dealInto = "deal into new stack"
    val dealWith = "deal with increment "

    abstract class Transform {
        abstract fun apply(index: Long, deckSize: Long): Long
        abstract fun invert(deckSize: Long): Transform
    }

    data class Cut(val n: Long) : Transform() {
        override fun apply(index: Long, deckSize: Long) = Math.floorMod(index - n, deckSize)
        override fun invert(deckSize: Long) = Cut(Math.floorMod(-n, deckSize))
    }

    data class Deal(val n: Long) : Transform() {
        override fun apply(index: Long, deckSize: Long) = Math.floorMod(index * n, deckSize)
        override fun invert(deckSize: Long) = Deal(n.toBigInteger().modInverse(deckSize.toBigInteger()).toLong())
    }

    infix fun BigInteger.mod(m: Long) = this.mod(m.toBigInteger()).toLong()

    fun List<Transform>.reduce(deckSize: Long): List<Transform> {
        var transforms = this
        while (transforms.size > 2) {
            transforms = transforms.fold(listOf()) { ts, t ->
                if (ts.isEmpty()) {
                    listOf(t)
                } else {
                    when (val t0 = ts.last()) {
                        is Cut -> when (t) {
                            is Cut -> ts.dropLast(1) + Cut(Math.floorMod(t0.n + t.n, deckSize))
                            is Deal -> ts.dropLast(1) + t + Cut((t0.n.toBigInteger() * t.n.toBigInteger()) mod deckSize)
                            else -> error("Invalid transform: $t")
                        }
                        is Deal -> when (t) {
                            is Cut -> ts + t
                            is Deal -> ts.dropLast(1) + Deal((t0.n.toBigInteger() * t.n.toBigInteger()) mod deckSize)
                            else -> error("Invalid transform: $t")
                        }
                        else -> error("Invalid transform: $t")
                    }
                }
            }
        }
        return transforms
    }

    fun List<Transform>.repeat(count: Long, deckSize: Long): List<Transform> {
        val finalTransforms = mutableListOf<Transform>()
        var c = count
        var transforms = this
        while (c > 0) {
            if (c % 2 != 0L) {
                finalTransforms.addAll(transforms)
            }
            c /= 2
            transforms = (transforms + transforms).reduce(deckSize)
        }
        return finalTransforms.reduce(deckSize)
    }

    fun List<Transform>.invert(deckSize: Long) = this.reversed().map { it.invert(deckSize) }

    fun List<Transform>.apply(index: Long, deckSize: Long) =
        this.fold(index) { i, transform -> transform.apply(i, deckSize) }

    fun parseTransforms(input: List<String>, deckSize: Long) = input.flatMap { line ->
        if (line == dealInto) {
            listOf(Deal(deckSize - 1), Cut(1))
        } else if (line.startsWith(cut)) {
            val n = line.substring(cut.length).toLong()
            listOf(Cut(n))
        } else if (line.startsWith(dealWith)) {
            val n = line.substring(dealWith.length).toLong()
            listOf(Deal(n))
        } else {
            error("Invalid line: $line")
        }
    }

    val answer1 = run {
        val deckSize = 10007L
        val cardPosition = 2019L
        parseTransforms(input, deckSize)
            .reduce(deckSize)
            .repeat(1L, deckSize)
            .apply(cardPosition, deckSize)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val deckSize = 119315717514047L
        val cardPosition = 2020L
        val repeatCount = 101741582076661L

        parseTransforms(input, deckSize)
            .reduce(deckSize)
            .repeat(repeatCount, deckSize)
            .invert(deckSize)
            .apply(cardPosition, deckSize)
    }
    println("Part 2: $answer2")
}
