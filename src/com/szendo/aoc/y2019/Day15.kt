package com.szendo.aoc.y2019

import com.szendo.aoc.Coord
import com.szendo.aoc.Helper
import com.szendo.aoc.plus
import java.math.BigInteger
import java.util.*
import kotlin.random.Random
import kotlin.random.nextInt

fun main() {
    val program = Helper.getResourceAsStream("2019/15.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val answer1 = run {
        val machine = IntCodeVM(program)
        var pos = 0 to 0
        val dirs = mapOf(
            1 to (0 to 1),
            2 to (0 to -1),
            3 to (-1 to 0),
            4 to (1 to 0)
        )
        val distances = mutableMapOf<Coord, Int>((0 to 0) to 0)
        val walls = mutableSetOf<Coord>()
        val floors = mutableSetOf<Coord>()
        var oxygen: Coord? = null
        do {

            val dir = Random.nextInt(1..4)
            machine.send(dir.toBigInteger())
            machine.run()
            val status = (machine.receive() ?: error("No response")).toInt()
            when (status) {
                0 -> walls.add(pos + dirs.getValue(dir))
                1 -> {
                    if (pos + dirs.getValue(dir) !in distances) {
                        distances[pos + dirs.getValue(dir)] = distances.getValue(pos) + 1
                    }
                    pos += dirs.getValue(dir)
                    floors.add(pos)
                }
                2 -> {
                    if (pos + dirs.getValue(dir) !in distances) {
                        distances[pos + dirs.getValue(dir)] = distances.getValue(pos) + 1
                    }
                    pos += dirs.getValue(dir)
                    oxygen = pos
                    println("Oxygen system: " + oxygen)
                }
            }

            if (oxygen != null) {
                (-25..25).forEach { y ->
                    (-25..25).forEach { x ->
                        print(
                            when {
                                (x to y) == (0 to 0) -> 'X'
                                (x to y) == oxygen -> 'O'
                                (x to y) in walls -> '#'
                                (x to y) in floors -> '.'
                                else -> '?'
                            }
                        )
                    }
                    println()
                }
                    println(distances[oxygen]!!)
                break
            }


        } while (!machine.isHalted)

        null
    }
    println("Part 1: $answer1")

}