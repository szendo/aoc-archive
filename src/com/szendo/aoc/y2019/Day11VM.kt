package com.szendo.aoc.y2019

import com.szendo.aoc.*
import java.math.BigInteger

fun main() {
    val program = Helper.getResourceAsStream("2019/11.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val answer1 = run {
        var pos = 0 to 0
        var dir = 1 to 0
        val squares = mutableMapOf<Coord, BigInteger>().withDefault { BigInteger.ZERO }

        val machine = IntCodeVM(program)
        do {
            machine.send(squares.getValue(pos))
            machine.run()
            squares[pos] = machine.receive() ?: break
            val turn = machine.receive()!!
            dir = when (turn) {
                BigInteger.ZERO -> -dir.y to dir.x
                BigInteger.ONE -> dir.y to -dir.x
                else -> error("Unknown turn direction: $turn")
            }
            pos += dir
        } while (!machine.isHalted)

        squares.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var pos = 0 to 0
        var dir = 0 to 1
        val squares = mutableMapOf<Coord, BigInteger>().withDefault { BigInteger.ZERO }
        squares[pos] = BigInteger.ONE

        val machine = IntCodeVM(program)

        do {
            machine.send(squares.getValue(pos))
            machine.run()
            squares[pos] = machine.receive() ?: break
            dir = when (val turn = machine.receive()!!) {
                BigInteger.ZERO -> -dir.y to dir.x
                BigInteger.ONE -> dir.y to -dir.x
                else -> error("Unknown turn direction: $turn")
            }
            pos += dir
        } while (!machine.isHalted)

        val black = squares.filterValues { it == BigInteger.ONE }.keys
        val xRange = (black.map { it.x }.minOf { it }..black.map { it.x }.maxOf { it })
        val yRange = (black.map { it.y }.minOf { it }..black.map { it.y }.maxOf { it }).reversed()
        yRange.joinToString("\n") { y -> xRange.joinToString("") { x -> if ((x to y) in black) "█" else " " } }
    }
    println("Part 2:\n$answer2")
}
