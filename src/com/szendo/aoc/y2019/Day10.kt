package com.szendo.aoc.y2019

import com.szendo.aoc.Helper
import com.szendo.aoc.minus
import com.szendo.aoc.x
import com.szendo.aoc.y
import kotlin.math.atan2
import kotlin.math.hypot

fun main() {
    val input = Helper.getResourceAsStream("2019/10.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.toList() }.toList() }

    val asteroids = input.withIndex().flatMap { (y, row) ->
        row.withIndex().filter { it.value == '#' }.map { it.index to y }
    }

    val answer1 = asteroids.map { coords ->
        asteroids.filter { it != coords }
            .map { atan2((it - coords).y.toDouble(), (it - coords).x.toDouble()) }
            .distinct()
            .count()
    }.maxOf { it }
    println("Part 1: $answer1")

    val st = asteroids.maxByOrNull { coords ->
        asteroids.filter { it != coords }
            .map { atan2((it - coords).y.toDouble(), (it - coords).x.toDouble()) }
            .distinct()
            .count()
    }!!

    //println("Station is at : $station")
    val remaining = asteroids.filter { it != st }.toMutableList()
    var angle = -Math.PI / 2
    var firstTarget = true
    (1..200).forEach { i ->
        val asteroidsByAngle =
            remaining.groupBy { atan2((it - st).y.toDouble(), (it - st).x.toDouble()) }

        val nextAngleTargets = asteroidsByAngle
            .filter { if (firstTarget) it.key >= angle else it.key > angle }
            .minByOrNull { it.key }
            ?: asteroidsByAngle.minByOrNull { it.key }!!
        angle = nextAngleTargets.key
        firstTarget = false

        val target = nextAngleTargets.value.minByOrNull { hypot((it - st).x.toDouble(), (it - st).y.toDouble()) }!!
        //println("$i: $target (angle: $angle, dist: ${hypot((target - st).x.toDouble(), (target - st).y.toDouble())}")

        remaining.remove(target)

        if (i == 200) {
            val answer2 = target.x * 100 + target.y
            println("Part 2: $answer2")
        }
    }
}