package com.szendo.aoc.y2019

import com.szendo.aoc.*

fun main() {
    val program = Helper.getResourceAsStream("2019/21.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val answer1 = run {
        val machine = IntCodeVM(program)
        ("NOT C J\nAND D J\n" + // (!C && D)
            "NOT A T\nOR T J\n" + // || !A
            "WALK\n").forEach { machine.send(it.code.toBigInteger()) }
        machine.run()
        generateSequence { machine.receive() }
            .first { it > 127.toBigInteger() }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val machine = IntCodeVM(program)
        ("NOT C J\nAND D J\nOR E T\nOR H T\nAND T J\n" + // (!C && D && (E || H))
            "NOT J T\nNOT T T\nOR B T\nOR E T\nNOT T T\nOR T J\n" + // || !(B || E)
            "NOT A T\nOR T J\n" + // || !A
            "RUN\n").forEach { machine.send(it.code.toBigInteger()) }
        machine.run()
        generateSequence { machine.receive() }
            .first { it > 127.toBigInteger() }
    }
    println("Part 2: $answer2")
}
