package com.szendo.aoc.y2019

import com.szendo.aoc.Helper

fun main() {
    val program = Helper.getResourceAsStream("2019/7.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toInt() }

    val answer1: Int = (0..4).map { ap ->
        (0..4).filter { bp -> bp != ap }.map { bp ->
            (0..4).filter { cp -> cp !in listOf(ap, bp) }.map { cp ->
                (0..4).filter { dp -> dp !in listOf(ap, bp, cp) }.map { dp ->
                    (0..4).filter { ep -> ep !in listOf(ap, bp, cp, dp) }.map { ep ->
                        val a = IntCodeVMLite(program).apply { send(ap); send(0) }
                        val b = IntCodeVMLite(program).apply { send(bp) }
                        val c = IntCodeVMLite(program).apply { send(cp) }
                        val d = IntCodeVMLite(program).apply { send(dp) }
                        val e = IntCodeVMLite(program).apply { send(ep) }

                        a.run()
                        b.send(a.receive()!!)
                        b.run()
                        c.send(b.receive()!!)
                        c.run()
                        d.send(c.receive()!!)
                        d.run()
                        e.send(d.receive()!!)
                        e.run()

                        e.receive()!!
                    }.maxOf { it }
                }.maxOf { it }
            }.maxOf { it }
        }.maxOf { it }
    }.maxOf { it }
    println("Part 1: $answer1")

    val answer2: Int = (5..9).map { ap ->
        (5..9).filter { bp -> bp != ap }.map { bp ->
            (5..9).filter { cp -> cp !in listOf(ap, bp) }.map { cp ->
                (5..9).filter { dp -> dp !in listOf(ap, bp, cp) }.map { dp ->
                    (5..9).filter { ep -> ep !in listOf(ap, bp, cp, dp) }.map { ep ->
                        val a = IntCodeVMLite(program).apply { send(ap) }
                        val b = IntCodeVMLite(program).apply { send(bp) }
                        val c = IntCodeVMLite(program).apply { send(cp) }
                        val d = IntCodeVMLite(program).apply { send(dp) }
                        val e = IntCodeVMLite(program).apply { send(ep) }

                        do {
                            a.send(e.receive() ?: 0)
                            a.run()
                            b.send(a.receive()!!)
                            b.run()
                            c.send(b.receive()!!)
                            c.run()
                            d.send(c.receive()!!)
                            d.run()
                            e.send(d.receive()!!)
                            e.run()
                        } while (!e.isHalted)

                        e.receive()!!
                    }.maxOf { it }
                }.maxOf { it }
            }.maxOf { it }
        }.maxOf { it }
    }.maxOf { it }
    println("Part 2: $answer2")
}