package com.szendo.aoc.y2019

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2019/6.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.split(')') }.map { it[1] to it[0] }.toMap() }

    val answer1 = input.keys
        .map { obj -> generateSequence(obj) { input[it] }.count() - 1 }
        .sum()
    println("Part 1: $answer1")

    val you = generateSequence("YOU") { input[it] }.toMutableList()
    val san = generateSequence("SAN") { input[it] }.toMutableList()
    while (you[you.lastIndex] == san[san.lastIndex]) {
        you.removeAt(you.lastIndex)
        san.removeAt(san.lastIndex)
    }
    val answer2 = you.size + san.size - 2
    println("Part 2: $answer2")
}