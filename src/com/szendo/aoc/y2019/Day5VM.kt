package com.szendo.aoc.y2019

import com.szendo.aoc.Helper

fun main() {
    val program = Helper.getResourceAsStream("2019/5.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toInt() }

    val answer1 = run {
        IntCodeVMLite(program)
            .apply { send(1); run() }
            .let {
                generateSequence { it.receive() }
                    .dropWhile { it == 0 }
                    .single()
            }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        IntCodeVMLite(program)
            .apply { send(5); run() }
            .receive()!!
    }
    println("Part 2: $answer2")
}