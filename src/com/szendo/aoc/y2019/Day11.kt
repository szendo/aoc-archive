package com.szendo.aoc.y2019

import com.szendo.aoc.*
import java.math.BigInteger
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit

fun main() {

    fun Map<Int, BigInteger>.mode(ip: Int, paramNumber: Int): Int {
        var mode = getValue(ip).toInt() / 100
        repeat(paramNumber - 1) {
            mode /= 10
        }
        mode %= 10
        return mode
    }

    operator fun Map<Int, BigInteger>.get(ip: Int, base: Int, paramNumber: Int): BigInteger {
        val addr = when (val mode = mode(ip, paramNumber)) {
            0 -> getValue(ip + paramNumber).toInt()
            1 -> ip + paramNumber
            2 -> base + getValue(ip + paramNumber).toInt()
            else -> error("Unknown parameter mode: $mode")
        }
        return getValue(addr)
    }

    operator fun MutableMap<Int, BigInteger>.set(ip: Int, base: Int, paramNumber: Int, value: BigInteger) {
        val addr: Int = when (val mode = mode(ip, paramNumber)) {
            0 -> getValue(ip + paramNumber).toInt()
            1 -> throw IllegalArgumentException("Invalid parameter mode: $mode")
            2 -> base + getValue(ip + paramNumber).toInt()
            else -> error("Unknown parameter mode: $mode")
        }
        this[addr] = value
    }

    fun execute(
        readonlyProgram: List<BigInteger>,
        input: BlockingQueue<BigInteger>,
        output: BlockingQueue<BigInteger>
    ) = Thread {
        val program = mutableMapOf(*readonlyProgram.withIndex().map { it.index to it.value }.toTypedArray())
            .withDefault { BigInteger.ZERO }
        var ip = 0
        var base = 0

        while (true) {
            val instr = program.getValue(ip).toInt()
            when (val opcode = instr % 100) {
                1 -> { // add
                    program[ip, base, 3] = program[ip, base, 1] + program[ip, base, 2]
                    ip += 4
                }

                2 -> { // multiply
                    program[ip, base, 3] = program[ip, base, 1] * program[ip, base, 2]
                    ip += 4
                }

                3 -> { // input
                    program[ip, base, 1] = input.take()
                    ip += 2
                }

                4 -> { // output
                    output.put(program[ip, base, 1])
                    ip += 2
                }

                5 -> { // jump-if-true
                    ip = if (program[ip, base, 1] != BigInteger.ZERO) program[ip, base, 2].toInt() else ip + 3
                }

                6 -> { // jump-if-false
                    ip = if (program[ip, base, 1] == BigInteger.ZERO) program[ip, base, 2].toInt() else ip + 3
                }

                7 -> { // less-than
                    program[ip, base, 3] =
                        if (program[ip, base, 1] < program[ip, base, 2]) BigInteger.ONE else BigInteger.ZERO
                    ip += 4
                }

                8 -> { // equals
                    program[ip, base, 3] =
                        if (program[ip, base, 2] == program[ip, base, 1]) BigInteger.ONE else BigInteger.ZERO
                    ip += 4
                }

                9 -> { // adjust-relative-base
                    base += program[ip, base, 1].toInt()
                    ip += 2
                }

                99 -> { // halt
                    return@Thread
                }

                else -> error("Unknown opcode: $opcode")
            }
        }
    }.apply { start() }

    val readonlyProgram = Helper.getResourceAsStream("2019/11.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val answer1 = run {
        var pos = 0 to 0
        var dir = 1 to 0
        val squares = mutableMapOf<Coord, BigInteger>()

        val i = LinkedBlockingQueue<BigInteger>()
        val o = LinkedBlockingQueue<BigInteger>()
        execute(readonlyProgram, i, o)

        while (true) {
            i.add(squares[pos] ?: BigInteger.ZERO)
            squares[pos] = o.poll(100, TimeUnit.MILLISECONDS) ?: break
            val turn = o.take() ?: break
            dir = when (turn) {
                BigInteger.ZERO -> -dir.y to dir.x
                BigInteger.ONE -> dir.y to -dir.x
                else -> error("Unknown turn direction: $turn")
            }
            pos += dir
        }

        squares.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var pos = 0 to 0
        var dir = 0 to 1
        val squares = mutableMapOf<Coord, BigInteger>()
        squares[pos] = BigInteger.ONE

        val i = LinkedBlockingQueue<BigInteger>()
        val o = LinkedBlockingQueue<BigInteger>()
        execute(readonlyProgram, i, o)

        while (true) {
            i.add(squares[pos] ?: BigInteger.ZERO)
            squares[pos] = o.poll(100, TimeUnit.MILLISECONDS) ?: break
            dir = when (val turn = o.take()) {
                BigInteger.ZERO -> -dir.y to dir.x
                BigInteger.ONE -> dir.y to -dir.x
                else -> error("Unknown turn direction: $turn")
            }
            pos += dir
        }

        val black = squares.filterValues { it == BigInteger.ONE }.keys
        val xRange = (black.map { it.x }.minOf { it }..black.map { it.x }.maxOf { it })
        val yRange = (black.map { it.y }.minOf { it }..black.map { it.y }.maxOf { it }).reversed()
        yRange.joinToString("\n") { y -> xRange.joinToString("") { x -> if ((x to y) in black) "█" else " " } }
    }
    println("Part 2:\n$answer2")

}
