package com.szendo.aoc.y2019

import com.szendo.aoc.*
import java.awt.Color
import java.awt.RenderingHints
import java.awt.image.BufferedImage
import java.io.File
import java.math.BigInteger
import javax.imageio.ImageIO
import kotlin.math.sign

fun main() {
    val program = Helper.getResourceAsStream("2019/13.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val answer2 = run {
        val machine = IntCodeVM(program)
        machine[0] = 2.toBigInteger() // number of quarters inserted; set it to 2 to play for free

        val grid = mutableMapOf<Coord, Int>()
            .withDefault { 0 }
        var paddle = 0 to 0
        var ball = 0 to 0

        val scale = 8
        val image = BufferedImage(38 * scale, 21 * scale, BufferedImage.TYPE_INT_RGB)
        val g = image.createGraphics()
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.background = Color.BLACK
        g.clearRect(0, 0, image.width, image.height)

        var n = 0L
        var score = 0
        do {
            machine.run()
            generateSequence { machine.receive() }.chunked(3)
                .map { it.map(BigInteger::toInt) }
                .forEach { (x, y, t) ->
                    if (x < 0 || y < 0) {
                        score = t
                    } else {
                        when (t) {
                            3 -> paddle = x to y
                            4 -> ball = x to y
                        }

                        grid[x to y] = t
                    }
                }

            (0..20).forEach { y ->
                (0..37).forEach { x ->
                    val t = grid[x to y]
                    g.color = when (t) {
                        0 -> Color.BLACK
                        1 -> Color.GRAY
                        2 -> Color.BLUE
                        3 -> Color.MAGENTA
                        4 -> Color.RED
                        else -> error("Unknown tile: $t")
                    }

                    when (t) {
                        4 -> g.fillOval(scale * x, scale * y, scale, scale)
                        else -> g.fillRect(scale * x, scale * y, scale, scale)
                    }

                }
            }
            ImageIO.write(image, "gif", File("breakout/%08d.gif".format(n++)))

            if (!machine.isHalted) {
                machine.send((ball.x - paddle.x).sign.toBigInteger())
            }


        } while (!machine.isHalted)
        score
    }
    println("Part 2: $answer2")
}
