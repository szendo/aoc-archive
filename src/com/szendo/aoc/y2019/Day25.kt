package com.szendo.aoc.y2019

import com.szendo.aoc.*

fun main() {
    val program = Helper.getResourceAsStream("2019/25.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    fun IntCodeVM.sendCommand(command: String): String {
        command.forEach { send(it.code.toBigInteger()) }
        run()
        return String(generateSequence { receive() }
            .map { it.toInt().toChar() }.toList().toCharArray())
    }

    val answer1 = run {
        val collectCommands = "south\neast\ntake whirled peas\n" +
            "west\nnorth\nnorth\neast\ntake ornament\n" +
            "north\nnorth\ntake dark matter\n" +
            "south\nsouth\nwest\nwest\nwest\ntake candy cane\n" +
            "west\nwest\ntake tambourine\n" +
            "east\neast\neast\nnorth\ntake astrolabe\n" +
            "east\ntake hologram\n" +
            "east\ntake klein bottle\n" +
            "west\nsouth\nwest\n"

        val machine = IntCodeVM(program)
        machine.sendCommand(collectCommands)

        val items = listOf(
            "astrolabe",
            "candy cane",
            "dark matter",
            // "escape pod", // You're launched into space! Bye!
            // "giant electromagnet", // The giant electromagnet is stuck to you.  You can't move!!
            "hologram",
            // "infinite loop",
            "klein bottle",
            // "molten lava", // The molten lava is way too hot! You melt!
            "ornament",
            // "photons", // It is suddenly completely dark! You are eaten by a Grue!
            "tambourine",
            "whirled peas",
        )

        // Gray code ordering, so there's only a single take/drop between tries
        items
            .fold<String, List<Set<String>>>(listOf(setOf())) { sets, item ->
                sets.map { it + item } + sets.reversed()
            }
            .zipWithNext { oldInv, newInv ->
                if (oldInv.size > newInv.size) "drop ${(oldInv - newInv).first()}\n"
                else "take ${(newInv - oldInv).first()}\n"
            }
            .forEach { command ->
                val output = machine.sendCommand("north\n")
                Regex("Oh, hello! You should be able to get in by typing (\\d+) on the keypad at the main airlock\\.")
                    .find(output)?.let { mr -> return@run mr.groupValues[1] }
                machine.sendCommand(command)
            }
    }
    println("Part 1: $answer1")
}
