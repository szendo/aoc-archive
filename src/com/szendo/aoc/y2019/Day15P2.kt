package com.szendo.aoc.y2019

import com.szendo.aoc.Coord
import com.szendo.aoc.Helper
import com.szendo.aoc.plus

fun main() {
    val program = Helper.getResourceAsStream("2019/15.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val answer1 = run {
        val machine = IntCodeVM(program)
        var pos = 0 to 0
        val dirs = mapOf(
            1 to (0 to 1),
            2 to (0 to -1),
            3 to (-1 to 0),
            4 to (1 to 0)
        )
        val revDirs = mapOf(
            1 to 2,
            2 to 1,
            3 to 4,
            4 to 3
        )

        val distances = mutableMapOf((0 to 0) to 0)
        val walls = mutableSetOf<Coord>()
        val floors = mutableSetOf<Coord>()
        var oxygen: Coord? = null

        fun explore(pos: Coord) {
            (1..4).forEach { dir ->
                val newPos = pos + dirs.getValue(dir)
                if (newPos !in walls && newPos !in floors && newPos != oxygen) {
                    machine.send(dir.toBigInteger())
                    machine.run()

                    when (machine.receive()!!.toInt()) {
                        0 -> walls.add(newPos)
                        1 -> {
                            if (newPos !in distances) {
                                distances[newPos] = distances.getValue(pos) + 1
                            }

                            floors.add(newPos)
                            explore(newPos)
                            machine.send(revDirs.getValue(dir).toBigInteger())
                            machine.run()
                            check(checkNotNull(machine.receive()).toInt() > 0)
                        }
                        2 -> {
                            if (newPos !in distances) {
                                distances[newPos] = distances.getValue(pos) + 1
                            }

                            oxygen = newPos
                            explore(newPos)
                            machine.send(revDirs.getValue(dir).toBigInteger())
                            machine.run()
                            check(checkNotNull(machine.receive()).toInt() > 0)
                        }
                    }
                }
            }
        }

        explore(0 to 0)

        (-19..21).forEach { y ->
            (-21..19).forEach { x ->
                print(
                    when {
                        (x to y) == (0 to 0) -> '△'
                        (x to y) == oxygen -> '○'
                        (x to y) in walls -> '█'
                        (x to y) in floors -> ' '
                        else -> '█'
                    }
                )
            }
            println()
        }


        distances.getValue(oxygen!!)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val machine = IntCodeVM(program)
        var pos = 0 to 0
        val dirs = mapOf(
            1 to (0 to 1),
            2 to (0 to -1),
            3 to (-1 to 0),
            4 to (1 to 0)
        )
        val revDirs = mapOf(
            1 to 2,
            2 to 1,
            3 to 4,
            4 to 3
        )

        val distances = mutableMapOf((0 to 0) to 0)
        val forwardTrack = mutableMapOf<Coord, Set<Coord>>()
        val backtrack = mutableMapOf<Coord, Coord>()
        val walls = mutableSetOf<Coord>()
        val floors = mutableSetOf<Coord>()
        var oxygen: Coord? = null

        fun explore(pos: Coord) {
            (1..4).forEach { dir ->
                val newPos = pos + dirs.getValue(dir)
                if (newPos !in walls && newPos !in floors && newPos != oxygen) {
                    machine.send(dir.toBigInteger())
                    machine.run()

                    when (machine.receive()!!.toInt()) {
                        0 -> walls.add(newPos)
                        1 -> {
                            if (newPos !in distances) {
                                backtrack[newPos] = pos
                                forwardTrack.merge(pos, setOf(newPos)) { a, b -> a + b }
                                distances[newPos] = distances.getValue(pos) + 1
                            }

                            floors.add(newPos)
                            explore(newPos)
                            machine.send(revDirs.getValue(dir).toBigInteger())
                            machine.run()
                            machine.receive()
                        }
                        2 -> {
                            if (newPos !in distances) {
                                backtrack[newPos] = pos
                                forwardTrack.merge(pos, setOf(newPos)) { a, b -> a + b }
                                distances[newPos] = distances.getValue(pos) + 1
                            }

                            oxygen = newPos
                            explore(newPos)
                            machine.send(revDirs.getValue(dir).toBigInteger())
                            machine.run()
                            machine.receive()
                        }
                    }
                }
            }
        }

        explore(0 to 0)

        val filled = mutableSetOf(oxygen!!)
        val unfilled = floors.toMutableSet()
        unfilled.remove(oxygen!!)

        var minutes = 0
        while (unfilled.isNotEmpty()) {
            minutes++

            val newlyFilled =
                filled.map { backtrack[it] ?: (0 to 0) } + filled.flatMap { forwardTrack[it] ?: emptySet() }.toSet()
            filled.addAll(newlyFilled)
            unfilled.removeAll(newlyFilled)


        }

        minutes

    }
    println("Part 2: $answer2")

}