package com.szendo.aoc.y2019

import com.szendo.aoc.*
import kotlin.math.abs

fun main() {

    fun getReachTimes(wire: List<String>): Map<Coord, Int> {
        val points = mutableMapOf<Coord, Int>()

        var t = 0
        var p = 0 to 0
        for (path in wire) {
            val dp = when (path[0]) {
                'U' -> 0 to -1
                'D' -> 0 to 1
                'L' -> -1 to 0
                'R' -> 1 to 0
                else -> error("Unknown direction: ${path[0]}")
            }

            val len = path.substring(1).toInt()

            repeat(len) {
                p += dp
                t++
                points.putIfAbsent(p, t)
            }
        }

        return points
    }

    val input: List<List<String>> = Helper.getResourceAsStream("2019/3.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.split(',') }.toList() }

    val wire1 = input[0]
    val wire2 = input[1]

    val wire1Times = getReachTimes(wire1)
    val wire2Times = getReachTimes(wire2)

    val commonPoints = wire1Times.keys.intersect(wire2Times.keys)

    val answer1 = commonPoints.map { abs(it.x) + abs(it.y) }.minOf { it }
    println("Part 1: $answer1")

    val answer2 = commonPoints.map { wire1Times.getValue(it) + wire2Times.getValue(it) }.minOf { it }
    println("Part 2: $answer2")
}