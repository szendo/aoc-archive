package com.szendo.aoc.y2019

import java.util.*

class IntCodeVMLite(program: List<Int>) {
    private var memory = mutableMapOf(*program.mapIndexed(::Pair).toTypedArray()).withDefault { 0 }
    private var ip = 0
    private var base = 0

    private val input: Queue<Int> = LinkedList()
    private val output: Queue<Int> = LinkedList()

    var isHalted = false
        private set

    operator fun get(addr: Int) = memory[addr]

    operator fun set(addr: Int, value: Int) {
        memory[addr] = value
    }

    fun send(value: Int) = if (isHalted) error("Program halted") else input.offer(value)

    fun receive(): Int? = output.poll()

    fun run(): Boolean {
        if (isHalted) throw IllegalStateException("Program halted")

        while (true) {
            val instr = memory.getValue(ip)
            when (val opcode = instr % 100) {
                1 -> { // add
                    memory[ip, base, 3] = memory[ip, base, 1] + memory[ip, base, 2]
                    ip += 4
                }

                2 -> { // multiply
                    memory[ip, base, 3] = memory[ip, base, 1] * memory[ip, base, 2]
                    ip += 4
                }

                3 -> { // input
                    memory[ip, base, 1] = input.poll() ?: return true
                    ip += 2
                }

                4 -> { // output
                    output.add(memory[ip, base, 1])
                    ip += 2
                }

                5 -> { // jump-if-true
                    ip = if (memory[ip, base, 1] != 0) memory[ip, base, 2] else ip + 3
                }

                6 -> { // jump-if-false
                    ip = if (memory[ip, base, 1] == 0) memory[ip, base, 2] else ip + 3
                }

                7 -> { // less-than
                    memory[ip, base, 3] = if (memory[ip, base, 1] < memory[ip, base, 2]) 1 else 0
                    ip += 4
                }

                8 -> { // equals
                    memory[ip, base, 3] = if (memory[ip, base, 2] == memory[ip, base, 1]) 1 else 0
                    ip += 4
                }

                9 -> { // adjust-relative-base
                    base += memory[ip, base, 1]
                    ip += 2
                }

                99 -> { // halt
                    isHalted = true
                    return false
                }

                else -> error("Unknown opcode: $opcode")
            }
        }
    }

    private fun Map<Int, Int>.mode(ip: Int, paramNumber: Int): Int {
        var mode = getValue(ip) / 100
        repeat(paramNumber - 1) {
            mode /= 10
        }
        mode %= 10
        return mode
    }

    private operator fun Map<Int, Int>.get(ip: Int, base: Int, paramNumber: Int): Int {
        val addr = when (val mode = mode(ip, paramNumber)) {
            0 -> getValue(ip + paramNumber)
            1 -> ip + paramNumber
            2 -> base + getValue(ip + paramNumber)
            else -> error("Unknown parameter mode: $mode")
        }
        return getValue(addr)
    }

    private operator fun MutableMap<Int, Int>.set(ip: Int, base: Int, paramNumber: Int, value: Int) {
        val addr = when (val mode = mode(ip, paramNumber)) {
            0 -> getValue(ip + paramNumber)
            1 -> throw IllegalArgumentException("Invalid parameter mode: $mode")
            2 -> base + getValue(ip + paramNumber)
            else -> error("Unknown parameter mode: $mode")
        }
        this[addr] = value
    }
}