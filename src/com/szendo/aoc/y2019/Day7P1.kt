package com.szendo.aoc.y2019

import com.szendo.aoc.Helper

fun main() {

    operator fun List<Int>.get(ip: Int, paramNumber: Int): Int {
        var mode = this[ip] / 100
        repeat(paramNumber - 1) {
            mode /= 10
        }
        mode %= 10

        return when (mode) {
            0 -> this[this[ip + paramNumber]]
            1 -> this[ip + paramNumber]
            else -> error("Unknown parameter mode: $mode")
        }
    }

    operator fun MutableList<Int>.set(ip: Int, paramNumber: Int, value: Int) {
        this[this[ip + paramNumber]] = value
    }

    fun execute(readonlyProgram: List<Int>, input: (Int) -> Int): List<Int> {
        val program = readonlyProgram.toMutableList()
        var ip = 0

        var inputIndex = 0
        val output = mutableListOf<Int>()
        while (true) {
            val instr = program[ip]
            when (val opcode = instr % 100) {
                1 -> { // add
                    program[ip, 3] = program[ip, 1] + program[ip, 2]
                    ip += 4
                }

                2 -> { // multiply
                    program[ip, 3] = program[ip, 1] * program[ip, 2]
                    ip += 4
                }

                3 -> { // input
                    program[ip, 1] = input(inputIndex++)
                    ip += 2
                }

                4 -> { // output
                    output.add(program[ip, 1])
                    ip += 2
                }

                5 -> { // jump-if-true
                    ip = if (program[ip, 1] != 0) program[ip, 2] else ip + 3
                }

                6 -> { // jump-if-false
                    ip = if (program[ip, 1] == 0) program[ip, 2] else ip + 3
                }

                7 -> { // less-than
                    program[ip, 3] = if (program[ip, 1] < program[ip, 2]) 1 else 0
                    ip += 4
                }

                8 -> { // equals
                    program[ip, 3] = if (program[ip, 2] == program[ip, 1]) 1 else 0
                    ip += 4
                }

                99 -> { // halt
                    return output.toList()
                }

                else -> error("Unknown opcode: $opcode")
            }
        }
    }

    val readonlyProgram = Helper.getResourceAsStream("2019/7.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toInt() }

    val answer1: Int = (0..4).map { ap ->
        val aos = execute(readonlyProgram, listOf(ap, 0)::get).single()
        (0..4).filter { bp -> bp != ap }.map { bp ->
            val bos = execute(readonlyProgram, listOf(bp, aos)::get).single()
            (0..4).filter { cp -> cp !in listOf(ap, bp) }.map { cp ->
                val cos = execute(readonlyProgram, listOf(cp, bos)::get).single()
                (0..4).filter { dp -> dp !in listOf(ap, bp, cp) }.map { dp ->
                    val dos = execute(readonlyProgram, listOf(dp, cos)::get).single()
                    (0..4).filter { ep -> ep !in listOf(ap, bp, cp, dp) }.map { ep ->
                        execute(readonlyProgram, listOf(ep, dos)::get).single()
                    }.maxOf { it }
                }.maxOf { it }
            }.maxOf { it }
        }.maxOf { it }
    }.maxOf { it }
    println("Part 1: $answer1")
}