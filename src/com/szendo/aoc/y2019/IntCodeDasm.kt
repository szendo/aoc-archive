package com.szendo.aoc.y2019

import com.szendo.aoc.Helper
import java.math.BigInteger

fun main() {

    fun Map<Int, BigInteger>.param(ip: Int, paramNumber: Int): String {
        var mode = this.getValue(ip).toInt() / 100
        repeat(paramNumber - 1) {
            mode /= 10
        }
        mode %= 10
        return when (mode) {
            0 -> "pos" + "(" + this[ip + paramNumber] + ")"
            1 -> this[ip + paramNumber].toString()
            2 -> "rel" + "(" + this[ip + paramNumber] + ")"
            else -> error("Unknown parameter mode: $mode")
        }
    }

    fun disassemble(program: Map<Int, BigInteger>): List<String> {
        val lines = mutableListOf<String>()
        var ip = 0

        while (ip < program.size) {
            val instr = program.getValue(ip).toInt()
            when (val opcode = instr % 100) {
                1 -> { // add
                    lines.add("$ip: ADD ${program.param(ip, 1)} ${program.param(ip, 2)} ${program.param(ip, 3)}")
                    ip += 4
                }

                2 -> { // multiply
                    lines.add("$ip: MUL ${program.param(ip, 1)} ${program.param(ip, 2)} ${program.param(ip, 3)}")
                    ip += 4
                }

                3 -> { // input
                    lines.add("$ip: INP ${program.param(ip, 1)}")
                    ip += 2
                }

                4 -> { // output
                    lines.add("$ip: OUT ${program.param(ip, 1)}")
                    ip += 2
                }

                5 -> { // jump-if-true
                    lines.add("$ip: JNZ ${program.param(ip, 1)} ${program.param(ip, 2)}")
                    ip += 3
                }

                6 -> { // jump-if-false
                    lines.add("$ip: JEZ ${program.param(ip, 1)} ${program.param(ip, 2)}")
                    ip += 3
                }

                7 -> { // less-than
                    lines.add("$ip: LT ${program.param(ip, 1)} ${program.param(ip, 2)} ${program.param(ip, 3)}")
                    ip += 4
                }

                8 -> { // equals
                    lines.add("$ip: EQ ${program.param(ip, 1)} ${program.param(ip, 2)} ${program.param(ip, 3)}")
                    ip += 4
                }

                9 -> { // adjust-relative-base
                    lines.add("$ip: ARB ${program.param(ip, 1)}")
                    ip += 2
                }

                99 -> { // halt
                    lines.add("$ip: HALT")
                    ip++
                }

                else -> { // unknown (data)
                    lines.add("$ip: DATA $instr")
                    ip++
                }
            }
        }

        return lines.toList()
    }

    val readonlyProgram = Helper.getResourceAsStream("2019/13.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val program = disassemble(mapOf(*readonlyProgram.withIndex().map { it.index to it.value }.toTypedArray())
        .withDefault { BigInteger.ZERO })
    println(program.joinToString("\n"))
}