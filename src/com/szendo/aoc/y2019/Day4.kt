package com.szendo.aoc.y2019

fun main() {
    val input = 264360..746325

    fun matchesPart1(password: String) = password.zipWithNext().any { it.first == it.second }
            && password.zipWithNext().all { it.first <= it.second }

    fun matchesPart2(password: String) = password.zipWithNext().all { it.first <= it.second }
            && ('0'..'9').any { digit -> password.indexOfLast { it == digit } - password.indexOfFirst { it == digit } == 1 }

    val answer1 = input.count { matchesPart1(it.toString()) }
    println("Part 1: $answer1")

    val answer2 = input.count { matchesPart2(it.toString()) }
    println("Part 2: $answer2")
}