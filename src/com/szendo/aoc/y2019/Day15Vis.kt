package com.szendo.aoc.y2019

import com.szendo.aoc.Coord
import com.szendo.aoc.Helper
import com.szendo.aoc.plus
import java.awt.Color
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

fun main() {
    val program = Helper.getResourceAsStream("2019/15.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val scale = 8
    val image = BufferedImage(43 * scale, 43 * scale, BufferedImage.TYPE_INT_RGB)
    val g = image.createGraphics()
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g.background = Color(0, 0, 0x99)
    g.clearRect(0, 0, image.width, image.height)

    var frame1 = 0L
    var frame2 = 0L

    fun renderFrame1(g: Graphics2D, pos: Coord, walls: Set<Coord>, floors: Set<Coord>, oxygen: Coord?) {
        (-22..20).forEach { y ->
            (-22..20).forEach { x ->
                g.color = when {
                    (x to y) == (0 to 0) -> Color.YELLOW
                    (x to y) in walls -> Color.BLACK
                    (x to y) == oxygen -> Color.CYAN
                    (x to y) in floors -> Color.WHITE
                    else -> g.background
                }

                g.fillRect((x + 22) * scale, (y + 22) * scale, scale, scale)

                if (pos == (x to y)) {
                    g.color = Color.RED
                    g.fillOval((x + 22) * scale, (y + 22) * scale, scale, scale)
                }
            }
        }
        ImageIO.write(image, "gif", File("maze1/%08d.gif".format(frame1++)))
    }

    fun renderFrame2(g: Graphics2D, walls: Set<Coord>, floors: Set<Coord>, filled: Set<Coord>) {
        (-22..20).forEach { y ->
            (-22..20).forEach { x ->
                g.color = when {
                    (x to y) in walls -> Color.BLACK
                    (x to y) in filled -> Color.CYAN
                    (x to y) in floors -> Color.WHITE
                    else -> g.background
                }

                g.fillRect((x + 22) * scale, (y + 22) * scale, scale, scale)
            }
        }
        ImageIO.write(image, "gif", File("maze2/%08d.gif".format(frame2++)))
    }

    run {
        val machine = IntCodeVM(program)
        val dirs = mapOf(
            1 to (0 to -1),
            2 to (0 to 1),
            3 to (-1 to 0),
            4 to (1 to 0)
        )
        val revDirs = mapOf(
            1 to 2,
            2 to 1,
            3 to 4,
            4 to 3
        )

        val distances = mutableMapOf((0 to 0) to 0)
        val forwardTrack = mutableMapOf<Coord, Set<Coord>>()
        val backtrack = mutableMapOf<Coord, Coord>()

        val walls = mutableSetOf<Coord>()
        val floors = mutableSetOf(0 to 0)
        var oxygen: Coord? = null

        fun explore(pos: Coord) {
            (1..4).forEach { dir ->
                val newPos = pos + dirs.getValue(dir)
                if (newPos !in walls && newPos !in floors && newPos != oxygen) {
                    machine.send(dir.toBigInteger())
                    machine.run()

                    when (machine.receive()!!.toInt()) {
                        0 -> {
                            walls.add(newPos)
                            renderFrame1(g, pos, walls, floors, oxygen)
                        }
                        1 -> {
                            if (newPos !in distances) {
                                backtrack[newPos] = pos
                                forwardTrack.merge(pos, setOf(newPos)) { a, b -> a + b }
                                distances[newPos] = distances.getValue(pos) + 1
                            }

                            floors.add(newPos)
                            renderFrame1(g, newPos, walls, floors, oxygen)
                            explore(newPos)
                            machine.send(revDirs.getValue(dir).toBigInteger())
                            machine.run()
                            check(machine.receive()!!.toInt() > 0)
                            renderFrame1(g, pos, walls, floors, oxygen)
                        }
                        2 -> {
                            if (newPos !in distances) {
                                backtrack[newPos] = pos
                                forwardTrack.merge(pos, setOf(newPos)) { a, b -> a + b }
                                distances[newPos] = distances.getValue(pos) + 1
                            }

                            oxygen = newPos
                            renderFrame1(g, newPos, walls, floors, oxygen)
                            explore(newPos)
                            machine.send(revDirs.getValue(dir).toBigInteger())
                            machine.run()
                            check(machine.receive()!!.toInt() > 0)
                            renderFrame1(g, pos, walls, floors, oxygen)
                        }
                    }
                }
            }
        }
        renderFrame1(g, 0 to 0, walls, floors, oxygen)

        explore(0 to 0) // part 1

        val filled = mutableSetOf(oxygen!!)
        val unfilled = floors.toMutableSet()
        unfilled.remove(oxygen!!)


        renderFrame2(g, walls, floors, filled)
        var minutes = 0
        do {
            minutes++

            val newlyFilled =
                filled.map { backtrack[it] ?: (0 to 0) } + filled.flatMap { forwardTrack[it] ?: emptySet() }.toSet()
            filled.addAll(newlyFilled)
            unfilled.removeAll(newlyFilled)

            renderFrame2(g, walls, floors, filled)

        } while (unfilled.isNotEmpty())

    }
}