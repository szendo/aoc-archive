package com.szendo.aoc.y2019

import com.szendo.aoc.Helper
import java.util.*

fun main() {
    // (outputChemical) -> (outputQuantity, [(inputChemical, inputQuantity), ...])
    val reactions = Helper.getResourceAsStream("2019/14.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { line ->
                val (inStr, outStr) = line.split(" => ")
                val inputs = inStr
                    .split(", ").map { it.split(' ').let { (quantity, chemical) -> chemical to quantity.toInt() } }
                val output = outStr.split(' ').let { (quantity, chemical) -> chemical to quantity.toInt() }
                output.first to (output.second to inputs)
            }.toMap()
        }

    fun calculateRequiredOre(fuelQuantity: Long): Long {
        var requiredOreQuantity = 0L
        val requirements: Queue<Pair<String, Long>> = LinkedList(listOf("FUEL" to fuelQuantity))
        val leftovers = mutableMapOf<String, Long>()

        while (requirements.isNotEmpty()) {
            val (requiredChemical, requiredQuantity) = requirements.remove()

            if (requiredChemical == "ORE") {
                requiredOreQuantity += requiredQuantity
                continue
            }

            val leftoverQuantity = leftovers[requiredChemical] ?: 0
            when {
                leftoverQuantity < requiredQuantity -> {
                    leftovers.remove(requiredChemical)
                    val quantityToProduce = requiredQuantity - leftoverQuantity
                    val (producedQuantity, inputChemicals) = reactions.getValue(requiredChemical)
                    val batchesRequired = (quantityToProduce + producedQuantity - 1) / producedQuantity
                    if (producedQuantity * batchesRequired > quantityToProduce) {
                        leftovers[requiredChemical] = producedQuantity * batchesRequired - quantityToProduce
                    }
                    inputChemicals.forEach { (inputChemical, inputQuantity) ->
                        requirements.add(inputChemical to inputQuantity * batchesRequired)
                    }
                }
                leftoverQuantity == requiredQuantity -> leftovers.remove(requiredChemical)
                leftoverQuantity > requiredQuantity -> leftovers[requiredChemical] = leftoverQuantity - requiredQuantity
            }
        }
        return requiredOreQuantity
    }

    val answer1 = calculateRequiredOre(fuelQuantity = 1)
    println("Part 1: $answer1")

    val answer2 = run {
        val totalOre = 1_000_000_000_000

        var (upperBound, upperRequiredOre) = generateSequence(totalOre / answer1) { it * 2 }
            .map { it to calculateRequiredOre(fuelQuantity = it) }
            .dropWhile { it.second < totalOre }
            .firstOrNull() ?: return@run 0

        if (upperRequiredOre == totalOre) {
            return@run upperBound
        }

        var lowerBound = upperBound / 2
        while (lowerBound + 1 < upperBound) {
            val targetFuel = (upperBound + lowerBound) / 2

            val requiredOre = calculateRequiredOre(fuelQuantity = targetFuel)
            when {
                requiredOre < totalOre -> lowerBound = targetFuel
                requiredOre == totalOre -> return@run targetFuel
                requiredOre > totalOre -> upperBound = targetFuel
            }
        }
        return@run lowerBound
    }
    println("Part 2: $answer2")
}