package com.szendo.aoc.y2019

import com.szendo.aoc.Helper
import java.math.BigInteger

fun main() {
    val program = Helper.getResourceAsStream("2019/19.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    fun isAffected(x: Int, y: Int): Boolean = IntCodeVM(program).run {
        send(x.toBigInteger())
        send(y.toBigInteger())
        run()
        receive()
    } == BigInteger.ONE

    val answer1 = (0 until 50).sumOf { x -> (0 until 50).count { y -> isAffected(x, y) } }
    println("Part 1: $answer1")

    val answer2 = run {
        val ranges = mutableMapOf(0 to (0 to 0)) // y to x-range
        var y = 0
        while (true) {
            y++

            val (lastMinX, lastMaxX) = ranges[y - 1] ?: ranges[y - 2] ?: ranges[y - 3]!!
            var minX = lastMinX
            while (minX < lastMinX + 10 && !isAffected(minX, y)) {
                minX++
            }
            if (minX == lastMinX + 10) {
                continue
            }

            var maxX = maxOf(minX, lastMaxX)
            while (isAffected(maxX, y)) {
                maxX++
            }

            ranges[y] = minX to maxX - 1

            if (y > 110) {
                val topY = y - 99
                val (_, topMaxX) = ranges[topY]!!

                if (topMaxX - minX == 99) {
                    return@run minX * 10000 + topY
                }
            }
        }
    }
    println("Part 2: $answer2")
}
