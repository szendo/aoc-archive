package com.szendo.aoc.y2019

import com.szendo.aoc.*
import java.io.FileOutputStream
import java.io.PrintStream
import java.math.BigInteger
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit
import kotlin.math.sign

fun main() {

    fun Map<Int, BigInteger>.mode(ip: Int, paramNumber: Int): Int {
        var mode = getValue(ip).toInt() / 100
        repeat(paramNumber - 1) {
            mode /= 10
        }
        mode %= 10
        return mode
    }

    operator fun Map<Int, BigInteger>.get(ip: Int, base: Int, paramNumber: Int): BigInteger {
        val addr = when (val mode = mode(ip, paramNumber)) {
            0 -> getValue(ip + paramNumber).toInt()
            1 -> ip + paramNumber
            2 -> base + getValue(ip + paramNumber).toInt()
            else -> error("Unknown parameter mode: $mode")
        }
        return getValue(addr)
    }

    operator fun MutableMap<Int, BigInteger>.set(ip: Int, base: Int, paramNumber: Int, value: BigInteger) {
        val addr: Int = when (val mode = mode(ip, paramNumber)) {
            0 -> getValue(ip + paramNumber).toInt()
            1 -> throw IllegalArgumentException("Invalid parameter mode: $mode")
            2 -> base + getValue(ip + paramNumber).toInt()
            else -> error("Unknown parameter mode: $mode")
        }
        this[addr] = value
    }

    fun execute(
        readonlyProgram: List<BigInteger>,
        input: BlockingQueue<BigInteger>,
        output: BlockingQueue<BigInteger>
    ) = Thread {
        val program = mutableMapOf(*readonlyProgram.withIndex().map { it.index to it.value }.toTypedArray())
            .withDefault { BigInteger.ZERO }
        var ip = 0
        var base = 0

        while (true) {
            val instr = program.getValue(ip).toInt()
            when (val opcode = instr % 100) {
                1 -> { // add
                    program[ip, base, 3] = program[ip, base, 1] + program[ip, base, 2]
                    ip += 4
                }

                2 -> { // multiply
                    program[ip, base, 3] = program[ip, base, 1] * program[ip, base, 2]
                    ip += 4
                }

                3 -> { // input
                    program[ip, base, 1] = input.take()
                    ip += 2
                }

                4 -> { // output
                    output.put(program[ip, base, 1])
                    ip += 2
                }

                5 -> { // jump-if-true
                    ip = if (program[ip, base, 1] != BigInteger.ZERO) program[ip, base, 2].toInt() else ip + 3
                }

                6 -> { // jump-if-false
                    ip = if (program[ip, base, 1] == BigInteger.ZERO) program[ip, base, 2].toInt() else ip + 3
                }

                7 -> { // less-than
                    program[ip, base, 3] =
                        if (program[ip, base, 1] < program[ip, base, 2]) BigInteger.ONE else BigInteger.ZERO
                    ip += 4
                }

                8 -> { // equals
                    program[ip, base, 3] =
                        if (program[ip, base, 2] == program[ip, base, 1]) BigInteger.ONE else BigInteger.ZERO
                    ip += 4
                }

                9 -> { // adjust-relative-base
                    base += program[ip, base, 1].toInt()
                    ip += 2
                }

                99 -> { // halt
                    return@Thread
                }

                else -> error("Unknown opcode: $opcode")
            }
        }
    }.apply { start() }

    val readonlyProgram = Helper.getResourceAsStream("2019/13.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val answer1 = run {
        val o = LinkedBlockingQueue<BigInteger>()
        execute(readonlyProgram, LinkedBlockingQueue(), o).join()

        var blockCount = 0
        while (true) {
            val x = (o.poll() ?: break).toInt()
            val y = (o.poll() ?: break).toInt()
            val t = (o.poll() ?: break).toInt()

            if (t == 2) {
                blockCount++

            }
        }

        blockCount
    }
    println("Part 1: $answer1")

    System.setErr(PrintStream(FileOutputStream("err.txt"), true, "UTF-8"))
    val answer2 = run {
        val hackedProgram = readonlyProgram.mapIndexed { index, value -> if (index == 0) 2.toBigInteger() else value }
        val i = LinkedBlockingQueue<BigInteger>()
        val o = LinkedBlockingQueue<BigInteger>()
        execute(hackedProgram, i, o)

        val grid = mutableMapOf<Coord, Char>()
            .withDefault { ' ' }
        var paddle = 0 to 0
        var ball = 0 to 0

        var score = 0
        while (true) {
            var gotGrid = false
            while (true) {
                val x = (o.poll(10, TimeUnit.MILLISECONDS) ?: break).toInt()
                val y = (o.poll(10, TimeUnit.MILLISECONDS) ?: break).toInt()
                val t = (o.poll(10, TimeUnit.MILLISECONDS) ?: break).toInt()
                gotGrid = true

                if (x < 0 || y < 0) {
                    System.err.println("New score: $t, diff: ${t - score}")
                    score = t
                } else  {

                    when (t) {
                        0 -> {
                            if (grid [x to y] == 'X') {
                                System.err.println("Broke $x, $y")
                            }
                        }
                        3 -> paddle = x to y
                        4 -> {
                            ball = x to y
                            if (grid [x to y] == '#') {
                                System.err.println("Broke $x, $y")
                            }
                        }
                    }

                    grid[x to y] = when (t) {
                        0 -> ' '
                        1 -> '#'
                        2 -> 'X'
                        3 -> '-'
                        4 -> 'O'
                        else -> error("Unknown tile: $t")
                    }

                }
            }

//            println("Score: $score")
//            (0..20).forEach { y ->
//                (0..37).forEach {x ->
//                    print(grid.getValue(x to y))
//                }
//                println()
//            }

            if (!gotGrid) {
                return@run score
            }

            i.put((ball.x - paddle.x).sign.toBigInteger())
        }
    }
    println("Part 2: $answer2")
}
