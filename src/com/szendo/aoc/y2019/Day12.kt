package com.szendo.aoc.y2019

import com.szendo.aoc.Helper
import com.szendo.aoc.lcm
import java.math.BigInteger

fun main() {

    data class Coord3D(val x: BigInteger, val y: BigInteger, val z: BigInteger) {
        val sign
            get() = Coord3D(
                this.x.signum().toBigInteger(),
                this.y.signum().toBigInteger(),
                this.z.signum().toBigInteger()
            )

        operator fun plus(other: Coord3D) = Coord3D(x + other.x, y + other.y, z + other.z)
        operator fun minus(other: Coord3D) = Coord3D(x - other.x, y - other.y, z - other.z)
    }


    data class Moon(val pos: Coord3D, val vel: Coord3D = Coord3D(BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO))

    val input = Helper.getResourceAsStream("2019/12.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { line ->
                (Regex("<x=(-?\\d+), y=(-?\\d+), z=(-?\\d+)>").matchEntire(line) ?: error("Invalid line: $line")).let {
                    Moon(
                        Coord3D(
                            it.groupValues[1].toBigInteger(),
                            it.groupValues[2].toBigInteger(),
                            it.groupValues[3].toBigInteger()
                        )
                    )
                }
            }.toList()
        }


    val seenStatesX = mutableSetOf(input.map { it.pos.x to it.vel.x })
    val seenStatesY = mutableSetOf(input.map { it.pos.y to it.vel.y })
    val seenStatesZ = mutableSetOf(input.map { it.pos.z to it.vel.z })

    var moons = input
    var n = 0
    var foundAnswer2 = false
    while (n <= 1000 || !foundAnswer2) {
        n++
        val gravity = moons.map { moon -> moon to moons.map { (it.pos - moon.pos).sign }.reduce { a, b -> a + b } }
        moons = gravity.map { (moon, dv) -> Moon(moon.pos + moon.vel + dv, moon.vel + dv) }

        if (n == 1000) {
            val answer1 = moons.map {
                (it.pos.x.abs() + it.pos.y.abs() + it.pos.z.abs()) * (it.vel.x.abs() + it.vel.y.abs() + it.vel.z.abs())
            }.reduce(BigInteger::add)
            println("Part 1: $answer1")
        }

        val newX = seenStatesX.add(moons.map { it.pos.x to it.vel.x })
        val newY = seenStatesY.add(moons.map { it.pos.y to it.vel.y })
        val newZ = seenStatesZ.add(moons.map { it.pos.z to it.vel.z })

        if (!(newX || newY || newZ)) {
            val x = seenStatesX.size.toBigInteger()
            val y = seenStatesY.size.toBigInteger()
            val z = seenStatesZ.size.toBigInteger()
            val answer2 = lcm(lcm(x, y), z)
            println("Part 2: $answer2")
            foundAnswer2 = true
        }
    }
}