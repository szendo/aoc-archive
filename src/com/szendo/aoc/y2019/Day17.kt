package com.szendo.aoc.y2019

import com.szendo.aoc.Helper

fun main() {
    val program = Helper.getResourceAsStream("2019/17.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val answer1 = run {
        val machine = IntCodeVM(program)
        machine.run()

        val grid = generateSequence { machine.receive() }
            .map { it.toInt().toChar() }
            .joinToString("").trim()
            .split('\r', '\n')

        (1 until grid.lastIndex).sumOf { y ->
            (1 until grid[y].lastIndex).sumOf { x ->
                if (grid[y][x] != '.'
                    && grid[y - 1][x] != '.'
                    && grid[y + 1][x] != '.'
                    && grid[y][x - 1] != '.'
                    && grid[y][x + 1] != '.'
                ) x * y else 0
            }
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val machine = IntCodeVM(program)
        machine[0] = 2.toBigInteger() // movement logic override

        "A,A,B,C,C,A,B,C,A,B\nL,12,L,12,R,12\nL,8,L,8,R,12,L,8,L,8\nL,10,R,8,R,12\nn\n".forEach {
            machine.send(it.code.toBigInteger())
        }
        machine.run()

        generateSequence { machine.receive() }.last()
    }
    println("Part 2: $answer2")
}