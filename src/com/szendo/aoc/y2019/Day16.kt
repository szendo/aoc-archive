package com.szendo.aoc.y2019

import com.szendo.aoc.Helper
import kotlin.math.abs

fun main() {
    val input = Helper.getResourceAsStream("2019/16.txt")
        .bufferedReader()
        .readText().trim()
        .map { it - '0' }

    fun pattern(el: Int, pos: Int) =
        when (val rem = (pos + 1) / (el + 1) % 4) {
            0 -> 0
            1 -> 1
            2 -> 0
            3 -> -1
            else -> error("Invalid remainder: $rem")
        }

    val answer1 = run {
        var signal = input
        repeat(100) {
            signal = signal.mapIndexed { el, _ ->
                abs(signal.mapIndexed { pos, digit -> digit * pattern(el, pos) }.sum()) % 10
            }
        }

        signal.subList(0, 8).joinToString("")
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var signal = (1..10000).flatMap { input }
        val offset = signal.subList(0, 7).joinToString("").toInt()
        check(offset >= signal.size / 2) { "Offset is less than half of the signal length"}
        signal = signal.subList(offset, signal.size)

        repeat(100) {
            signal = signal
                .asReversed()
                .fold(mutableListOf<Int>()) { m, digit -> m.apply { add(((m.lastOrNull() ?: 0) + digit) % 10) } }
                .asReversed()
        }
        signal.subList(0, 8).joinToString("")
    }
    println("Part 2: $answer2")
}