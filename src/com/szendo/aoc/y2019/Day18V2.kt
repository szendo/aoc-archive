package com.szendo.aoc.y2019

import com.szendo.aoc.*

fun main() {
    fun findPos(grid: Grid<Char>, c: Char) =
        grid.flatMapIndexed { y, row ->
            row.withIndex().firstOrNull { it.value == c }?.let { (x, _) -> listOf(x to y) } ?: emptyList()
        }.first()

    fun isPassable(c: Char) = c == '.' || c in 'a'..'z' || c in 'A'..'Z' || c in "1234"

    fun shortestPathWithDeps(grid: Grid<Char>, startPos: Coord, endPos: Coord): Pair<Int, Set<Char>> {
        val height = grid.size
        val width = grid[0].size

        val seenPos = mutableSetOf<Coord>()
        val queue = mutableListOf((startPos to setOf<Char>()) to 0)
        while (queue.isNotEmpty()) {
            val (state, steps) = queue.removeFirst()
            val (pos, deps) = state
            if (pos == endPos) return steps to deps
            listOf(-1 to 0, 0 to -1, 0 to 1, 1 to 0).forEach { d ->
                val (x, y) = pos + d
                if (x in (0 until width) && y in (0 until height) && isPassable(grid[y][x]) && seenPos.add(x to y)) {
                    val floor = grid[y][x]
                    queue.add(((x to y) to if (floor in 'A'..'Z') deps + (floor.lowercaseChar()) else deps) to steps + 1)
                }
            }
        }
        return -1 to setOf()
    }

    val answer1 = run {
        val input = Helper.getResourceAsStream("2019/18.txt")
            .bufferedReader()
            .useLines { lines -> lines.toList() }
            .map { it.toList() }

        val allKeys = ('a'..'z')
            .filter { input.any { row -> it in row } }
            .toSet()

        val startPos = '@' to setOf<Char>()

        val paths = (setOf('@') + allKeys).associateWith { start ->
            allKeys.filter { it != start }.associateWith { end ->
                shortestPathWithDeps(input, findPos(input, start), findPos(input, end))
            }
        }

        shortestPathDijkstra(source = startPos, isTarget = { this.second == allKeys }) { (curr, keys) ->
            (allKeys - keys)
                .filter { next -> keys.containsAll(paths.getValue(curr).getValue(next).second) }
                .associate { next -> (next to keys + next) to paths.getValue(curr).getValue(next).first }
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val input = Helper.getResourceAsStream("2019/18p2.txt")
            .bufferedReader()
            .useLines { lines -> lines.toList() }
            .map { it.toList() }

        val allKeys = ('a'..'z')
            .filter { input.any { row -> it in row } }
            .toSet()

        val startPos = "1234".toList() to setOf<Char>()

        val paths = ("1234".toList() + allKeys).associateWith { start ->
            allKeys
                .filter { it != start }
                .associateWith { end ->
                    shortestPathWithDeps(input, findPos(input, start), findPos(input, end))
                }
        }

        shortestPathDijkstra(source = startPos, isTarget = { this.second == allKeys }) { (currs, keys) ->
            (allKeys - keys).flatMap { next ->
                currs
                    .filter { curr ->
                        val (cost, deps) = paths.getValue(curr).getValue(next)
                        cost != -1 && keys.containsAll(deps)
                    }
                    .map { curr ->
                        val (cost, _) = paths.getValue(curr).getValue(next)
                        (currs.map { if (it == curr) next else it } to keys + next) to cost
                    }
            }.toMap()
        }
    }
    println("Part 2: $answer2")
}
