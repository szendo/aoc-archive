package com.szendo.aoc.y2019

import com.szendo.aoc.*
import java.math.BigInteger
import kotlin.math.sign

fun main() {
    val program = Helper.getResourceAsStream("2019/13.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val answer1 = run {
        val machine = IntCodeVM(program)
        var blockCount = 0
        do {
            machine.run()

            blockCount += generateSequence { machine.receive() }
                .chunked(3)
                .map { it.map(BigInteger::toInt) }
                .count { (_, _, t) -> t == 2 }
        } while (!machine.isHalted)

        blockCount
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val machine = IntCodeVM(program)
        machine[0] = 2.toBigInteger() // number of quarters inserted; set it to 2 to play for free

//        val grid = mutableMapOf<Coord, Char>()
//            .withDefault { ' ' }
        var paddle = 0 to 0
        var ball = 0 to 0

        var score = 0
        do {
            machine.run()
            generateSequence { machine.receive() }.chunked(3)
                .map { it.map(BigInteger::toInt) }
                .forEach { (x, y, t) ->
                    if (x < 0 || y < 0) {
                        score = t
                    } else {
                        when (t) {
                            3 -> paddle = x to y
                            4 -> ball = x to y
                        }

//                        grid[x to y] = when (t) {
//                            0 -> ' '
//                            1 -> '#'
//                            2 -> 'X'
//                            3 -> '-'
//                            4 -> 'O'
//                            else -> error("Unknown tile: $t")
//                        }
                    }
                }

            if (!machine.isHalted) {
                machine.send((ball.x - paddle.x).sign.toBigInteger())
            }

        } while (!machine.isHalted)
        score
    }
    println("Part 2: $answer2")
}
