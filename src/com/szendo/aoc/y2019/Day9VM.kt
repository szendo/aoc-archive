package com.szendo.aoc.y2019

import com.szendo.aoc.Helper
import java.math.BigInteger

fun main() {
    val program = Helper.getResourceAsStream("2019/9.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    val answer1: BigInteger = run {
        val machine = IntCodeVM(program)
        machine.send(BigInteger.ONE)
        machine.run()
        machine.receive()!!
    }
    println("Part 1: $answer1")

    val answer2: BigInteger = run {
        val machine = IntCodeVM(program)
        machine.send(2.toBigInteger())
        machine.run()
        machine.receive()!!
    }
    println("Part 2: $answer2")
}