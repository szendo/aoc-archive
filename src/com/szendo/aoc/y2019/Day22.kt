package com.szendo.aoc.y2019

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2019/22.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val cut = "cut "
    val dealInto = "deal into new stack"
    val dealWith = "deal with increment "

    val answer1 = run {
        var deck = (0..10006).toList()
        for (line in input) {
            if (line == dealInto) {
                deck = deck.reversed()
            } else if (line.startsWith(cut)) {
                val n = line.substring(cut.length).toInt()
                deck = if (n > 0) {
                    deck.subList(n, deck.size) + deck.subList(0, n)
                } else {
                    deck.subList(deck.size + n, deck.size) + deck.subList(0, deck.size + n)
                }
            } else if (line.startsWith(dealWith)) {
                val inc = line.substring(dealWith.length).toInt()
                val newDeck = IntArray(deck.size)
                var pos = 0
                for (card in deck) {
                    newDeck[pos] = card
                    pos = (pos + inc) % deck.size
                }
                deck = newDeck.toList()
            }
        }

        deck.indexOf(2019)
    }
    println("Part 1: $answer1")
}
