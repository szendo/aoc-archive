package com.szendo.aoc.y2019

import java.math.BigInteger
import java.math.BigInteger.*
import java.util.*

class IntCodeVM(program: List<BigInteger>) {
    private var memory = mutableMapOf(*program.withIndex().map { it.index.toBigInteger() to it.value }.toTypedArray())
        .withDefault { ZERO }
    private var ip = ZERO
    private var base = ZERO

    private val input: Queue<BigInteger> = LinkedList()
    private val output: Queue<BigInteger> = LinkedList()

    var isHalted = false
        private set

    operator fun get(addr: Int) = memory[addr.toBigInteger()]

    operator fun get(addr: Long) = memory[addr.toBigInteger()]

    operator fun get(addr: BigInteger) = memory[addr]

    operator fun set(addr: Int, value: BigInteger) {
        memory[addr.toBigInteger()] = value
    }

    operator fun set(addr: Long, value: BigInteger) {
        memory[addr.toBigInteger()] = value
    }

    operator fun set(addr: BigInteger, value: BigInteger) {
        memory[addr] = value
    }

    fun send(value: BigInteger) = if (isHalted) error("Program halted") else input.offer(value)

    fun receive(): BigInteger? = output.poll()

    fun run(): Boolean {
        if (isHalted) error("Program halted")

        while (true) {
            val instr = memory.getValue(ip).toInt()
            when (val opcode = instr % 100) {
                1 -> { // add
                    memory[ip, 3] = memory[ip, 1] + memory[ip, 2]
                    ip += 4.toBigInteger()
                }

                2 -> { // multiply
                    memory[ip, 3] = memory[ip, 1] * memory[ip, 2]
                    ip += 4.toBigInteger()
                }

                3 -> { // input
                    memory[ip, 1] = (input.poll() ?: return true)
                    ip += 2.toBigInteger()
                }

                4 -> { // output
                    output.add(memory[ip, 1])
                    ip += 2.toBigInteger()
                }

                5 -> { // jump-if-true
                    ip = if (memory[ip, 1] != ZERO) memory[ip, 2] else ip + 3.toBigInteger()
                }

                6 -> { // jump-if-false
                    ip = if (memory[ip, 1] == ZERO) memory[ip, 2] else ip + 3.toBigInteger()
                }

                7 -> { // less-than
                    memory[ip, 3] = if (memory[ip, 1] < memory[ip, 2]) ONE else ZERO
                    ip += 4.toBigInteger()
                }

                8 -> { // equals
                    memory[ip, 3] = if (memory[ip, 2] == memory[ip, 1]) ONE else ZERO
                    ip += 4.toBigInteger()
                }

                9 -> { // adjust-relative-base
                    base += memory[ip, 1]
                    ip += 2.toBigInteger()
                }

                99 -> { // halt
                    isHalted = true
                    return false
                }

                else -> error("Unknown opcode: $opcode")
            }
        }
    }

    private fun Map<BigInteger, BigInteger>.mode(ip: BigInteger, paramNumber: Int): Int {
        var mode = getValue(ip) / 100.toBigInteger()
        repeat(paramNumber - 1) {
            mode /= TEN
        }
        mode %= TEN
        return mode.toInt()
    }

    private operator fun Map<BigInteger, BigInteger>.get(ip: BigInteger, paramNumber: Int): BigInteger {
        val addr = when (val mode = mode(ip, paramNumber)) {
            0 -> getValue(ip + paramNumber.toBigInteger())
            1 -> ip + paramNumber.toBigInteger()
            2 -> base + getValue(ip + paramNumber.toBigInteger())
            else -> error("Unknown parameter mode: $mode")
        }
        return getValue(addr)
    }

    private operator fun MutableMap<BigInteger, BigInteger>.set(ip: BigInteger, paramNumber: Int, value: BigInteger) {
        val addr = when (val mode = mode(ip, paramNumber)) {
            0 -> getValue(ip + paramNumber.toBigInteger())
            1 -> error("Invalid parameter mode: $mode")
            2 -> base + getValue(ip + paramNumber.toBigInteger())
            else -> error("Unknown parameter mode: $mode")
        }
        this[addr] = value
    }
}