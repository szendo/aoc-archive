package com.szendo.aoc.y2019

import com.szendo.aoc.Helper
import java.math.BigInteger
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

fun main() {

    operator fun List<BigInteger>.get(ip: Int, base: Int, paramNumber: Int): BigInteger {
        var mode = this[ip].toInt() / 100
        repeat(paramNumber - 1) {
            mode /= 10
        }
        mode %= 10

        val addr = when (mode) {
            0 -> this[ip + paramNumber].toInt()
            1 -> ip + paramNumber
            2 -> base + this[ip + paramNumber].toInt()
            else -> error("Unknown parameter mode: $mode")
        }
        return if (addr > this.lastIndex) BigInteger.ZERO else this[addr]
    }

    operator fun MutableList<BigInteger>.set(ip: Int, base: Int, paramNumber: Int, value: BigInteger) {
        var mode = this[ip].toInt() / 100
        repeat(paramNumber - 1) {
            mode /= 10
        }
        mode %= 10

        val addr: Int = when (mode) {
            0 -> this[ip + paramNumber].toInt()
            1 -> ip + paramNumber
            2 -> base + this[ip + paramNumber].toInt()
            else -> error("Unknown parameter mode: $mode")
        }

        if (addr > this.lastIndex) {
            this.addAll(List(addr - this.lastIndex) { BigInteger.ZERO })
        }
        this[addr] = value
    }

    fun execute(
        readonlyProgram: List<BigInteger>,
        input: BlockingQueue<BigInteger>,
        output: BlockingQueue<BigInteger>
    ) = Thread {
        val program = readonlyProgram.toMutableList()
        var ip = 0
        var base = 0

        while (true) {
            val instr = program[ip].toInt()
            when (val opcode = instr % 100) {
                1 -> { // add
                    program[ip, base, 3] = program[ip, base, 1] + program[ip, base, 2]
                    ip += 4
                }

                2 -> { // multiply
                    program[ip, base, 3] = program[ip, base, 1] * program[ip, base, 2]
                    ip += 4
                }

                3 -> { // input
                    program[ip, base, 1] = input.take()
                    ip += 2
                }

                4 -> { // output
                    output.put(program[ip, base, 1])
                    ip += 2
                }

                5 -> { // jump-if-true
                    ip = if (program[ip, base, 1] != BigInteger.ZERO) program[ip, base, 2].toInt() else ip + 3
                }

                6 -> { // jump-if-false
                    ip = if (program[ip, base, 1] == BigInteger.ZERO) program[ip, base, 2].toInt() else ip + 3
                }

                7 -> { // less-than
                    program[ip, base, 3] =
                        if (program[ip, base, 1] < program[ip, base, 2]) BigInteger.ONE else BigInteger.ZERO
                    ip += 4
                }

                8 -> { // equals
                    program[ip, base, 3] =
                        if (program[ip, base, 2] == program[ip, base, 1]) BigInteger.ONE else BigInteger.ZERO
                    ip += 4
                }

                9 -> { // adjust-relative-base
                    base += program[ip, base, 1].toInt()
                    ip += 2
                }

                99 -> { // halt
                    return@Thread
                }

                else -> error("Unknown opcode: $opcode")
            }
        }
    }.apply { start() }

    val readonlyProgram = Helper.getResourceAsStream("2019/9.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }


    val answer1: BigInteger = run {
        val io1 = LinkedBlockingQueue<BigInteger>(listOf(BigInteger.ONE))
        val io2 = LinkedBlockingQueue<BigInteger>()
        execute(readonlyProgram, io1, io2).join()
        io2.remove()
    }
    println("Part 1: $answer1")

    val answer2: BigInteger = run {
        val io1 = LinkedBlockingQueue<BigInteger>(listOf(BigInteger.valueOf(2)))
        val io2 = LinkedBlockingQueue<BigInteger>()
        execute(readonlyProgram, io1, io2).join()
        io2.remove()
    }
    println("Part 2: $answer2")

}