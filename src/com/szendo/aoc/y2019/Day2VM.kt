package com.szendo.aoc.y2019

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2019/2.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toInt() }
        .toList()

    val answer1 = IntCodeVMLite(input).apply { this[1] = 2; this[2] = 12; run() }[0]
    println("Part 1: $answer1")

    val answer2 = run {
        val (noun, verb) = (0..99).flatMap { noun ->
            (0..99).filter { verb ->
                IntCodeVMLite(input).apply { this[1] = noun; this[2] = verb; run() }[0] == 19690720
            }.map { noun to it }
        }.single()
        (100 * noun) + verb
    }
    println("Part 2: $answer2")
}