package com.szendo.aoc.y2019

import com.szendo.aoc.*
import java.math.BigInteger

fun main() {
    val program = Helper.getResourceAsStream("2019/23.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toBigInteger() }

    fun IntCodeVM.runAndSendQueuedPackets(
        addr: Int, packetQueue: Map<Int, MutableList<Pair<BigInteger, BigInteger>>>
    ) {
        val waitingForInput = run()
        if (waitingForInput) {
            val queue = packetQueue.getValue(addr)
            if (queue.isEmpty()) {
                send((-1).toBigInteger())
            } else {
                while (queue.isNotEmpty()) {
                    val (x, y) = queue.removeFirst()
                    send(x)
                    send(y)
                }
            }
        }
    }

    val answer1 = run {
        val machines = List(50) { addr ->
            IntCodeVM(program).apply { send(addr.toBigInteger()) }
        }

        val packetQueue = machines.indices.associateWith { mutableListOf<Pair<BigInteger, BigInteger>>() }
        while (true) {
            machines.forEachIndexed { addr, machine ->
                machine.runAndSendQueuedPackets(addr, packetQueue)
                generateSequence { machine.receive() }.chunked(3)
                    .forEach { (a, x, y) ->
                        if (a.toInt() == 255) return@run y
                        packetQueue[a.toInt()]?.add(x to y)
                    }
            }
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val machines = List(50) { addr ->
            IntCodeVM(program).apply { send(addr.toBigInteger()) }
        }

        var natPacket = BigInteger.ZERO to BigInteger.ZERO
        var lastDelivered: Pair<BigInteger, BigInteger>? = null
        val packetQueue = machines.indices.associateWith { mutableListOf<Pair<BigInteger, BigInteger>>() }
        while (true) {
            val emptyQueue = packetQueue.all { it.value.size == 0 }
            val sentPackets = machines.withIndex().sumOf { (addr, machine) ->
                machine.runAndSendQueuedPackets(addr, packetQueue)
                generateSequence { machine.receive() }.chunked(3)
                    .onEach { (a, x, y) ->
                        if (a.toInt() == 255) {
                            natPacket = x to y
                        } else {
                            packetQueue[a.toInt()]?.add(x to y)
                        }
                    }.count()
            }

            if (emptyQueue && sentPackets == 0) {
                if (lastDelivered != null && lastDelivered.second == natPacket.second) {
                    return@run natPacket.second
                }
                machines[0].send(natPacket.first)
                machines[0].send(natPacket.second)
                lastDelivered = natPacket
            }
        }
    }
    println("Part 2: $answer2")
}
