package com.szendo.aoc.y2019

import com.szendo.aoc.Helper
import kotlin.math.max

fun main() {
    fun fuel(n: Int) = max(0, n / 3 - 2)
    fun recursiveFuel(n: Int): Int {
        val fuel = fuel(n)
        return if (fuel > 0) fuel + recursiveFuel(fuel) else 0
    }
    // fun recursiveFuel2(n: Int) = generateSequence(fuel(n), ::fuel).takeWhile { it > 0 }.sum()
    // fun recursiveFuel3(n: Int) = generateSequence(n, ::fuel).takeWhile { it > 0 }.drop(1).sum()

    val masses = Helper.getResourceAsStream("2019/1.txt")
        .bufferedReader()
        .useLines { lines -> lines.map { it.toInt() }.toList() }

    val answer1 = masses.map(::fuel).sum()
    println("Part 1: $answer1")

    val answer2 = masses.map(::recursiveFuel).sum()
    println("Part 2: $answer2")
}