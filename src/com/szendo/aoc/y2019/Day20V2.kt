package com.szendo.aoc.y2019

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2019/20.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.toList() }

    fun shortestPath(grid: Grid<Char>, startPos: Coord, endPos: Coord, shortcuts: Map<Coord, Pair<Coord, Int>>): Int {
        val height = grid.size
        val width = grid.maxOf { it.size }

        return shortestPathBfs((startPos to 0), (endPos to 0)) { (pos, depth): Pair<Pair<Int, Int>, Int> ->
            setOf(-1 to 0, 0 to -1, 0 to 1, 1 to 0).map { pos + it }
                .filter { (x, y) -> x in (0 until width) && y in (0 until height) && grid[y][x] == '.' }
                .map { it to depth }.toSet() +
                (shortcuts[pos]?.let { (newPos, depthDiff) ->
                    if (depth + depthDiff >= 0) setOf(newPos to depth + depthDiff) else setOf()
                } ?: setOf())
        }
    }

    val answer1 = run {
        val portals = (input.flatMapIndexed { y, row ->
            row.windowed(3).flatMapIndexed { x, chars ->
                if (chars[0] == '.' && chars[1] in 'A'..'Z' && chars[2] in 'A'..'Z') {
                    listOf(chars.subList(1, 3).joinToString("") to (x to y))
                } else if (chars[2] == '.' && chars[0] in 'A'..'Z' && chars[1] in 'A'..'Z') {
                    listOf(chars.subList(0, 2).joinToString("") to (x + 2 to y))
                } else emptyList()
            }
        } + input.windowed(3).flatMapIndexed { y, rows ->
            (0..rows.minOf { it.lastIndex }).flatMap { x ->
                if (rows[0][x] == '.' && rows[1][x] in 'A'..'Z' && rows[2][x] in 'A'..'Z') {
                    listOf("${rows[1][x]}${rows[2][x]}" to (x to y))
                } else if (rows[2][x] == '.' && rows[0][x] in 'A'..'Z' && rows[1][x] in 'A'..'Z') {
                    listOf("${rows[0][x]}${rows[1][x]}" to (x to y + 2))
                } else emptyList()
            }
        }).groupBy({ it.first }) { it.second }

        val startPos = portals.getValue("AA")[0]
        val endPos = portals.getValue("ZZ")[0]

        val shortcuts = (portals - setOf("AA", "ZZ")).flatMap { (_, c) ->
            listOf(c[0] to (c[1] to 0), c[1] to (c[0] to 0))
        }.toMap()

        shortestPath(input, startPos, endPos, shortcuts)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val portals = (input.flatMapIndexed { y, row ->
            row.windowed(3).flatMapIndexed { x, chars ->
                if (chars[0] == '.' && chars[1] in 'A'..'Z' && chars[2] in 'A'..'Z') {
                    listOf(chars.subList(1, 3).joinToString("") to ((x to y) to (x + 2 == row.lastIndex)))
                } else if (chars[2] == '.' && chars[0] in 'A'..'Z' && chars[1] in 'A'..'Z') {
                    listOf(chars.subList(0, 2).joinToString("") to ((x + 2 to y) to (x == 0)))
                } else emptyList()
            }
        } + input.windowed(3).flatMapIndexed { y, rows ->
            (0..rows.minOf { it.lastIndex }).flatMap { x ->
                if (rows[0][x] == '.' && rows[1][x] in 'A'..'Z' && rows[2][x] in 'A'..'Z') {
                    listOf("${rows[1][x]}${rows[2][x]}" to ((x to y) to (y + 2 == input.lastIndex)))
                } else if (rows[2][x] == '.' && rows[0][x] in 'A'..'Z' && rows[1][x] in 'A'..'Z') {
                    listOf("${rows[0][x]}${rows[1][x]}" to ((x to y + 2) to (y == 0)))
                } else emptyList()
            }
        }).groupBy({ it.first }) { it.second }

        val startPos = portals.getValue("AA")[0].first
        val endPos = portals.getValue("ZZ")[0].first

        val shortcuts = (portals - setOf("AA", "ZZ")).flatMap { (_, c) ->
            listOf(
                c[0].first to (c[1].first to if (c[1].second) 1 else -1),
                c[1].first to (c[0].first to if (c[0].second) 1 else -1),
            )
        }.toMap()

        shortestPath(input, startPos, endPos, shortcuts)
    }
    println("Part 2: $answer2")
}
