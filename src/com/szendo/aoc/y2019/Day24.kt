package com.szendo.aoc.y2019

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2019/24.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.toList() }

    fun countAdjacentBugs(grid: Grid<Char>, pos: Coord) =
        listOf(-1 to 0, 0 to -1, 0 to 1, 1 to 0).count { d ->
            try {
                grid[pos + d] == '#'
            } catch (ignored: IndexOutOfBoundsException) {
                false
            }
        }

    val answer1 = run {
        val seenGrids = mutableSetOf<Grid<Char>>()

        var grid = input
        while (seenGrids.add(grid)) {
            grid = grid.mapIndexed { y, row ->
                row.mapIndexed { x, c ->
                    if (c == '#') if (countAdjacentBugs(grid, x to y) == 1) '#' else '.'
                    else if (countAdjacentBugs(grid, x to y) in 1..2) '#' else '.'
                }
            }
        }

        grid.flatten().map { if (it == '#') 1 else 0 }.reduceRight { i, acc -> 2 * acc + i }
    }
    println("Part 1: $answer1")

    fun getAdjacentCoords(coord: Triple<Int, Int, Int>) =
        listOf(-1 to 0, 0 to -1, 0 to 1, 1 to 0).flatMap { coordDelta ->
            val x = coord.first + coordDelta.first
            val y = coord.second + coordDelta.second
            val depth = coord.third
            if (x < 0) {
                listOf(Triple(1, 2, depth - 1))
            } else if (x > 4) {
                listOf(Triple(3, 2, depth - 1))
            } else if (y < 0) {
                listOf(Triple(2, 1, depth - 1))
            } else if (y > 4) {
                listOf(Triple(2, 3, depth - 1))
            } else if (x == 2 && y == 2) {
                if (coord.first == 1) {
                    (0..4).map { innerY -> Triple(0, innerY, depth + 1) }
                } else if (coord.first == 3) {
                    (0..4).map { innerY -> Triple(4, innerY, depth + 1) }
                } else if (coord.second == 1) {
                    (0..4).map { innerX -> Triple(innerX, 0, depth + 1) }
                } else if (coord.second == 3) {
                    (0..4).map { innerX -> Triple(innerX, 4, depth + 1) }
                } else error("Invalid neighbor for ($x;$y): (${coord.first};${coord.second})")
            } else {
                listOf(Triple(x, y, depth))
            }
        }

    val answer2 = run {
        var bugs = input.flatMapIndexed { y, row ->
            row.withIndex()
                .filter { (_, c) -> c == '#' }
                .map { (x, _) -> Triple(x, y, 0) }
        }.toSet()

        repeat(200) {
            bugs = bugs.flatMap { getAdjacentCoords(it) }.groupingBy { it }.eachCount()
                .filter { (coord, count) ->
                    if (coord in bugs) count == 1 else count in 1..2
                }.keys
        }

        bugs.size
    }
    println("Part 2: $answer2")
}
