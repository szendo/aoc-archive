package com.szendo.aoc.y2019

import com.szendo.aoc.Helper

fun main() {

    operator fun List<Int>.get(ip: Int, n: Int) = this[this[ip + n]]
    operator fun MutableList<Int>.set(ip: Int, n: Int, value: Int) {
        this[this[ip + n]] = value
    }

    fun execute(input: List<Int>, noun: Int, verb: Int): Int {
        val program = input.toMutableList()
        program[1] = noun
        program[2] = verb

        var ip = 0
        while (true) {
            when (val instr = program[ip]) {
                1 -> { // add
                    program[ip, 3] = program[ip, 1] + program[ip, 2]
                    ip += 4
                }

                2 -> { // multiply
                    program[ip, 3] = program[ip, 1] * program[ip, 2]
                    ip += 4
                }

                99 -> { // halt
                    return program[0]
                }

                else -> error("Unknown instruction: $instr")
            }
        }
    }

    val input = Helper.getResourceAsStream("2019/2.txt")
        .bufferedReader()
        .readText().trim()
        .split(',')
        .map { it.toInt() }
        .toList()

    val answer1 = execute(input, 12, 2)
    println("Part 1: $answer1")


    (0..99).forEach { noun ->
        (0..99).forEach { verb ->
            if (execute(input, noun, verb) == 19690720) {
                val answer2 = (100 * noun) + verb
                println("Part 2: $answer2")
            }
        }
    }
}