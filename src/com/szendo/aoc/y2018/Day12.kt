package com.szendo.aoc.y2018

fun main() {
    val input = "#.#.#...#..##..###.##.#...#.##.#....#..#.#....##.#.##...###.#...#######.....##.###.####.#....#.#..##"
    val rules = mapOf(
        ("#...#" to "#"), ("....#" to "."), ("##..#" to "#"), (".#.##" to "#"),
        ("##.##" to "."), ("###.#" to "#"), ("....." to "."), ("...#." to "."),
        (".#.#." to "#"), ("#.##." to "#"), ("..#.#" to "#"), (".#..." to "#"),
        ("#.#.." to "."), ("##.#." to "."), (".##.." to "#"), ("#..#." to "."),
        (".###." to "."), ("..#.." to "."), ("#.###" to "."), ("..##." to "."),
        (".#..#" to "#"), (".##.#" to "."), (".####" to "."), ("...##" to "#"),
        ("#.#.#" to "#"), ("..###" to "."), ("#..##" to "."), ("####." to "#"),
        ("#####" to "."), ("###.." to "#"), ("##..." to "#"), ("#...." to ".")
    )

    var state = input
    var offset = 0
    var gen = 0L
    var offsetDelta: Int
    while (true) {
        gen++
        val nextState = "....$state....".windowed(5) { rules[it] }.joinToString("")
        offsetDelta = nextState.indexOf('#') - 2
        offset += offsetDelta
        if (state == nextState.trim('.')) {
            break
        }
        state = nextState.trim('.')

        if (gen == 20L) {
            val answer1 = state.mapIndexed { i, c -> if (c == '#') offset + i else 0 }.sum()
            println("Answer 1: $answer1")
        }
    }

    println("Stable after $gen generations: $state; offset=$offset (+$offsetDelta/gen)")

    if (gen < 20L) {
        (offset + (20L - gen) * offsetDelta).let { finalOffset ->
            val answer1 = state.mapIndexed { i, c -> if (c == '#') finalOffset + i else 0 }.sum()
            println("Answer 1: $answer1")
        }
    }

    (offset + (50_000_000_000L - gen) * offsetDelta).let { finalOffset ->
        val answer2 = state.mapIndexed { i, c -> if (c == '#') finalOffset + i else 0 }.sum()
        println("Answer 2: $answer2")
    }
}