package com.szendo.aoc.y2018

fun main() {

    val initialState =
        "#.#.#...#..##..###.##.#...#.##.#....#..#.#....##.#.##...###.#...#######.....##.###.####.#....#.#..##"
    val rules = mapOf(
        "#...#" to "#",
        "....#" to ".",
        "##..#" to "#",
        ".#.##" to "#",
        "##.##" to ".",
        "###.#" to "#",
        "....." to ".",
        "...#." to ".",
        ".#.#." to "#",
        "#.##." to "#",
        "..#.#" to "#",
        ".#..." to "#",
        "#.#.." to ".",
        "##.#." to ".",
        ".##.." to "#",
        "#..#." to ".",
        ".###." to ".",
        "..#.." to ".",
        "#.###" to ".",
        "..##." to ".",
        ".#..#" to "#",
        ".##.#" to ".",
        ".####" to ".",
        "...##" to "#",
        "#.#.#" to "#",
        "..###" to ".",
        "#..##" to ".",
        "####." to "#",
        "#####" to ".",
        "###.." to "#",
        "##..." to "#",
        "#...." to "."
    )

    var state = mapOf(*initialState.mapIndexed { index, c -> index to (c == '#') }.toTypedArray())
    var minIndex = state.minOf { it.key }
    var maxIndex = state.maxOf { it.key }

    repeat(200) {
        state = mapOf(*((minIndex - 2)..(maxIndex + 2)).map { i ->
            val local = (-2..2).joinToString("") { off -> if (state[i + off] == true) "#" else "." }
            i to (rules[local] == "#")
        }.filter { it.second }.toTypedArray())

        minIndex = state.minOf { it.key }
        maxIndex = state.maxOf { it.key }

        println((-5..maxIndex).joinToString("") { if (state[it] == true) "#" else "." }.padEnd(305, '.'))
    }

    val answer1 = state.filterValues { it }.keys.sum()
    println("Answer 1: $answer1")

}