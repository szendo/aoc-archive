package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    val input: List<String> = Helper.getResourceAsStream("2018/2.txt")
        .bufferedReader()
        .useLines { it.toList() }

    var l2 = 0L
    var l3 = 0L
    for (id in input) {
        val charStats = mutableMapOf<Char, Int>()

        for (c in id) {
            charStats.merge(c, 1) { a, b -> a + b }
        }

        if (charStats.any { it.value == 2 }) {
            l2++
        }

        if (charStats.any { it.value == 3 }) {
            l3++
        }
    }

    val answer1 = l2 * l3
    println("Answer 1: $answer1")
}
