package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    val regex = Regex(('a'..'z').joinToString("|") { "$it${it.uppercaseChar()}|${it.uppercaseChar()}$it" })

    fun compute(s: String): Int {
        var p = s
        while (true) {
            val r = regex.replace(p, "")
            if (p == r) {
                return r.length
            }
            p = r
        }
    }

    val input = Helper.getResourceAsStream("2018/5.txt").bufferedReader().readText().trim()

    println("Answer 1: ${compute(input)}")

    var char = 'a'
    var len = Int.MAX_VALUE
    for (c in 'a'..'z') {
        val l = compute(input.replace(c.toString(), "", true))
        if (l < len) {
            char = c
            len = l
        }
    }

    println("Answer 2: $len ($char)")
}

