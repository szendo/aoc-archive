package com.szendo.aoc.y2018

fun main() {
    val input = 793031

    val recipes = StringBuilder("37")
    var i1 = 0
    var i2 = 1

    while (recipes.length < input + 10) {
        val score1 = (recipes[i1] - '0')
        val score2 = (recipes[i2] - '0')
        recipes.append(score1 + score2)

        i1 = (i1 + 1 + score1) % recipes.length
        i2 = (i2 + 1 + score2) % recipes.length
    }

    val answer1 = recipes.substring(input, input + 10)
    println("Answer 1: $answer1")

    while (recipes.indexOf(input.toString(), startIndex = recipes.length - 8) == -1) {
        val score1 = (recipes[i1] - '0')
        val score2 = (recipes[i2] - '0')
        recipes.append(score1 + score2)

        i1 = (i1 + 1 + score1) % recipes.length
        i2 = (i2 + 1 + score2) % recipes.length
    }

    val answer2 = recipes.indexOf(input.toString(), startIndex = recipes.length - 8)
    println("Answer 2: $answer2")
}