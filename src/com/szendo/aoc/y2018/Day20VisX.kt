package com.szendo.aoc.y2018

import com.szendo.aoc.*
import java.awt.Color
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO
import kotlin.math.abs
import kotlin.math.min

private const val SCALE = 3

fun main() {
    val input = Helper.getResourceAsStream("2018/20.txt")
        .bufferedReader()
        .useLines { it.first() }


    val (topLeft, bottomRight) = getBounds(input)
    val width = bottomRight.x - topLeft.x + 1
    val height = bottomRight.y - topLeft.y + 1

    val image = BufferedImage(SCALE * (2 * width + 1), SCALE * (2 * height + 1), BufferedImage.TYPE_INT_RGB)
    val g = image.createGraphics()

    val pathImage = BufferedImage(SCALE * (2 * width + 1), SCALE * (2 * height + 1), BufferedImage.TYPE_INT_RGB)
    val pg = pathImage.createGraphics()
    pg.color = Color.BLACK
    pg.fillRect(0, 0, image.width, image.height)
    pg.color = Color.WHITE

    val stack = ArrayDeque<Coord>()
    var currentCoord = (0 to 0) - topLeft
    val distanceMap = mutableMapOf(currentCoord to 0)
    val prevMap = mutableMapOf<Coord, Coord>()

    pg.color = Color.RED
    pg.fillRect(
        SCALE * (currentCoord.x * 2 + 1),
        SCALE * (currentCoord.y * 2 + 1),
        SCALE, SCALE
    )
    pg.color = Color.WHITE

    g.drawImage(pathImage, 0, 0, null)
    render(pathImage, image, g, setOf(currentCoord), listOf(), prevMap)

    input.toCharArray().forEach { c ->
        val distance = distanceMap[currentCoord] ?: throw RuntimeException()
        when (c) {
            '(' -> stack.push(currentCoord)
            '|' -> currentCoord = stack.peek()
            ')' -> currentCoord = stack.pop()
            '^', '$' -> Unit
            else -> {
                val prevCoord = currentCoord
                currentCoord = when (c) {
                    'N' -> currentCoord.x to currentCoord.y - 1
                    'S' -> currentCoord.x to currentCoord.y + 1
                    'W' -> currentCoord.x - 1 to currentCoord.y
                    'E' -> currentCoord.x + 1 to currentCoord.y
                    else -> throw RuntimeException()
                }
                distanceMap.putIfAbsent(currentCoord, distance + 1)
                prevMap.putIfAbsent(currentCoord, prevCoord)

//                pg.fillRect(
//                        SCALE * (min(prevCoord.x, currentCoord.x) * 2 + 1),
//                        SCALE * (min(prevCoord.y, currentCoord.y) * 2 + 1),
//                        SCALE * (abs(prevCoord.x - currentCoord.x) * 2 + 1),
//                        SCALE * (abs(prevCoord.y - currentCoord.y) * 2 + 1)
//                )
//                render(pathImage, image, g, stack, currentCoord, ++frame)
            }
        }
    }


    var prevCoords = List<Set<Coord>>(16) { setOf() }
    for (distance in 1..(distanceMap.values.maxOrNull() ?: 1) + 20) {
        var currentCoords = distanceMap.filterValues { it == distance }.keys
        currentCoords.forEach { coord ->
            val prevCoord = prevMap[coord]!!
            pg.fillRect(
                SCALE * (min(prevCoord.x, coord.x) * 2 + 1),
                SCALE * (min(prevCoord.y, coord.y) * 2 + 1),
                SCALE * (abs(prevCoord.x - coord.x) * 2 + 1),
                SCALE * (abs(prevCoord.y - coord.y) * 2 + 1)
            )

        }

        if (currentCoords.isEmpty()) {
            currentCoords = prevCoords.last()
        }

        if (distance > (distanceMap.values.maxOrNull() ?: 1) - 20) {
            println("rendering $distance")
            render(pathImage, image, g, currentCoords, prevCoords, prevMap, distance)
        }

        prevCoords = prevCoords.drop(1) + listOf(currentCoords)
    }
}


private fun render(
    pathImage: BufferedImage, image: BufferedImage, g: Graphics2D,
    redCoords: Set<Coord>, prevCoords: List<Set<Coord>>,
    prevMap: Map<Coord, Coord>, frame: Int = 0
) {
    g.drawImage(pathImage, 0, 0, null)


    prevCoords.forEachIndexed { index, coords ->
        val scale = 255 * index / prevCoords.size
        g.color = Color(255, 255 - scale, 255 - scale)

        coords.forEach { coord ->
            var currentCoord = coord
            while (true) {
                val prevCoord = prevMap[currentCoord] ?: break

                g.fillRect(
                    SCALE * (min(prevCoord.x, currentCoord.x) * 2 + 1),
                    SCALE * (min(prevCoord.y, currentCoord.y) * 2 + 1),
                    SCALE * (abs(prevCoord.x - currentCoord.x) * 2 + 1),
                    SCALE * (abs(prevCoord.y - currentCoord.y) * 2 + 1)
                )
                currentCoord = prevCoord
            }
        }
    }

    g.color = Color.RED
    redCoords.forEach { coord ->
        var currentCoord = coord
        while (true) {
            val prevCoord = prevMap[currentCoord] ?: break

            g.fillRect(
                SCALE * (min(prevCoord.x, currentCoord.x) * 2 + 1),
                SCALE * (min(prevCoord.y, currentCoord.y) * 2 + 1),
                SCALE * (abs(prevCoord.x - currentCoord.x) * 2 + 1),
                SCALE * (abs(prevCoord.y - currentCoord.y) * 2 + 1)
            )
            currentCoord = prevCoord
        }
    }

//    ImageIO.write(image, "gif", File("img/d20/f%07d.gif".format(frame)))
    ImageIO.write(image, "gif", File("C:/tmp/y/f%07d.gif".format(frame)))
}

private fun getBounds(input: String): Pair<Coord, Coord> {

    val distanceMap = mutableMapOf((0 to 0) to 0)

    val stack = ArrayDeque<Coord>()
    var currentCoord = 0 to 0
    input.toCharArray().forEach { c ->
        val distance = distanceMap[currentCoord] ?: throw RuntimeException()
        when (c) {
            '(' -> stack.push(currentCoord)
            '|' -> currentCoord = stack.peek()
            ')' -> currentCoord = stack.pop()
            '^', '$' -> Unit
            else -> {
                currentCoord = when (c) {
                    'N' -> currentCoord.x to currentCoord.y - 1
                    'S' -> currentCoord.x to currentCoord.y + 1
                    'W' -> currentCoord.x - 1 to currentCoord.y
                    'E' -> currentCoord.x + 1 to currentCoord.y
                    else -> throw RuntimeException()
                }
                distanceMap.putIfAbsent(currentCoord, distance + 1)
            }
        }
    }

    val coords = distanceMap.keys
    val minX = coords.minOf { it.x }
    val maxX = coords.maxOf { it.x }
    val minY = coords.minOf { it.y }
    val maxY = coords.maxOf { it.y }

    return (minX to minY) to (maxX to maxY)
}