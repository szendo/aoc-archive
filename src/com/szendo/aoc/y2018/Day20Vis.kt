package com.szendo.aoc.y2018

import com.szendo.aoc.*
import java.awt.Color
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO
import kotlin.math.abs
import kotlin.math.min

private const val SCALE = 4

fun main() {
    val input = Helper.getResourceAsStream("2018/20.txt")
        .bufferedReader()
        .useLines { it.first() }


    val (topLeft, bottomRight) = getBounds(input)
    val width = bottomRight.x - topLeft.x + 1
    val height = bottomRight.y - topLeft.y + 1

    val image = BufferedImage(SCALE * (2 * width + 1), SCALE * (2 * height + 1), BufferedImage.TYPE_INT_RGB)
    val g = image.createGraphics()

    val pathImage = BufferedImage(SCALE * (2 * width + 1), SCALE * (2 * height + 1), BufferedImage.TYPE_INT_RGB)
    val pg = pathImage.createGraphics()
    pg.color = Color.BLACK
    pg.fillRect(0, 0, image.width, image.height)
    pg.color = Color.WHITE

    val stack = ArrayDeque<Coord>()
    var currentCoord = (0 to 0) - topLeft
    val distanceMap = mutableMapOf(currentCoord to 0)

    g.drawImage(pathImage, 0, 0, null)
    render(pathImage, image, g, stack, currentCoord)

    var frame = 0
    input.toCharArray().forEach { c ->
        val distance = distanceMap[currentCoord] ?: throw RuntimeException()
        when (c) {
            '(' -> stack.push(currentCoord)
            '|', ')' -> {
                val prevCoord = currentCoord
                when (c) {
                    '|' -> currentCoord = stack.peek()
                    ')' -> currentCoord = stack.pop()
                }
                if (prevCoord != currentCoord) {
                    render(pathImage, image, g, stack, currentCoord, ++frame)
                }
            }
            '^', '$' -> Unit
            else -> {
                val prevCoord = currentCoord
                currentCoord = when (c) {
                    'N' -> currentCoord.x to currentCoord.y - 1
                    'S' -> currentCoord.x to currentCoord.y + 1
                    'W' -> currentCoord.x - 1 to currentCoord.y
                    'E' -> currentCoord.x + 1 to currentCoord.y
                    else -> throw RuntimeException()
                }
                distanceMap.putIfAbsent(currentCoord, distance + 1)

                pg.fillRect(
                    SCALE * (min(prevCoord.x, currentCoord.x) * 2 + 1),
                    SCALE * (min(prevCoord.y, currentCoord.y) * 2 + 1),
                    SCALE * (abs(prevCoord.x - currentCoord.x) * 2 + 1),
                    SCALE * (abs(prevCoord.y - currentCoord.y) * 2 + 1)
                )
                render(pathImage, image, g, stack, currentCoord, ++frame)
            }
        }
    }

    render(pathImage, image, g, ArrayDeque(), null, ++frame)
}

private fun render(
    pathImage: BufferedImage, image: BufferedImage, g: Graphics2D,
    stack: ArrayDeque<Coord>, currentCoord: Coord?, frame: Int = 0
) {
    g.drawImage(pathImage, 0, 0, null)

    g.color = Color.GREEN
    stack.forEach { coord -> g.fillRect(SCALE * (coord.x * 2 + 1), SCALE * (coord.y * 2 + 1), SCALE, SCALE) }

    g.color = Color.RED
    currentCoord?.let { coord -> g.fillRect(SCALE * (coord.x * 2 + 1), SCALE * (coord.y * 2 + 1), SCALE, SCALE) }

//    ImageIO.write(image, "gif", File("img/d20/f%07d.gif".format(frame)))
    ImageIO.write(image, "gif", File("C:/tmp/x/f%07d.gif".format(frame)))
}

private fun getBounds(input: String): Pair<Coord, Coord> {

    val distanceMap = mutableMapOf((0 to 0) to 0)

    val stack = ArrayDeque<Coord>()
    var currentCoord = 0 to 0
    input.toCharArray().forEach { c ->
        val distance = distanceMap[currentCoord] ?: throw RuntimeException()
        when (c) {
            '(' -> stack.push(currentCoord)
            '|' -> currentCoord = stack.peek()
            ')' -> currentCoord = stack.pop()
            '^', '$' -> Unit
            else -> {
                currentCoord = when (c) {
                    'N' -> currentCoord.x to currentCoord.y - 1
                    'S' -> currentCoord.x to currentCoord.y + 1
                    'W' -> currentCoord.x - 1 to currentCoord.y
                    'E' -> currentCoord.x + 1 to currentCoord.y
                    else -> throw RuntimeException()
                }
                distanceMap.putIfAbsent(currentCoord, distance + 1)
            }
        }
    }

    val coords = distanceMap.keys
    val minX = coords.minOf { it.x }
    val maxX = coords.maxOf { it.x }
    val minY = coords.minOf { it.y }
    val maxY = coords.maxOf { it.y }

    return (minX to minY) to (maxX to maxY)
}