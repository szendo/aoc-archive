package com.szendo.aoc.y2018

fun main() {
    val input = 8199
    val gridSize = 300

    val grid = (1..gridSize).map { x ->
        (1..gridSize).map { y ->
            val rackId = x + 10
            ((rackId * y + input).toLong() * rackId / 100 % 10 - 5).toInt()
        }
    }

    var maxX = 0
    var maxY = 0
    var max = 0

    for (x in 0..grid.lastIndex - 2) {
        for (y in 0..grid.lastIndex - 2) {
            val sum =
                grid[x][y] + grid[x][y + 1] + grid[x][y + 2] + grid[x + 1][y] + grid[x + 1][y + 1] + grid[x + 1][y + 2] + grid[x + 2][y] + grid[x + 2][y + 1] + grid[x + 2][y + 2]
            if (sum > max) {
                max = sum
                maxX = x + 1
                maxY = y + 1
            }
        }
    }
    val answer1 = "$maxX,$maxY"
    println("Answer 1: $answer1")

    maxX = 0
    maxY = 0
    var size = 0
    max = 0

    for (s in 1..300) {
        for (x in 0..grid.size - s) {
            for (y in 0..grid.size - s) {
                val sum = (0 until s).sumOf { dx -> (0 until s).sumOf { dy -> grid[x + dx][y + dy] } }
                if (sum > max) {
                    max = sum
                    maxX = x + 1
                    maxY = y + 1
                    size = s
                }
            }
        }
    }

    val answer2 = "$maxX,$maxY,$size"
    println("Answer 2: $answer2")
}