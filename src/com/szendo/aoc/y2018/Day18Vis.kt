package com.szendo.aoc.y2018

import com.szendo.aoc.Helper
import java.awt.Color
import java.awt.Font
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

fun main() {
    val input = Helper.getResourceAsStream("2018/18.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { it.toList() }.toList()
        }

    val image = BufferedImage(input[0].size * SCALE, input.size * SCALE, BufferedImage.TYPE_INT_RGB)

    val seenStates = mutableSetOf(input)
    var firstRepeatingState: List<List<Char>>? = null
    var firstRepeatAt = 0
    var cyclesToSkip = 0

    render(input, image, 0)

    var state = input
    var step = 0
    while (step < 1000000000) {
        step++
        state = state.mapIndexed { y, row ->
            row.mapIndexed { x, c ->
                when (c) {
                    '.' -> {
                        val t =
                            (-1..1).sumOf { dy -> (-1..1).count { dx -> state.getOrNull(y + dy)?.getOrNull(x + dx) == '|' } }
                        if (t >= 3) '|' else '.'
                    }

                    '|' -> {
                        val l =
                            (-1..1).sumOf { dy -> (-1..1).count { dx -> state.getOrNull(y + dy)?.getOrNull(x + dx) == '#' } }
                        if (l >= 3) '#' else '|'
                    }

                    '#' -> {
                        val l =
                            (-1..1).sumOf { dy -> (-1..1).count { dx -> state.getOrNull(y + dy)?.getOrNull(x + dx) == '#' } }
                        val t =
                            (-1..1).sumOf { dy -> (-1..1).count { dx -> state.getOrNull(y + dy)?.getOrNull(x + dx) == '|' } }
                        if (l >= 2 && t >= 1) '#' else '.'
                    }

                    else -> throw RuntimeException()
                }
            }
        }

        if (firstRepeatingState != null) {
            if (cyclesToSkip > 0) {
                render(state, image, step)

            } else {
                if (firstRepeatingState == state) {
                    val cycleLength = step - firstRepeatAt
                    cyclesToSkip = (1000000000 - step) / cycleLength
                    step += cyclesToSkip * cycleLength

                    render(state, image, step)
                } else {
                    render(state, image, step, cycle = true)
                }
            }

        } else if (!seenStates.add(state)) {
            render(state, image, step, cycle = true)

            firstRepeatingState = state
            firstRepeatAt = step
        } else {
            render(state, image, step)
        }
    }
}

private const val SCALE = 8

private fun render(state: List<List<Char>>, image: BufferedImage, step: Int, cycle: Boolean = false) {
    val g = image.createGraphics()
    state.forEachIndexed { y, row ->
        row.forEachIndexed { x, c ->
            val color = when (c) {
                '.' -> Color(0x8B4513) // saddle brown
                '|' -> Color(0x228B22) // forest green
                '#' -> Color(0xDEB887) // burlywood
                else -> throw RuntimeException()
            }
            g.color = color
            g.fillRect(SCALE * x, SCALE * y, SCALE, SCALE)
        }
    }

    when (step) {
        0 -> "Initial state"
        1 -> "After $step minute"
        10 -> {
            val value = state.sumOf { row -> row.count { it == '#' } } * state.sumOf { row -> row.count { it == '|' } }
            "Part 1: $value"
        }
        1000000000 -> {
            val value = state.sumOf { row -> row.count { it == '#' } } * state.sumOf { row -> row.count { it == '|' } }
            "Part 2: $value"
        }
        else -> if (cycle) {
            "Skipping ${28571414} cycles" // FIXME hardcoded value
        } else {
            "After $step minutes"
        }
    }.let { text ->
        g.font = Font("DejaVu Sans Mono", Font.PLAIN, 16)
        val fm = g.fontMetrics
        val textWidth = fm.stringWidth(text)
        g.color = Color(0, 0, 0, 0x66)
        g.fillRect(1, image.height - fm.ascent, textWidth, fm.ascent - 1)
        g.color = Color.WHITE
        g.drawString(text, 1, image.height - 2)
    }

    ImageIO.write(image, "gif", File("img/d18/f%010d.gif".format(step)))
}