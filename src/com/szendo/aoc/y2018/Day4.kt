package com.szendo.aoc.y2018

import com.szendo.aoc.Helper
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun main() {
    val regex = Regex("\\[([0-9 :-]+)] (Guard #(\\d+) begins shift|falls asleep|wakes up)")
    val dateTimeFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm")

    val input: List<String> = Helper.getResourceAsStream("2018/4.txt")
        .bufferedReader()
        .useLines { it.sorted().toList() }

    val sleepTimes = mutableMapOf<Int, Long>()
    val sleepTimesByMinute = mutableMapOf<Int, MutableMap<Int, Int>>()
    var currentGuardId = 0
    var sleepStartTime: LocalDateTime? = null
    for (line in input) {
        val entry = regex.matchEntire(line) ?: throw RuntimeException("Line does not match: $line")
        //val timestamp = LocalDateTime.parse(entry.groupValues[1].replaceFirst(' ', 'T') + ":00")
        val timestamp = LocalDateTime.parse(entry.groupValues[1], dateTimeFormatter)
        val guardId = entry.groupValues[3]
        if (guardId.isNotEmpty()) {
            if (sleepStartTime != null) {
                throw RuntimeException("Shift change when guard $currentGuardId is asleep: $line")
            }
            currentGuardId = guardId.toInt()
        } else {
            if (entry.groupValues[2] == "falls asleep") {
                if (sleepStartTime != null) {
                    throw RuntimeException("Guard is already asleep: $line")
                }
                sleepStartTime = timestamp
            } else if (entry.groupValues[2] == "wakes up") {
                if (sleepStartTime == null) {
                    throw RuntimeException("Guard is already awake: $line")
                }

                val sleepDuration = Duration.between(sleepStartTime, timestamp).toMinutes()
                val minuteMap = sleepTimesByMinute.getOrPut(currentGuardId, ::mutableMapOf)
                for (m in sleepStartTime.minute until timestamp.minute) {
                    minuteMap.merge(m, 1) { a, b -> a + b }
                }
                sleepTimes.merge(currentGuardId, sleepDuration) { a, b -> a + b }
                sleepStartTime = null
            }
        }
    }

    val answer1 = sleepTimes.maxByOrNull { it.value }!!.let { it.key * sleepTimesByMinute[it.key]!!.maxByOrNull { it.value }!!.key }
    println("Answer 1: $answer1")

    var answer2 = 0
    var maxFreq = 0
    for ((guardId, minuteMap) in sleepTimesByMinute) {
        for ((m, f) in minuteMap) {
            if (f > maxFreq) {
                maxFreq = f
                answer2 = guardId * m
            }
        }
    }

    println("Answer 2: $answer2")
}
