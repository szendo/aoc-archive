package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {

    val regex = Regex("Step (.) must be finished before step (.) can begin.")

    val input: List<Pair<String, String>> = Helper.getResourceAsStream("2018/7.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.mapNotNull { line -> regex.matchEntire(line)?.groupValues }
                .map { it[1] to it[2] }
                .toList()
        }

    visualize1(input)
}

private fun visualize1(input: List<Pair<String, String>>) {
    val stepsWithDeps = mutableMapOf<String, MutableSet<String>>()

    for ((dep, step) in input) {
        stepsWithDeps.getOrPut(dep, ::mutableSetOf)
        stepsWithDeps.getOrPut(step, ::mutableSetOf).add(dep)
    }

    println(stepsWithDeps.flatMap { (step, deps) -> deps.map { "\"$it\" -> \"$step\"" } }.sorted().joinToString("\n"))

}
