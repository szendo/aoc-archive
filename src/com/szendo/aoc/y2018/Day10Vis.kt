package com.szendo.aoc.y2018

import com.szendo.aoc.Helper
import com.szendo.aoc.minus
import com.szendo.aoc.plus
import java.awt.Color
import java.awt.Font
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

fun main() {
    val regex = Regex("-?\\d+")

    val input: List<Pair<Pair<Int, Int>, Pair<Int, Int>>> = Helper.getResourceAsStream("2018/10.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { line ->
                regex.findAll(line).toList().let {
                    (it[0].value.toInt() to it[1].value.toInt()) to (it[2].value.toInt() to it[3].value.toInt())
                }
            }.toList()
        }

    val (pos, vel) = input.unzip().let { (a, b) -> a.toMutableList() to b }

    var minHeight = (pos.maxOf { it.second } - pos.minOf { it.second })
    var timer = 0

    while (true) {
        for (i in 0 until pos.size) {
            pos[i] += vel[i]
        }
        timer++

        val height = (pos.maxOf { it.second } - pos.minOf { it.second })

        if (height < minHeight) {
            minHeight = height
        } else {
            for (i in 0 until pos.size) {
                pos[i] -= vel[i]
            }
            timer--
            break
        }
    }

    val (w, h) = 320 to 240
    val image = BufferedImage(w, h, BufferedImage.TYPE_INT_RGB)
    val g = image.createGraphics()
    g.font = Font("DejaVu Sans Mono", Font.PLAIN, 12)

    val loX = pos.minOf { it.first }
    val loY = pos.minOf { it.second }
    val hiX = pos.maxOf { it.first }
    val hiY = pos.maxOf { it.second }

    val xOff = (w - (hiX - loX + 1)) / 2
    val yOff = (h - (hiY - loY + 1)) / 2

    while (true) {
        for (i in 0 until pos.size) {
            pos[i] -= vel[i]
        }
        timer--

        if ((loY - yOff..hiY + yOff).none { y -> (loX - xOff..hiX + xOff).any { x -> x to y in pos } }) {
            break
        }
    }

    while (true) {
        for (y in loY - yOff..hiY + yOff) {
            for (x in loX - xOff..hiX + xOff) {
                image.setRGB(
                    x - loX + xOff, y - loY + yOff, when {
                        x to y in pos -> 0xffff00
                        else -> 0x000033
                    }
                )
            }
        }

        g.color = Color.CYAN
        g.drawString("T=$timer", 2, h - 2)
        ImageIO.write(image, "gif", File("img/d10/f%05d.gif".format(timer)))

        for (i in 0 until pos.size) {
            pos[i] += vel[i]
        }
        timer++

        if ((loY - yOff..hiY + yOff).none { y -> (loX - xOff..hiX + xOff).any { x -> x to y in pos } }) {
            break
        }
    }
}
