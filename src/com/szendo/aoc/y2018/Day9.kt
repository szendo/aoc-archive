package com.szendo.aoc.y2018

fun main() {
    class Dll {
        val value: Int
        var next: Dll
        var prev: Dll

        constructor(value: Int) {
            this.value = value
            this.next = this
            this.prev = this
        }

        constructor(value: Int, next: Dll, prev: Dll) {
            this.value = value
            this.next = next
            this.prev = prev
        }
    }

    val players = 468
    val lastMarble = 71843


    val scores = MutableList(players) { 0L }
    var currentMarble = Dll(0)

    for (m in 1..(lastMarble * 100)) {
        if (m % 23 == 0) {
            val marbleToRemove = currentMarble.prev.prev.prev.prev.prev.prev.prev
            scores[m % scores.size] += m + marbleToRemove.value.toLong()
            marbleToRemove.prev.next = marbleToRemove.next
            marbleToRemove.next.prev = marbleToRemove.prev
            currentMarble = marbleToRemove.next

        } else {
            val insertPos = currentMarble.next
            val newMarble = Dll(m, insertPos.next, insertPos)
            insertPos.next.prev = newMarble
            insertPos.next = newMarble
            currentMarble = newMarble
        }

        if (m == lastMarble) {
            println("Answer 1: " + scores.maxOf { it })
        }
    }

    println("Answer 2: " + scores.maxOf { it })
}

