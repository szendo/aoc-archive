package com.szendo.aoc.y2018

import com.szendo.aoc.Coord4D
import com.szendo.aoc.Helper
import com.szendo.aoc.distance

fun main() {
    val regex = Regex("-?\\d+")

    val input = Helper.getResourceAsStream("2018/25.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { line ->
                val (x, y, z, m) = regex.findAll(line).map { it.value.toInt() }.toList()
                Coord4D(x, y, z, m)
            }.toList()
        }

    val constellations = mutableSetOf<Set<Coord4D>>()

    input.forEach { point ->
        val newConst = mutableSetOf(point)

        val constsToRemove = constellations.filter { constellation ->
            constellation.any { cp -> cp.distance(point) <= 3 }
        }

        println("$point joins: $constsToRemove")

        constsToRemove.forEach { constellation ->
            constellations.remove(constellation)
            newConst.addAll(constellation)
        }

        constellations.add(newConst.toSet())
    }


    println(constellations.size)
}