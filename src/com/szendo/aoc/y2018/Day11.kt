package com.szendo.aoc.y2018

fun main() {
    val input = 8199

    val gridSize = 300
    val grid = (1..gridSize).map { x ->
        (1..gridSize).map { y ->
            val rackId = x + 10
            (rackId * y + input) * rackId / 100 % 10 - 5
        }
    }

    var maxX = 0
    var maxY = 0
    var maxSize = 3
    var maxSum = 0

    for (x in 0..grid.lastIndex - 2) {
        for (y in 0..grid.lastIndex - 2) {
            //val sum = grid[x][y] + grid[x][y + 1] + grid[x][y + 2] + grid[x + 1][y] + grid[x + 1][y + 1] + grid[x + 1][y + 2] + grid[x + 2][y] + grid[x + 2][y + 1] + grid[x + 2][y + 2]
            val sum = (0..2).sumOf { dx -> (0..2).sumOf { dy -> grid[x + dx][y + dy] } }
            if (sum > maxSum) {
                maxSum = sum
                maxX = x + 1
                maxY = y + 1
            }
        }
    }
    val answer1 = "$maxX,$maxY"
    println("Answer 1: $answer1")
    println("  (total power: $maxSum)")

    for (x in 0..grid.lastIndex) {
        for (y in 0..grid.lastIndex) {
            var sum = 0
            for (s in 1 until gridSize - maxOf(x, y)) {
                sum += (0 until s).sumOf { dx -> grid[x + dx][y + s - 1] }
                sum += (0 until s - 1).sumOf { dy -> grid[x + s - 1][y + dy] }
                if (sum > maxSum) {
                    maxSum = sum
                    maxX = x + 1
                    maxY = y + 1
                    maxSize = s
                }
            }
        }
    }

    val answer2 = "$maxX,$maxY,$maxSize"
    println("Answer 2: $answer2")
    println("  (total power: $maxSum)")
}