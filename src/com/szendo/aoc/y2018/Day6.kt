package com.szendo.aoc.y2018

import com.szendo.aoc.Helper
import com.szendo.aoc.distance

fun main() {
    val input: List<Pair<Int, Int>> = Helper.getResourceAsStream("2018/6.txt")
        .bufferedReader()
        .useLines {
            it.mapNotNull { line -> line.split(", ") }
                .map { it[0].toInt() to it[1].toInt() }
                .toList()
        }

    val minX = input.minOf { it.first } - 250
    val minY = input.minOf { it.second } - 250
    val maxX = input.maxOf { it.first } + 250
    val maxY = input.maxOf { it.second } + 250

    val finiteIndices = input.withIndex().map { (index, value) ->
        val (x, y) = value

        val nwf = input.any { it.first < x && it.second < y }
        val nef = input.any { it.first > x && it.second < y }
        val swf = input.any { it.first < x && it.second > y }
        val sef = input.any { it.first > x && it.second > y }

        val nf = input.any { it.first == x && it.second < y }
        val wf = input.any { it.first < x && it.second == y }
        val ef = input.any { it.first > x && it.second == y }
        val sf = input.any { it.first == x && it.second > y }

        val finite = (nwf || nf && wf || nf && swf || nef && wf)
                && (nf || nef && nwf || nef && wf || ef && nwf)
                && (nef || nf && ef || nf && sef || nwf && ef)
                && (ef || nef && sef || nef && sf || nf && sef)
                && (sef || sf && ef || sf && nef || swf && ef)
                && (sf || sef && swf || sef && sf || sf && swf)
                && (swf || sf && wf || sf && nwf || sef && wf)
                && (wf || swf && nwf || swf && nf || sf && nwf)

        return@map index to finite
    }
        .filter { it.second }
        .map { it.first }

    val areas1 = mutableMapOf<Int, Int>() // index -> distance
    val areas2 = mutableMapOf<Int, Int>() // index -> distance

    for (x in (minX - 250)..(maxX + 250)) {
        for (y in (minY - 250)..(maxY + 250)) {
            var minDistance = Int.MAX_VALUE
            var closest = -1
            var tie = false
            input.forEachIndexed { index, p ->
                val distance = p.distance(x to y)
                if (distance < minDistance) {
                    minDistance = distance
                    closest = index
                    tie = false
                } else if (distance == minDistance) {
                    closest = -1
                    tie = true
                }
            }
            if (!tie) {
                areas1.merge(closest, 1, Int::plus)
                if (x in minX..maxX && y in minY..maxY) {
                    areas2.merge(closest, 1, Int::plus)
                }
            }
        }
    }

    val a1 = areas1.filterKeys { areas1[it] == areas2[it] }.maxByOrNull { areas1[it.key]!! }
    println(a1)
    val answer1 = areas1.keys.filter { areas1[it] == areas2[it] }.mapNotNull { areas1[it] }.maxOf { it }
    println("Answer 1: $answer1")

    var answer2 = 0
    for (x in (minX - 1000)..(maxX + 1000)) {
        for (y in (minY - 1000)..(maxY + 1000)) {
            if (input.sumOf { it.distance(x to y) } < 10_000) {
                answer2++
            }
        }
    }
    println("Answer 2: $answer2")
}
