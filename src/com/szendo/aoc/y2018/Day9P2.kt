package com.szendo.aoc.y2018

import java.util.*

fun main() {
    val players = 468
    val lastMarble = 71843 * 100


    val scores = Vector<Long>()
    repeat(players) {
        scores.add(0)
    }
    var currentMarble = Aoc9Ll(0)

    var lastTime = System.currentTimeMillis()
    for (m in 1..lastMarble) {
        if (m % 500_000 == 0) {
            val time = System.currentTimeMillis()

            println("Time: ${(time - lastTime) / 1000} s (m = $m)")
            lastTime = time


        }


        if (m % 23 == 0) {
            val marbleToRemove = currentMarble.prev.prev.prev.prev.prev.prev.prev
            scores[m % scores.size] += m + marbleToRemove.value.toLong()
            marbleToRemove.prev.next = marbleToRemove.next
            marbleToRemove.next.prev = marbleToRemove.prev
            currentMarble = marbleToRemove.next

        } else {
            val insertPos = currentMarble.next
            val newMarble = Aoc9Ll(m, insertPos.next, insertPos)
            insertPos.next.prev = newMarble
            insertPos.next = newMarble
            currentMarble = newMarble
        }
    }

    println("Answer 2: " + scores.maxOf { it })
}

class Aoc9Ll {
    val value: Int
    var next: Aoc9Ll
    var prev: Aoc9Ll


    constructor(value: Int) {
        this.value = value
        this.next = this
        this.prev = this
    }

    constructor(value: Int, next: Aoc9Ll, prev: Aoc9Ll) {
        this.value = value
        this.next = next
        this.prev = prev
    }


}