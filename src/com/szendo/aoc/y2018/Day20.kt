package com.szendo.aoc.y2018

import com.szendo.aoc.Coord
import com.szendo.aoc.Helper
import com.szendo.aoc.x
import com.szendo.aoc.y
import java.util.*

fun main() {
    val input = Helper.getResourceAsStream("2018/20.txt")
        .bufferedReader()
        .useLines { it.first() }

    var currentCoord = 0 to 0
    val stack = ArrayDeque<Coord>()
    val distanceMap = mutableMapOf(currentCoord to 0)
    input.toCharArray().forEach { c ->
        val distance = distanceMap[currentCoord] ?: throw RuntimeException()
        when (c) {
            '(' -> stack.push(currentCoord)
            '|' -> currentCoord = stack.peek()
            ')' -> currentCoord = stack.pop()
            '^', '$' -> Unit
            else -> {
                currentCoord = when (c) {
                    'N' -> currentCoord.x to currentCoord.y - 1
                    'S' -> currentCoord.x to currentCoord.y + 1
                    'W' -> currentCoord.x - 1 to currentCoord.y
                    'E' -> currentCoord.x + 1 to currentCoord.y
                    else -> throw RuntimeException()
                }
                distanceMap.putIfAbsent(currentCoord, distance + 1)

            }
        }
    }

    val answer1 = distanceMap.values.maxOf { it }
    println("Answer 1: $answer1")

    val answer2 = distanceMap.values.count { it >= 1000 }
    println("Answer 2: $answer2")
}