package com.szendo.aoc.y2018

import com.szendo.aoc.Helper
import com.szendo.aoc.minus
import com.szendo.aoc.plus

fun main() {
    val regex = Regex("-?\\d+")

    val input: List<Pair<Pair<Int, Int>, Pair<Int, Int>>> = Helper.getResourceAsStream("2018/10.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { line ->
                regex.findAll(line).toList().let {
                    (it[0].value.toInt() to it[1].value.toInt()) to (it[2].value.toInt() to it[3].value.toInt())
                }
            }.toList()
        }

    val (pos, vel) = input.unzip().let { (a, b) -> a.toMutableList() to b }

    var minHeight = (pos.maxOf { it.second } - pos.minOf { it.second })

    var timer = 0

    while (true) {
        for (i in 0 until pos.size) {
            pos[i] += vel[i]
        }
        timer++

        val height = (pos.maxOf { it.second } - pos.minOf { it.second })

        if (height < minHeight) {
            minHeight = height
        } else {
            for (i in 0 until pos.size) {
                pos[i] -= vel[i]
            }
            timer--
            break
        }
    }

    val loX = pos.minOf { it.first }
    val loY = pos.minOf { it.second }
    val hiX = pos.maxOf { it.first }
    val hiY = pos.maxOf { it.second }

    println("Answer 1:")
    for (y in loY..hiY) {
        for (x in loX..hiX) {
            print(
                when {
                    x to y in pos -> "#"
                    else -> " "
                }
            )
        }
        println()
    }
    println()
    println("Answer 2: $timer")
}
