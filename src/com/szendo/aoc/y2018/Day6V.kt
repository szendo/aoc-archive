package com.szendo.aoc.y2018

import com.szendo.aoc.Helper
import com.szendo.aoc.distance
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

fun main() {

    val colors = listOf(
        0xF06292,
        0xBA68C8,
        0x9575CD,
        0x7986CB,
        0x64B5F6,
        0x4FC3F7,
        0x4DD0E1,
        0x4DB6AC,
        0x81C784,
        0xAED581,
        0xDCE775,
        0xFFF176,
        0xFFD54F,
        0xFFB74D,
        0xFF8A65,
        0xA1887F
    )

    val input: List<Pair<Int, Int>> = Helper.getResourceAsStream("2018/6.txt")
        .bufferedReader()
        .useLines {
            it.mapNotNull { line -> line.split(", ") }
                .map { it[0].toInt() to it[1].toInt() }
                .toList()
        }


    val minX = input.minOf { it.first } - 250
    val minY = input.minOf { it.second } - 250
    val maxX = input.maxOf { it.first } + 250
    val maxY = input.maxOf { it.second } + 250

    val areas1 = mutableMapOf<Int, Int>() // index -> distance
    val areas2 = mutableMapOf<Int, Int>() // index -> distance

    val image1 = BufferedImage(maxX - minX + 1, maxY - minY + 1, BufferedImage.TYPE_INT_RGB)
    val image2 = BufferedImage(maxX - minX + 1, maxY - minY + 1, BufferedImage.TYPE_INT_RGB)

    for (x in (minX - 250)..(maxX + 250)) {
        for (y in (minY - 250)..(maxY + 250)) {
            var minDistance = Int.MAX_VALUE
            var closest = -1
            var tie = false
            input.forEachIndexed { index, p ->
                val distance = p.distance(x to y)
                if (distance < minDistance) {
                    minDistance = distance
                    closest = index
                    tie = false
                } else if (distance == minDistance) {
                    closest = -1
                    tie = true
                }
            }
            if (!tie) {
                areas1.merge(closest, 1, Int::plus)
                if (x in minX..maxX && y in minY..maxY) {
                    areas2.merge(closest, 1, Int::plus)
                    image1.setRGB(x - minX, y - minY, if (minDistance == 0) 0xff0000 else colors[closest % colors.size])
                }
            } else {
                if (x in minX..maxX && y in minY..maxY) {
                    image1.setRGB(x - minX, y - minY, 0x000000)
                }
            }
        }
    }


    val a1k = areas1.filterKeys { areas1[it] == areas2[it] }.maxByOrNull { it.value }!!.key
    val g = image1.createGraphics()
    g.color = Color.RED
    g.font = g.font.deriveFont(10.0f)
    input.forEachIndexed { index, c ->
        if (index != a1k) return@forEachIndexed
        if (areas1[index] == areas2[index]) {
            val a = areas1[index]?.toString()
            val w = g.fontMetrics.stringWidth(a)
            g.drawString(a, c.first - minX - w / 2, c.second - minY + g.fontMetrics.maxAscent)
        }
    }

    ImageIO.write(image1, "png", File("out.png"))

    for (x in minX..maxX) {
        for (y in minY..maxY) {
            image2.setRGB(x - minX, y - minY, if (input.sumOf { it.distance(x to y) } < 10_000) 0xFFFFFF else 0xCCCCCC)
        }
    }
    ImageIO.write(image2, "png", File("out2.png"))
}
