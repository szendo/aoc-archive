package com.szendo.aoc.y2018

import com.szendo.aoc.Helper
import com.szendo.aoc.y2018.day16.Op
import com.szendo.aoc.y2018.day16.eval

fun main() {
    val input = Helper.getResourceAsStream("2018/21.txt")
        .bufferedReader()
        .useLines(Sequence<String>::toList)
    val seed = input.drop(8).first().split(" ")[1].toInt()

    val (answer1, answer2) = solve(seed)
    simulate(input, answer1)
    println("Answer 1: $answer1")
    // simulate(input, answer2)
    println("Answer 2: $answer2")
}

private fun solve(seed: Int = 8858047): Pair<Int, Int> {
    var r4: Int
    var r5: Int

    val allR5s = mutableSetOf<Int>()
    var answer1 = 0
    var answer2 = 0

    r5 = 0
    while (true) {
        r4 = r5 or 0x10000
        r5 = seed
        while (true) {
            r5 = (r5 + (r4 and 255) and 16777215) * 65899 and 16777215
            if (256 > r4) {
                break
            }
            r4 /= 256
        }

        if (r5 in allR5s) {
            break
        } else {
            if (allR5s.isEmpty()) {
                answer1 = r5
            }
            answer2 = r5
            allR5s.add(r5)
        }
    }

    return answer1 to answer2
}

private val ops = mapOf<String, Op>(
    "addr" to { i1, i2, r -> r[i1] + r[i2] },
    "addi" to { i1, i2, r -> r[i1] + i2 },
    "mulr" to { i1, i2, r -> r[i1] * r[i2] },
    "muli" to { i1, i2, r -> r[i1] * i2 },
    "banr" to { i1, i2, r -> r[i1] and r[i2] },
    "bani" to { i1, i2, r -> r[i1] and i2 },
    "borr" to { i1, i2, r -> r[i1] or r[i2] },
    "bori" to { i1, i2, r -> r[i1] or i2 },
    "setr" to { i1, _, r -> r[i1] },
    "seti" to { i1, _, _ -> i1 },
    "gtir" to { i1, i2, r -> if (i1 > r[i2]) 1 else 0 },
    "gtri" to { i1, i2, r -> if (r[i1] > i2) 1 else 0 },
    "gtrr" to { i1, i2, r -> if (r[i1] > r[i2]) 1 else 0 },
    "eqir" to { i1, i2, r -> if (i1 == r[i2]) 1 else 0 },
    "eqri" to { i1, i2, r -> if (r[i1] == i2) 1 else 0 },
    "eqrr" to { i1, i2, r -> if (r[i1] == r[i2]) 1 else 0 }
)

private fun simulate(input: List<String>, r0: Int) {
    var registers = List(6) { if (it == 0) r0 else 0 }
    val instructions = input.drop(1)
    val ip = input.first().split(' ')[1].toInt()

    while (true) {
        val ipx = registers[ip]
        val (opcode, a, b, c) = instructions.getOrNull(ipx)?.split(' ') ?: break
        registers = eval(a.toInt(), b.toInt(), c.toInt(), registers, ops[opcode]!!)
        registers = registers.mapIndexed { index, i -> if (index == ip) (i + 1) else i }
    }
}
