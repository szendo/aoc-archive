package com.szendo.aoc.y2018.day15

import com.szendo.aoc.Coord
import com.szendo.aoc.Helper
import com.szendo.aoc.x
import com.szendo.aoc.y

fun main() {
    val input = Helper.getResourceAsStream("2018/15.txt")
        .bufferedReader()
        .useLines { it.toList() }

    val answer1 = simulate(input).first
    println("Answer 1: $answer1")

    var power = 4
    while (true) {
        val (outcome, win) = simulate(input, power++)

        if (win) {
            println("Answer 2: $outcome")
            break
        }
    }
}

fun simulate(input: List<String>, elfPow: Int = 3): Pair<Int, Boolean> {
    var allElvesAlive = true

    val creatures = mutableListOf<Creature>()

    val walls = mutableSetOf<Coord>()

    for ((y, row) in input.withIndex()) {
        for ((x, cell) in row.withIndex()) {
            when (cell) {
                'G' -> creatures.add(Creature(Type.G, 200, x to y))
                'E' -> creatures.add(Creature(Type.E, 200, x to y))
                '#' -> walls.add(x to y)
            }
        }
    }

    var fullRounds = 0
    while (true) {
        var fullRound = false


        creatures.withIndex()
            .sortedWith(compareBy({ it.value.coord.y }, { it.value.coord.x }))
            .forEach { (index, creature) ->
                if (creature.hp <= 0) {
                    return@forEach
                }

                val target = getTarget(index, creatures, walls)
                if (target == null) {
                    if (creatures.none { it.hp > 0 && it.type != creature.type }) {
                        fullRound = false
                    }
                    return@forEach
                }

                fullRound = true

                val targetCreature = creatures.firstOrNull { it.coord == target }
                if (targetCreature != null) {
                    targetCreature.let {
                        if (it.type == creature.type) throw RuntimeException("Attacking ally")
                        it.hp -= if (creature.type == Type.G) 3 else elfPow
                        if (it.hp <= 0) {
                            if (it.type == Type.E) allElvesAlive = false
                            it.coord = -1 to -1
                        }
                    }
                } else {
                    creatures[index].coord = target

                    val (x, y) = target
                    var targetCreature = creatures
                        .filter {
                            it.coord in listOf(
                                x to (y - 1),
                                x - 1 to y,
                                x + 1 to y,
                                x to (y + 1)
                            ) && it.type != creature.type && it.hp > 0
                        }
                        .sortedWith(compareBy({ it.hp }, { it.coord.y }, { it.coord.x }))
                        .firstOrNull()
                    if (targetCreature != null) {
                        targetCreature.let {
                            if (it.type == creature.type) throw RuntimeException("Attacking ally")
                            it.hp -= if (creature.type == Type.G) 3 else elfPow
                            if (it.hp <= 0) {
                                if (it.type == Type.E) allElvesAlive = false
                                it.coord = -1 to -1
                            }
                        }
                    }

                }
            }

        if (fullRound) {
            fullRounds++
        } else {
            break
        }
    }

    val totalHpLeft = creatures.filter { it.hp > 0 }.sumOf { it.hp }

    return fullRounds * totalHpLeft to allElvesAlive
}

enum class Type { G, E }

data class Creature(val type: Type, var hp: Int, var coord: Coord)

fun getTarget(index: Int, creatures: List<Creature>, walls: Set<Coord>): Coord? {
    val type = creatures[index].type
    val (x, y) = creatures[index].coord

    var targetCreature = creatures
        .filter {
            it.coord in listOf(x to (y - 1), x - 1 to y, x + 1 to y, x to (y + 1)) && it.type != type && it.hp > 0
        }
        .sortedWith(compareBy({ it.hp }, { it.coord.y }, { it.coord.x }))
        .firstOrNull()
    if (targetCreature != null) {
        return targetCreature.coord
    }

    val targetCells = creatures.filter { it.type != type }
        .flatMap {
            listOf(
                it.coord.x to it.coord.y - 1,
                it.coord.x - 1 to it.coord.y,
                it.coord.x + 1 to it.coord.y,
                it.coord.x to it.coord.y + 1
            )
        }
        .filter { !walls.contains(it) }
        .filter { creatures.none { c -> c.coord == it } }
        .toSet()

    return nextStep(index, creatures, walls, targetCells)
}

fun nextStep(index: Int, creatures: List<Creature>, walls: Set<Coord>, targets: Iterable<Coord>): Coord? {
    val (x, y) = creatures[index].coord

    val done = mutableSetOf<Coord>()
    val queue = mutableListOf<Pair<Coord, List<Coord>>>((x to y) to emptyList())

    while (queue.isNotEmpty()) {
        val newQueue = mutableListOf<Pair<Coord, List<Coord>>>()

        while (queue.isNotEmpty()) {
            val (cell, path) = queue.removeAt(0)
            done.add(cell)

            val newPath = path + listOf(cell)

            val newCells = listOf(
                (cell.x to cell.y - 1) to newPath,
                (cell.x - 1 to cell.y) to newPath,
                (cell.x + 1 to cell.y) to newPath,
                (cell.x to cell.y + 1) to newPath
            )
                .filter { !done.contains(it.first) }
                .filter { queue.none { x -> x.first == (it.first) } }
                .filter { newQueue.none { x -> x.first == (it.first) } }
                .filter { !walls.contains(it.first) }
                .filter { creatures.none { c -> c.coord == it.first } }

            val tc = newCells.firstOrNull { it.first in targets }
            if (tc != null) {
                return if (tc.second.size < 2) tc.first else tc.second[1]
            }

            newQueue.addAll(newCells)
        }
        queue.addAll(newQueue)
        queue.sortWith(compareBy({ (_, path) -> path.size }, { (cell, _) -> cell.y }, { (cell, _) -> cell.x }))
    }

    return null
}