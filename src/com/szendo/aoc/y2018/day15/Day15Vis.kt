package com.szendo.aoc.y2018.day15

import com.szendo.aoc.Helper
import java.awt.Color
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

fun main() {
    val input = Helper.getResourceAsStream("2018/15.txt")
        .bufferedReader()
        .useLines { it.toList() }


    val answer1 = simulateV(input, 3).first
    println("Answer 1: $answer1")

}

fun simulateV(input: List<String>, elfPow: Int = 3): Pair<Int, Boolean> {
    var elfWin = true

    val creatures = mutableListOf<Creature>()

    val walls = mutableSetOf<Pair<Int, Int>>()

    for ((y, row) in input.withIndex()) {
        for ((x, cell) in row.withIndex()) {
            when (cell) {
                'G' -> {
                    creatures.add(Creature(Type.G, 200, x to y))
                }
                'E' -> {
                    creatures.add(Creature(Type.E, 200, x to y))
                }
                '#' -> {
                    walls.add(x to y)
                }
            }
        }
    }

    val image = BufferedImage(32 * 16, 32 * 16, BufferedImage.TYPE_INT_RGB)
    val g = image.createGraphics()
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
    g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF)
    g.font = g.font.deriveFont(9f)

    render(g, creatures, walls)
    ImageIO.write(image, "gif", File("img/d15/f%07d.gif".format(0)))

    var fullRounds = 0
    while (true) {
        var fullRound = false

        creatures.withIndex()
            .sortedBy { 32 * it.value.coord.second + it.value.coord.first }
            .forEach { (index, creature) ->
                if (creature.hp <= 0) {
                    return@forEach
                }

                val target = getTarget(index, creatures, walls)
                if (target == null) {
                    if (creatures.none { it.hp > 0 && it.type != creature.type }) {
                        fullRound = false
                    }
                    return@forEach
                }

                fullRound = true

                val targetCreature = creatures.firstOrNull { it.coord == target }
                if (targetCreature != null) {
                    targetCreature.let {
                        if (it.type == creature.type) throw RuntimeException("Attacking ally")
                        it.hp -= if (creature.type == Type.G) 3 else elfPow
                        if (it.hp <= 0) {
                            if (it.type == Type.E) elfWin = false
                            it.coord = -1 to -1
                        }
                    }
                } else {
                    creatures[index].coord = target

                    val (x, y) = target
                    var targetCreature = creatures
                        .filter {
                            it.coord in listOf(
                                x to (y - 1),
                                x - 1 to y,
                                x + 1 to y,
                                x to (y + 1)
                            ) && it.type != creature.type && it.hp > 0
                        }
                        .sortedBy { 10000 * it.hp + 32 * it.coord.second + it.coord.first }
                        .firstOrNull()
                    if (targetCreature != null) {
                        targetCreature.let {
                            if (it.type == creature.type) throw RuntimeException("Attacking ally")
                            it.hp -= if (creature.type == Type.G) 3 else elfPow
                            if (it.hp <= 0) {
                                if (it.type == Type.E) elfWin = false
                                it.coord = -1 to -1
                            }
                        }
                    }

                }
            }

        if (fullRound) {
            fullRounds++

            render(g, creatures, walls)
            ImageIO.write(image, "gif", File("img/d15/f%07d.gif".format(fullRounds)))
        } else {
            render(g, creatures, walls)
            ImageIO.write(image, "gif", File("img/d15/f%07d.gif".format(fullRounds + 1)))
            break
        }
    }

    val totalHpLeft = creatures.filter { it.hp > 0 }.sumOf { it.hp }

    return fullRounds * totalHpLeft to elfWin
}

private fun render(g: Graphics2D, creatures: MutableList<Creature>, walls: MutableSet<Pair<Int, Int>>) {
    val r = 6
    (0..32).forEach { x ->
        (0..32).forEach { y ->
            if (walls.contains(x to y)) {
                g.color = Color.DARK_GRAY
                g.fillRect(16 * x, 16 * y, 16, 16)
                g.color = Color.GRAY
                g.fillRoundRect(16 * x, 16 * y, 16, 16, 2 * r, 2 * r)

                val topLeft = walls.contains(x - 1 to y - 1) || x == 0 && y == 0
                val top = walls.contains(x to y - 1)
                val topRight = walls.contains(x + 1 to y - 1) || x == 31 && y == 0
                val left = walls.contains(x - 1 to y)
                val right = walls.contains(x + 1 to y)
                val bottomLeft = walls.contains(x - 1 to y + 1) || x == 0 && y == 31
                val bottom = walls.contains(x to y + 1)
                val bottomRight = walls.contains(x + 1 to y + 1) || x == 31 && y == 31
                if (top || left || topLeft) g.fillRect(16 * x, 16 * y, r, r)
                if (top || right || topRight) g.fillRect(16 * x + 16 - r, 16 * y, r, r)
                if (bottom || left || bottomLeft) g.fillRect(16 * x, 16 * y + 16 - r, r, r)
                if (bottom || right || bottomRight) g.fillRect(16 * x + 16 - r, 16 * y + 16 - r, r, r)
            } else {
                g.color = Color.GRAY
                g.fillRect(16 * x, 16 * y, 16, 16)
                g.color = Color.DARK_GRAY
                g.fillRoundRect(16 * x, 16 * y, 16, 16, 2 * r, 2 * r)

                val top = !walls.contains(x to y - 1)
                val left = !walls.contains(x - 1 to y)
                val right = !walls.contains(x + 1 to y)
                val bottom = !walls.contains(x to y + 1)
                if (top || left) g.fillRect(16 * x, 16 * y, r, r)
                if (top || right) g.fillRect(16 * x + 16 - r, 16 * y, r, r)
                if (bottom || left) g.fillRect(16 * x, 16 * y + 16 - r, r, r)
                if (bottom || right) g.fillRect(16 * x + 16 - r, 16 * y + 16 - r, r, r)
            }
            creatures.firstOrNull { it.coord == x to y }?.let {
                g.color = when (it.type) {
                    Type.E -> Color.RED
                    Type.G -> Color.GREEN.darker()
                }
                g.fillOval(16 * x + 2, 16 * y + 4, 12, 12)

                g.color = Color.BLACK
                g.fillRect(16 * x, 16 * y, 16, 6)

                val hpWidth = 14 * it.hp / 200
                g.color = when (it.hp) {
                    in (1..50) -> Color.RED
                    in (51..100) -> Color.ORANGE
                    in (101..150) -> Color.GREEN.darker()
                    in (151..200) -> Color.GREEN
                    else -> Color.BLACK
                }
                g.fillRect(16 * x + 1, 16 * y + 1, hpWidth, 4)

//                g.color = Color.WHITE
//                val xoff = 8 - min(14, g.fontMetrics.stringWidth(it.hp.toString())) / 2
//                g.drawString(it.hp.toString(), 16 * x + xoff, 16 * y + 14)

            }
        }
    }
}
