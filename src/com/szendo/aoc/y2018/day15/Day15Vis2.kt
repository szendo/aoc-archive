package com.szendo.aoc.y2018.day15

import com.szendo.aoc.Helper
import java.awt.BasicStroke
import java.awt.Color
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.min

fun main() {
    val input = Helper.getResourceAsStream("2018/15.txt")
        .bufferedReader()
        .useLines { it.toList() }

    simulateV2(input, 20)
}

fun simulateV2(input: List<String>, elfPow: Int = 3): Unit {
    val creatures = mutableListOf<Creature>()

    val walls = mutableSetOf<Pair<Int, Int>>()

    for ((y, row) in input.withIndex()) {
        for ((x, cell) in row.withIndex()) {
            when (cell) {
                'G' -> creatures.add(Creature(Type.G, 200, x to y))
                'E' -> creatures.add(Creature(Type.E, 200, x to y))
                '#' -> walls.add(x to y)
            }
        }
    }

    val image = BufferedImage(32 * 16, 32 * 16, BufferedImage.TYPE_INT_RGB)
    val g = image.createGraphics()
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
    g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF)
    g.font = g.font.deriveFont(9f)

    render2(g, creatures, walls, null, null)
    ImageIO.write(image, "gif", File("img/d15/f%03d_%03d.gif".format(0, 0)))

    var fullRounds = 0
    while (true) {
        var step = 0
        var fullRound = false

        creatures.withIndex()
            .sortedBy { 32 * it.value.coord.second + it.value.coord.first }
            .forEach { (index, creature) ->
                if (creature.hp <= 0) {
                    return@forEach
                }

                val target = getTarget2(index, creatures, walls)
                if (target == null) {
                    if (creatures.none { it.hp > 0 && it.type != creature.type }) {
                        fullRound = false
                    }
                    return@forEach
                }

                fullRound = true

                var moved = false
                var targetCreature = creatures.firstOrNull { it.coord == target[1] }
                if (targetCreature != null) {
                    targetCreature.let {
                        if (it.type == creature.type) throw RuntimeException("Attacking ally")
                        it.hp -= if (creature.type == Type.G) 3 else elfPow
                        if (it.hp <= 0) {
                            it.coord = -1 to -1
                        }
                    }
                } else {
                    moved = true
                    creatures[index].coord = target[1]

                    val (x, y) = target[1]
                    targetCreature = creatures
                        .filter {
                            it.coord in listOf(
                                x to (y - 1),
                                x - 1 to y,
                                x + 1 to y,
                                x to (y + 1)
                            ) && it.type != creature.type && it.hp > 0
                        }
                        .sortedBy { 10000 * it.hp + 32 * it.coord.second + it.coord.first }
                        .firstOrNull()
                    targetCreature?.let {
                        if (it.type == creature.type) throw RuntimeException("Attacking ally")
                        it.hp -= if (creature.type == Type.G) 3 else elfPow
                        if (it.hp <= 0) {
                            it.coord = -1 to -1
                        }
                    }

                }


                render2(g, creatures, walls, if (moved) target else null, targetCreature)
                ImageIO.write(image, "gif", File("img/d15/f%03d_%03d.gif".format(fullRounds, ++step)))

            }

        if (fullRound) {
            fullRounds++

        } else {
            break
        }
    }
}


fun getTarget2(index: Int, creatures: List<Creature>, walls: Set<Pair<Int, Int>>): List<Pair<Int, Int>>? {
    val type = creatures[index].type
    val (x, y) = creatures[index].coord

    var targetCreature = creatures
        .filter {
            it.coord in listOf(x to (y - 1), x - 1 to y, x + 1 to y, x to (y + 1)) && it.type != type && it.hp > 0
        }
        .sortedBy { 10000 * it.hp + 32 * it.coord.second + it.coord.first }
        .firstOrNull()
    if (targetCreature != null) {
        return listOf(x to y, targetCreature.coord)
    }

    val targetCells = creatures.filter { it.type != type }
        .flatMap {
            listOf(
                it.coord.first to it.coord.second - 1,
                it.coord.first - 1 to it.coord.second,
                it.coord.first + 1 to it.coord.second,
                it.coord.first to it.coord.second + 1
            )
        }
        .filter { !walls.contains(it) }
        .filter { creatures.none { c -> c.coord == it } }
        .toSet()

    return nextStep2(index, creatures, walls, targetCells)
}

fun nextStep2(
    index: Int,
    creatures: List<Creature>,
    walls: Set<Pair<Int, Int>>,
    targets: Iterable<Pair<Int, Int>>
): List<Pair<Int, Int>>? {
    val (x, y) = creatures[index].coord

    val done = mutableSetOf<Pair<Int, Int>>()
    val queue = mutableListOf((x to y) to emptyList<Pair<Int, Int>>())

    while (queue.isNotEmpty()) {
        val newQueue = mutableListOf<Pair<Pair<Int, Int>, List<Pair<Int, Int>>>>()

        while (queue.isNotEmpty()) {
            val (cell, path) = queue.removeAt(0)
            done.add(cell)

            val newPath = path + listOf(cell)

            val newCells = listOf(
                (cell.first to cell.second - 1) to newPath,
                (cell.first - 1 to cell.second) to newPath,
                (cell.first + 1 to cell.second) to newPath,
                (cell.first to cell.second + 1) to newPath
            )
                .filter { !done.contains(it.first) }
                .filter { queue.none { x -> x.first == (it.first) } }
                .filter { newQueue.none { x -> x.first == (it.first) } }
                .filter { !walls.contains(it.first) }
                .filter { creatures.none { c -> c.coord == it.first } }

            val tc = newCells.firstOrNull { it.first in targets }
            if (tc != null) return (tc.second + listOf(tc.first))

            newQueue.addAll(newCells)
        }
        queue.addAll(newQueue)
        queue.sortBy { (cell, path) -> 10000 * path.size + 32 * cell.second + cell.first }
    }

    return null
}

private fun render2(
    g: Graphics2D,
    creatures: MutableList<Creature>,
    walls: MutableSet<Pair<Int, Int>>,
    path: List<Pair<Int, Int>>?,
    targetCreature: Creature?
) {
    val r = 6
    (0..32).forEach { x ->
        (0..32).forEach { y ->
            if (walls.contains(x to y)) {
                g.color = Color.DARK_GRAY
                g.fillRect(16 * x, 16 * y, 16, 16)
                g.color = Color.GRAY
                g.fillRoundRect(16 * x, 16 * y, 16, 16, 2 * r, 2 * r)

                val topLeft = walls.contains(x - 1 to y - 1) || x == 0 && y == 0
                val top = walls.contains(x to y - 1)
                val topRight = walls.contains(x + 1 to y - 1) || x == 31 && y == 0
                val left = walls.contains(x - 1 to y)
                val right = walls.contains(x + 1 to y)
                val bottomLeft = walls.contains(x - 1 to y + 1) || x == 0 && y == 31
                val bottom = walls.contains(x to y + 1)
                val bottomRight = walls.contains(x + 1 to y + 1) || x == 31 && y == 31
                if (top || left || topLeft) g.fillRect(16 * x, 16 * y, r, r)
                if (top || right || topRight) g.fillRect(16 * x + 16 - r, 16 * y, r, r)
                if (bottom || left || bottomLeft) g.fillRect(16 * x, 16 * y + 16 - r, r, r)
                if (bottom || right || bottomRight) g.fillRect(16 * x + 16 - r, 16 * y + 16 - r, r, r)
            } else {
                g.color = Color.GRAY
                g.fillRect(16 * x, 16 * y, 16, 16)
                g.color = Color.DARK_GRAY
                g.fillRoundRect(16 * x, 16 * y, 16, 16, 2 * r, 2 * r)

                val top = !walls.contains(x to y - 1)
                val left = !walls.contains(x - 1 to y)
                val right = !walls.contains(x + 1 to y)
                val bottom = !walls.contains(x to y + 1)
                if (top || left) g.fillRect(16 * x, 16 * y, r, r)
                if (top || right) g.fillRect(16 * x + 16 - r, 16 * y, r, r)
                if (bottom || left) g.fillRect(16 * x, 16 * y + 16 - r, r, r)
                if (bottom || right) g.fillRect(16 * x + 16 - r, 16 * y + 16 - r, r, r)
            }
            creatures.firstOrNull { it.coord == x to y }?.let {
                g.color = when (it.type) {
                    Type.E -> Color.RED
                    Type.G -> Color.GREEN.darker()
                }
                g.fillOval(16 * x + 2, 16 * y + 4, 12, 12)

                g.color = Color.BLACK
                g.fillRect(16 * x, 16 * y, 16, 6)

                val hpWidth = 14 * it.hp / 200
                g.color = when (it.hp) {
                    in (1..50) -> Color.RED
                    in (51..100) -> Color.ORANGE
                    in (101..150) -> Color.GREEN.darker()
                    in (151..200) -> Color.GREEN
                    else -> Color.BLACK
                }
                g.fillRect(16 * x + 1, 16 * y + 1, hpWidth, 4)

                g.color = Color.WHITE
                val xoff = 8 - min(14, g.fontMetrics.stringWidth(it.hp.toString())) / 2
                g.drawString(it.hp.toString(), 16 * x + xoff, 16 * y + 14)

            }
        }
    }

    if (path != null && path.size > 1) {
        g.color = Color.MAGENTA
        g.stroke = BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10f, floatArrayOf(6f, 10f), 3f)
        g.drawPolyline(path.map { 16 * it.first + 8 }.toIntArray(),
            path.map { 16 * it.second + 8 }.toIntArray(), path.size
        )
    }

    if (targetCreature != null) {
        g.color = Color.CYAN
        g.drawLine(
            16 * targetCreature.coord.first + 4, 16 * targetCreature.coord.second + 6,
            16 * targetCreature.coord.first + 12, 16 * targetCreature.coord.second + 14
        )
    }

}
