package com.szendo.aoc.y2018.day16

import com.szendo.aoc.Helper

fun main() {
    val regex = Regex("-?\\d+")

    val samples = Helper.getResourceAsStream("16-samples.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.toList().chunked(4)
                .map { (l1, l2, l3) ->
                    val before = regex.findAll(l1).map { it.value.toInt() }.toList()
                    val instruction = regex.findAll(l2).map { it.value.toInt() }.toList()
                    val after = regex.findAll(l3).map { it.value.toInt() }.toList()

                    Sample(instruction, before, after)
                }
        }

    val ops = setOf<Op>(
        /* addr */ { i1, i2, r -> r[i1] + r[i2] },
        /* addi */ { i1, i2, r -> r[i1] + i2 },
        /* mulr */ { i1, i2, r -> r[i1] * r[i2] },
        /* muli */ { i1, i2, r -> r[i1] * i2 },
        /* banr */ { i1, i2, r -> r[i1] and r[i2] },
        /* bani */ { i1, i2, r -> r[i1] and i2 },
        /* borr */ { i1, i2, r -> r[i1] or r[i2] },
        /* bori */ { i1, i2, r -> r[i1] or i2 },
        /* setr */ { i1, _, r -> r[i1] },
        /* seti */ { i1, _, _ -> i1 },
        /* gtir */ { i1, i2, r -> if (i1 > r[i2]) 1 else 0 },
        /* gtri */ { i1, i2, r -> if (r[i1] > i2) 1 else 0 },
        /* gtrr */ { i1, i2, r -> if (r[i1] > r[i2]) 1 else 0 },
        /* eqir */ { i1, i2, r -> if (i1 == r[i2]) 1 else 0 },
        /* eqri */ { i1, i2, r -> if (r[i1] == i2) 1 else 0 },
        /* eqrr */ { i1, i2, r -> if (r[i1] == r[i2]) 1 else 0 }
    )

    val answer1 = samples.count { sample ->
        ops.count { op ->
            val (_, i1, i2, o) = sample.instruction
            eval(i1, i2, o, sample.before, op) == sample.after
        } >= 3
    }
    println("Answer 1: $answer1")

    val opcodeMap = mapOf(*(0..15).map { it to ops.toMutableSet() }.toTypedArray())

    for (sample in samples) {
        opcodeMap[sample.instruction[0]]!!.removeIf { op ->
            val (_, i1, i2, o) = sample.instruction
            eval(i1, i2, o, sample.before, op) != sample.after
        }
    }

    while (opcodeMap.any { it.value.size > 1 }) {
        for (op in ops) {
            if (opcodeMap.count { op in it.value } == 1) {
                opcodeMap.filter { op in it.value }.forEach { _, opSet -> opSet.removeIf { it != op } }
            }
        }

        opcodeMap.filter { it.value.size == 1 }.forEach { _, singleOpSet ->
            opcodeMap.forEach { _, otherOpSet ->
                if (singleOpSet != otherOpSet) {
                    otherOpSet.removeAll(singleOpSet)
                }
            }
        }
    }

    var registers = listOf(0, 0, 0, 0)
    Helper.getResourceAsStream("16-test-program.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.forEach { line ->
                val (opcode, i1, i2, o) = regex.findAll(line).map { it.value.toInt() }.toList()
                registers = eval(i1, i2, o, registers, opcodeMap[opcode]!!.first())
            }
        }

    val answer2 = registers[0]
    println("Answer 2: $answer2")
}
