package com.szendo.aoc.y2018.day16

import com.szendo.aoc.Helper

fun main() {
    val regex = Regex("-?\\d+")

    val samples = Helper.getResourceAsStream("16-samples.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.toList().chunked(4)
                .map { (l1, l2, l3) ->
                    val before = regex.findAll(l1).map { it.value.toInt() }.toList()
                    val instruction = regex.findAll(l2).map { it.value.toInt() }.toList()
                    val after = regex.findAll(l3).map { it.value.toInt() }.toList()

                    Sample(instruction, before, after)
                }
        }

    val opMap = mapOf<String, Op>(
        "addr" to { i1, i2, r -> r[i1] + r[i2] },
        "addi" to { i1, i2, r -> r[i1] + i2 },
        "mulr" to { i1, i2, r -> r[i1] * r[i2] },
        "muli" to { i1, i2, r -> r[i1] * i2 },
        "banr" to { i1, i2, r -> r[i1] and r[i2] },
        "bani" to { i1, i2, r -> r[i1] and i2 },
        "borr" to { i1, i2, r -> r[i1] or r[i2] },
        "bori" to { i1, i2, r -> r[i1] or i2 },
        "setr" to { i1, _, r -> r[i1] },
        "seti" to { i1, _, _ -> i1 },
        "gtir" to { i1, i2, r -> if (i1 > r[i2]) 1 else 0 },
        "gtri" to { i1, i2, r -> if (r[i1] > i2) 1 else 0 },
        "gtrr" to { i1, i2, r -> if (r[i1] > r[i2]) 1 else 0 },
        "eqir" to { i1, i2, r -> if (i1 == r[i2]) 1 else 0 },
        "eqri" to { i1, i2, r -> if (r[i1] == i2) 1 else 0 },
        "eqrr" to { i1, i2, r -> if (r[i1] == r[i2]) 1 else 0 }
    )

    val answer1 = samples.count { sample ->
        opMap.count { (_, opfn) ->
            val (_, i1, i2, o) = sample.instruction
            eval(i1, i2, o, sample.before, opfn) == sample.after
        } >= 3
    }
    println("Answer 1: $answer1")

    val opcodeMap = mapOf(*(0..15).map { it to opMap.keys.toMutableSet() }.toTypedArray())

    for (sample in samples) {
        val opcode = sample.instruction[0]
        opcodeMap[opcode]!!.removeIf { possibleOp ->
            val (_, i1, i2, o) = sample.instruction
            eval(i1, i2, o, sample.before, opMap[possibleOp]!!) != sample.after
        }
    }

    while (opcodeMap.any { it.value.size > 1 }) {
        for (op in opMap.keys) {
            if (opcodeMap.count { op in it.value } == 1) {
                opcodeMap.filter { op in it.value }.forEach { _, opSet ->
                    opSet.removeIf { it != op }
                }
            }
        }

        opcodeMap.filter { it.value.size == 1 }.forEach { _, opSet ->
            opcodeMap.forEach { _, opSet2 ->
                if (opSet != opSet2) {
                    opSet2.removeAll(opSet)
                }
            }
        }
    }

    var registers = listOf(0, 0, 0, 0)
    Helper.getResourceAsStream("16-test-program.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.forEach { line ->
                val (op, i1, i2, o) = regex.findAll(line).map { it.value.toInt() }.toList()
                registers = eval(i1, i2, o, registers, opMap[opcodeMap[op]!!.first()]!!)
            }
        }

    val answer2 = registers[0]
    println("Answer 2: $answer2")
}

typealias Op = (Int, Int, List<Int>) -> Int

data class Sample(val instruction: List<Int>, val before: List<Int>, val after: List<Int>)

fun eval(i1: Int, i2: Int, o: Int, registers: List<Int>, op: Op): List<Int> {
    return registers.mapIndexed { index, value -> if (o == index) op(i1, i2, registers) else value }
}