package com.szendo.aoc.y2018.day17

import com.szendo.aoc.*

fun main() {
    val regex = Regex("\\d+")
    val clay = Helper.getResourceAsStream("2018/17.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.toList().flatMap { line ->
                val numbers = regex.findAll(line).map { it.value.toInt() }.toList()
                if (line[0] == 'x') {
                    (numbers[1]..numbers[2]).map { numbers[0] to it }
                } else {
                    (numbers[1]..numbers[2]).map { it to numbers[0] }
                }
            }
        }
        .toSet()

    val minY = clay.minOf { it.y }
    val maxY = clay.maxOf { it.y }

    val stillWater = mutableSetOf<Coord>()
    val flowingWater = mutableSetOf<Coord>()
    val queue = mutableListOf(500 to 0)

    while (queue.isNotEmpty()) {
        var coord = queue.removeAt(0)

        while (coord.y <= maxY
            && coord !in flowingWater
            && coord !in clay
            && coord !in stillWater
        ) {
            flowingWater.add(coord)
            coord = (coord.x to coord.y + 1)
        }

        if (coord.y > maxY || coord in flowingWater) {
            continue
        }

        var currY = coord.y
        val pool = mutableSetOf<Coord>()
        val line = mutableSetOf<Coord>()
        var reachedSide = false
        while (!reachedSide) {
            currY--
            pool.addAll(line)
            line.clear()
            line.add(coord.x to currY)

            var l = (coord.x - 1 to currY)
            while (l !in clay) {
                val belowL = l + (0 to 1)
                if (belowL in clay || belowL in stillWater || belowL in pool) {
                    line.add(l)
                    l = (l.x - 1) to currY
                } else {
                    reachedSide = true
                    queue.add(l)
                    break
                }
            }
            var r = (coord.x + 1 to currY)
            while (r !in clay) {
                val belowR = r + (0 to 1)
                if (belowR in clay || belowR in stillWater || belowR in pool) {
                    line.add(r)
                    r = (r.x + 1) to currY
                } else {
                    reachedSide = true
                    queue.add(r)
                    break
                }
            }
        }

        flowingWater.addAll(line)
        flowingWater.removeAll(pool)
        stillWater.addAll(pool)
    }

    val answer1 = stillWater.count { it.y in minY..maxY } + flowingWater.count { it.y in minY..maxY }
    println("Answer 1: $answer1")
    val answer2 = stillWater.count { it.y in minY..maxY }
    println("Answer 2: $answer2")
}
