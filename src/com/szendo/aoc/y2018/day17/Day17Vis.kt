package com.szendo.aoc.y2018.day17

import com.szendo.aoc.Coord
import com.szendo.aoc.Helper
import com.szendo.aoc.x
import com.szendo.aoc.y
import java.awt.BasicStroke
import java.awt.Color
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO
import kotlin.math.max
import kotlin.math.min

fun main() {
    val regex = Regex("\\d+")
    val clay = Helper.getResourceAsStream("17-riddle.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.toList().flatMap { line ->
                val numbers = regex.findAll(line).map { it.value.toInt() }.toList()
                if (line[0] == 'x') {
                    (numbers[1]..numbers[2]).map { numbers[0] to it }
                } else {
                    (numbers[1]..numbers[2]).map { it to numbers[0] }
                }
            }
        }
        .toSet()

    val minY = clay.minOf { it.y }
    val maxY = clay.maxOf { it.y }

    val minX = clay.minOf { it.x }
    val maxX = clay.maxOf { it.x }

    val image = BufferedImage(maxX - minX + 21, maxY + 11, BufferedImage.TYPE_INT_RGB)
    val g = image.createGraphics()
    g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE)

    val stillWater = mutableSetOf<Coord>()
    val flowingWater = mutableSetOf<Coord>()
    val queue = PriorityQueue<Coord>(compareBy({ it.y }, { it.x })).apply { add(500 to 0) }

    render(clay, stillWater, flowingWater, queue, image, g)

    var frame = 0
    while (queue.isNotEmpty()) {
        var coord = queue.remove()

        while (coord.y <= (maxY + 10)
            && coord !in flowingWater
            && coord !in clay
            && coord !in stillWater
        ) {
            flowingWater.add(coord)
            coord = (coord.x to coord.y + 1)
        }

        if (coord.y > (maxY + 10) || coord in flowingWater) {
            render(clay, stillWater, flowingWater, queue, image, g, ++frame)
            continue
        }

        var currY = coord.y
        val pool = mutableSetOf<Coord>()
        val line = mutableSetOf<Coord>()
        var reachedSide = false
        while (!reachedSide) {
            currY--
            pool.addAll(line)
            line.clear()

            if ((coord.x to currY) in clay) {
                break
            }

            line.add(coord.x to currY)

            var l = (coord.x - 1 to currY)
            while (l !in clay) {
                if ((l.x to l.y + 1) in clay
                    || (l.x to l.y + 1) in stillWater
                    || (l.x to l.y + 1) in pool
                ) {
                    line.add(l)
                    l = (l.x - 1) to currY
                } else {
                    reachedSide = true
                    queue.add(l)
                    break
                }
            }
            var r = (coord.x + 1 to currY)
            while (r !in clay) {
                if ((r.x to r.y + 1) in clay
                    || (r.x to r.y + 1) in stillWater
                    || (r.x to r.y + 1) in pool
                ) {
                    line.add(r)
                    r = (r.x + 1) to currY
                } else {
                    reachedSide = true
                    queue.add(r)
                    break
                }
            }
        }

        stillWater.addAll(pool)
        flowingWater.addAll(line)
        flowingWater.removeAll(pool)
        queue.removeAll(line)
        queue.removeAll(pool)

        render(clay, stillWater, flowingWater, queue, image, g, ++frame)
    }

    val answer1 = stillWater.count { it.y in minY..maxY } + flowingWater.count { it.y in minY..maxY }
    println("Answer 1: $answer1")
    val answer2 = stillWater.count { it.y in minY..maxY }
    println("Answer 2: $answer2")
}

private fun render(
    clay: Set<Pair<Int, Int>>,
    stillWater: Set<Coord>,
    flowingWater: Set<Coord>,
    queue: Queue<Coord>,
    image: BufferedImage,
    g: Graphics2D,
    frame: Int = 0
) {
    val maxY = clay.maxOf { it.y }

    val minX = clay.minOf { it.x }
    val maxX = clay.maxOf { it.x }

    ((minX - 10)..(maxX + 10)).forEach { x ->
        (0..(maxY + 10)).forEach { y ->
            when (x to y) {
                in queue -> if ((x to y) == queue.first()) Color.MAGENTA else Color.RED
                in flowingWater -> Color.CYAN
                in stillWater -> Color.BLUE
                in clay -> Color.DARK_GRAY
                else -> Color.LIGHT_GRAY
            }.let { color ->
                image.setRGB(x - minX + 10, y, color.rgb)
            }
        }
    }

    g.stroke = BasicStroke(2f)
    g.color = Color.RED
    queue.drop(1).forEach { (x, y) -> g.drawOval(x - minX + 10 - 4, y - 4, 9, 9) }
    queue.firstOrNull()?.let { (x, y) ->
        g.color = Color.MAGENTA
        g.drawOval(x - minX + 10 - 4, y - 4, 9, 9)
    }

    val height = 480
    val padding = 120
    val scroll = 10

    val lowestY = max(stillWater.maxOfOrNull { it.y } ?: 0, flowingWater.maxOfOrNull { it.y } ?: 0)

    if (lowestY + padding > lastOffsetY + height) {
        lastOffsetY = min(min(lastOffsetY + scroll, lowestY - height + padding), image.height - height)
        println("$frame: $lastOffsetY")
    }


    val scaledImage = BufferedImage(image.width, height, BufferedImage.TYPE_INT_RGB)
    val sg = scaledImage.createGraphics()
    sg.color = Color.LIGHT_GRAY
    sg.fillRect(0, 0, scaledImage.width, scaledImage.height)
    sg.drawImage(image, 0, -lastOffsetY, null)

    ImageIO.write(scaledImage, "gif", File("img/d17/f%07d.gif".format(frame)))
}

private var lastOffsetY = 0

private const val SCALE = 4