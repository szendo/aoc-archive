package com.szendo.aoc.y2018

import com.szendo.aoc.Coord
import com.szendo.aoc.plus
import com.szendo.aoc.x
import com.szendo.aoc.y
import java.util.*

fun main() {
    val depth = 9465
    val target = 13 to 704
//    val depth = 510
//    val target = 10 to 10

    val erosionLevel = mutableMapOf<Coord, Int>()
    erosionLevel[0 to 0] = 0

    for (x in 0..target.x + 1000) {
        erosionLevel[x to 0] = (x * 16807 + depth) % 20183
    }

    for (y in 0..target.y + 1000) {
        erosionLevel[0 to y] = (y * 48271 + depth) % 20183
    }

    for (x in 1..target.x + 1000) {
        for (y in 1..target.y + 1000) {
            erosionLevel[x to y] = (erosionLevel[x - 1 to y]!! * erosionLevel[x to y - 1]!! + depth) % 20183
        }
    }

    erosionLevel[target] = 0

    val answer1 = (0..target.x).sumOf { x -> (0..target.y).sumOf { y -> erosionLevel[x to y]!! % 3 } }
    println("Answer 1: $answer1")

    val reached = mutableMapOf<Pair<Coord, Tool>, Int>()
    val queue = PriorityQueue<ItemD22>().apply {
        add(ItemD22(0 to 0, Tool.TORCH, 0))
    }

    while ((target to Tool.TORCH) !in reached) {
        val (coord, tool, time) = queue.poll()

        if (reached.putIfAbsent(coord to tool, time) == null) {
            Tool.values().forEach { newTool ->
                if ((coord to newTool) !in reached && (erosionLevel[coord]!! % 3) in newTool.allowedTypes) {
                    queue.add(ItemD22(coord, newTool, time + 7))
                }
            }

            if (coord.y > 0) {
                (coord + (0 to -1)).let { newCoord ->
                    if ((erosionLevel[newCoord]!! % 3) in tool.allowedTypes && (newCoord to tool) !in reached) {
                        queue.add(ItemD22(newCoord, tool, time + 1))
                    }
                }

            }

            (coord + (0 to 1)).let { newCoord ->
                if ((erosionLevel[newCoord]!! % 3) in tool.allowedTypes && (newCoord to tool) !in reached) {
                    queue.add(ItemD22(newCoord, tool, time + 1))
                }
            }

            if (coord.x > 0) {
                (coord + (-1 to 0)).let { newCoord ->
                    if ((erosionLevel[newCoord]!! % 3) in tool.allowedTypes && (newCoord to tool) !in reached) {
                        queue.add(ItemD22(newCoord, tool, time + 1))
                    }
                }
            }

            (coord + (1 to 0)).let { newCoord ->
                if ((erosionLevel[newCoord]!! % 3) in tool.allowedTypes && (newCoord to tool) !in reached) {
                    queue.add(ItemD22(newCoord, tool, time + 1))
                }
            }
        }
    }

    val answer2 = reached[target to Tool.TORCH]!!
    println("Answer 2: $answer2")
}