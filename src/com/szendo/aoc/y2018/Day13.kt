package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    val lines = Helper.getResourceAsStream("2018/13.txt")
        .bufferedReader()
        .useLines { it.toList() }

    val carts = mutableListOf<Pair<Int, Int>>()
    val cartDirs = mutableListOf<Pair<Int, Int>>()
    val cartInter = mutableListOf<Int>()
    val tracks = mutableMapOf<Pair<Int, Int>, Char>()

    lines.forEachIndexed { y, line ->
        line.forEachIndexed { x, c ->
            when (c) {
                '>' -> {
                    carts.add(x to y)
                    cartDirs.add(1 to 0)
                    cartInter.add(0)
                }
                '<' -> {
                    carts.add(x to y)
                    cartDirs.add(-1 to 0)
                    cartInter.add(0)
                }
                '^' -> {
                    carts.add(x to y)
                    cartDirs.add(0 to -1)
                    cartInter.add(0)
                }
                'v' -> {
                    carts.add(x to y)
                    cartDirs.add(0 to 1)
                    cartInter.add(0)
                }
                in listOf('+', '/', '\\') -> tracks[x to y] = c
            }
        }
    }

    var tick = 0
    while (carts.count { it.first >= 0 } > 1) {
        tick++
        val crashedIndices = mutableSetOf<Int>()
        carts.withIndex()
            .filter { it.value.first >= 0 }
            .sortedBy { (_, coord) ->
                val (x, y) = coord
                x + 1000 * y
            }
            .forEach { (index, coord) ->
                if (crashedIndices.contains(index)) {
                    return@forEach
                }

                val (x, y) = coord
                val (dx, dy) = cartDirs[index]

                val nextCoord = (x + dx) to (y + dy)

                if (carts.any { it == nextCoord }) {
                    if (carts.none { it.first < 0 }) {
                        println("Answer 1: ${nextCoord.first},${nextCoord.second} (tick=$tick)")
                    } else {
                        println("Crash: ${nextCoord.first},${nextCoord.second} (tick=$tick)")
                    }
                    carts[index] = -1 to index
                    cartDirs[index] = 0 to 0
                    crashedIndices.add(index)
                    carts.withIndex().filter { (_, otherCoord) -> nextCoord == otherCoord }
                        .forEach { (otherIndex, _) ->
                            carts[otherIndex] = -1 to otherIndex
                            cartDirs[otherIndex] = 0 to 0
                            crashedIndices.add(otherIndex)
                        }
                    return@forEach
                }

                carts[index] = nextCoord
                val track = tracks[nextCoord]

                when (track) {
                    '/' -> {
                        cartDirs[index] = (-dy to -dx)
                    }
                    '\\' -> {
                        cartDirs[index] = (dy to dx)
                    }
                    '+' -> {
                        when (cartInter[index]) {
                            0 -> {
                                cartDirs[index] = (dy to -dx)
                                cartInter[index] = 1
                            }
                            1 -> {
                                cartInter[index] = 2
                            }
                            2 -> {
                                cartDirs[index] = (-dy to dx)
                                cartInter[index] = 0
                            }
                            else -> throw RuntimeException()
                        }
                    }
                }
            }
    }

    val lastCart = carts.first { it.first >= 0 }
    val answer2 = "${lastCart.first},${lastCart.second}"
    println("Answer 2: $answer2")
}