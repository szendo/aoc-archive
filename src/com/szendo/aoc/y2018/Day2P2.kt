package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    val input: List<String> = Helper.getResourceAsStream("2018/2.txt")
        .bufferedReader()
        .useLines { it.toList() }

    val previousIds = mutableListOf<String>()
    for (id in input) {
        val answer2 = previousIds
            .mapNotNull { prevId -> getCommonIf1Diff(id, prevId) }
            .firstOrNull()

        if (answer2 != null) {
            println("Answer 2: $answer2")
            return
        }
        previousIds.add(id)
    }

}

fun getCommonIf1Diff(s1: String, s2: String): String? {
    if (s1.length != s2.length) {
        return null
    }

    for (i in 0 until s1.length) {
        val s = s1.substring(0, i) + s1.substring(i + 1, s1.length)

        if (s == s2.substring(0, i) + s2.substring(i + 1, s2.length)) {
            return s
        }
    }

    return null
}