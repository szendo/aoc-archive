package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    val input: List<Int> = Helper.getResourceAsStream("2018/8.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList().flatMap { it.split(' ').map(String::toInt) } }

    val answer1 = calculate1(input).first
    println("Answer 1: $answer1")
    val answer2 = calculate2(input).first
    println("Answer 2: $answer2")
}

private fun calculate1(input: List<Int>, index: Int = 0): Pair<Int, Int> {
    var i = index
    val childNodes = input[i++]
    val metadataCount = input[i++]

    var sum = 0
    repeat(childNodes) {
        val (metaSum, iRet) = calculate1(input, i)
        i = iRet
        sum += metaSum
    }

    repeat(metadataCount) {
        sum += input[i++]
    }

    return sum to i
}

private fun calculate2(input: List<Int>, index: Int = 0): Pair<Int, Int> {
    var i = index
    val childNodes = input[i++]
    val metadataCount = input[i++]

    val entries = mutableListOf<Int>()
    repeat(childNodes) {
        val (entry, iRet) = calculate2(input, i)
        i = iRet
        entries.add(entry)
    }

    var sum = 0
    if (childNodes > 0) {
        repeat(metadataCount) {
            val ptr = input[i++]
            if (ptr in 1..childNodes) {
                sum += entries[ptr - 1]
            }
        }
    } else {
        repeat(metadataCount) {
            sum += input[i++]
        }
    }

    return sum to i
}