package com.szendo.aoc.y2018

fun main() {
    val players = 468
    val lastMarble = 71843 * 100


    val scores = mutableListOf<Long>()
    repeat(players) {
        scores.add(0)
    }
    val marbles = mutableListOf(0)
    var lastMarbleIndex = 2

    var lastTime = System.currentTimeMillis()
    for (m in 1..lastMarble) {
        if (m % 500_000 == 0) {
            val time = System.currentTimeMillis()

            println("Time: ${(time - lastTime) / 1000} s (m = $m)")
            lastTime = time
        }

        if (m % 23 == 0) {
            scores[m % scores.size] += m.toLong()

            val nextIndex = (lastMarbleIndex - 7 + marbles.size) % marbles.size
            scores[m % scores.size] += marbles.removeAt(nextIndex).toLong()
            lastMarbleIndex = nextIndex

        } else {
            val nextIndex = (lastMarbleIndex + 2) % marbles.size
            marbles.add(nextIndex, m)
            lastMarbleIndex = nextIndex
        }
    }

    println("Answer 1: " + scores.maxOf { it })
}