package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {

    fun react(s: String): String {
        val chars = s.toMutableList()

        var i = 0
        while (i < chars.size - 1) {
            if (i + 1 >= chars.size) {
                break
            }

            if (chars[i] != chars[i + 1] && chars[i].lowercaseChar() == chars[i + 1].lowercaseChar()) {
                chars.removeAt(i)
                chars.removeAt(i)
                if (i > 0) {
                    i--
                }
            } else {
                i++
            }
        }

        return String(chars.toCharArray())
    }

    val input = Helper.getResourceAsStream("2018/5.txt").bufferedReader().readText().trim()

    println("Answer 1: ${react(input).length}")

    var char = 'a'
    var len = Int.MAX_VALUE
    for (c in 'a'..'z') {
        val l = react(input.replace(c.toString(), "", true)).length
        if (l < len) {
            char = c
            len = l
        }
    }

    println("Answer 2: $len ($char)")


}

