package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    val input: List<Long> = Helper.getResourceAsStream("2018/1.txt")
        .bufferedReader()
        .useLines { it.map(String::toLong).toList() }

    var answer1: Long? = null
    var answer2: Long? = null

    var currentFreq: Long = 0
    val reachedFrequencies = mutableSetOf(0L)

    while (answer1 == null || answer2 == null) {
        for (freqChange in input) {
            currentFreq += freqChange

            if (answer2 == null && !reachedFrequencies.add(currentFreq)) {
                answer2 = currentFreq
                println("Answer 2: $answer2")
            }
        }
        if (answer1 == null) {
            answer1 = currentFreq
            println("Answer 1: $answer1")
        }
    }
}
