package com.szendo.aoc.y2018

fun main() {
    val input = 793031

    val recipes = mutableListOf(3, 7)
    var i1 = 0
    var i2 = 1

    while (recipes.size < input + 10) {
        val newRecipes = mutableListOf<Int>()
        var sum = recipes[i1] + recipes[i2]
        if (sum == 0) {
            newRecipes.add(0)
        } else {
            while (sum > 0) {
                newRecipes.add(sum % 10)
                sum /= 10
            }
        }
        recipes.addAll(newRecipes.reversed())

        i1 = (i1 + 1 + recipes[i1]) % recipes.size
        i2 = (i2 + 1 + recipes[i2]) % recipes.size
    }

    val answer1 = recipes.drop(input).take(10).joinToString("") { it.toString() }
    println("Answer 1: $answer1")


    while (recipes.takeLast(8).joinToString("") { it.toString() }.indexOf(input.toString()) == -1) {
        val newRecipes = mutableListOf<Int>()
        var sum = recipes[i1] + recipes[i2]
        if (sum == 0) {
            newRecipes.add(0)
        } else {
            while (sum > 0) {
                newRecipes.add(sum % 10)
                sum /= 10
            }
        }
        recipes.addAll(newRecipes.reversed())

        i1 = (i1 + 1 + recipes[i1]) % recipes.size
        i2 = (i2 + 1 + recipes[i2]) % recipes.size
    }

    val answer2 = recipes.joinToString("") { it.toString() }.indexOf(input.toString())
    println("Answer 2: $answer2")
}