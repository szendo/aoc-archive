package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {

    val regex = Regex("Step (.) must be finished before step (.) can begin.")

    val input: List<Pair<Char, Char>> = Helper.getResourceAsStream("2018/7.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.mapNotNull { line -> regex.matchEntire(line)?.groupValues }
                .map { it[1][0] to it[2][0] }
                .toList()
        }

    val answer1 = calculate1(input)
    println("Answer 1: $answer1")
    val answer2 = calculate2(input)
    println("Answer 2: $answer2")

}

private fun calculate1(input: List<Pair<Char, Char>>): String {
    val stepsWithDeps = mutableMapOf<Char, MutableSet<Char>>()

    for ((blocker, dependent) in input) {
        stepsWithDeps.getOrPut(blocker, ::mutableSetOf)
        stepsWithDeps.getOrPut(dependent, ::mutableSetOf).add(blocker)
    }

    val result = StringBuilder()
    while (stepsWithDeps.isNotEmpty()) {
        val stepToDo = stepsWithDeps.keys
            .filter { stepsWithDeps[it].isNullOrEmpty() }
            .sorted()
            .first()
        stepsWithDeps.remove(stepToDo)

        result.append(stepToDo)
        stepsWithDeps.forEach { _, deps -> deps.remove(stepToDo) }
    }

    return result.toString()
}

private fun calculate2(input: List<Pair<Char, Char>>): Int {
    val workers = mutableListOf(
        ' ' to 0,
        ' ' to 0,
        ' ' to 0,
        ' ' to 0,
        ' ' to 0
    )

    val stepsWithDeps = mutableMapOf<Char, MutableSet<Char>>()

    for ((blocker, dependent) in input) {
        stepsWithDeps.getOrPut(blocker, ::mutableSetOf)
        stepsWithDeps.getOrPut(dependent, ::mutableSetOf).add(blocker)
    }

    var currentTime = 0;
    while (stepsWithDeps.isNotEmpty()) {
        if (workers.all { it.second > currentTime }) {
            currentTime = workers.minOf { it.second }

            workers.filter { (_, time) -> time == currentTime }
                .forEach { (step, _) -> stepsWithDeps.forEach { _, deps -> deps.remove(step) } }
        }

        val next = stepsWithDeps.keys
            .filter { stepsWithDeps[it].isNullOrEmpty() }
            .sorted()
            .firstOrNull()

        if (next == null) {
            currentTime = workers.filter { (_, time) -> time > currentTime }.minOfOrNull { it.second }
                ?: throw RuntimeException()
            continue
        }

        stepsWithDeps.remove(next)

        val freeWorkerIndex = workers.indexOfFirst { (_, time) -> time <= currentTime }
        workers[freeWorkerIndex] = next to (currentTime + 61 + (next - 'A'))
    }

    return workers.map { it.second }.maxOrNull() ?: currentTime
}
