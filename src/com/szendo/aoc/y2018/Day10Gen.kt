package com.szendo.aoc.y2018

import com.szendo.aoc.Helper
import java.security.SecureRandom
import javax.imageio.ImageIO


fun main() {
    val random = SecureRandom()

    val d = 10200 + random.nextInt(1000)

    val pos = mutableListOf<Pair<Int, Int>>()
    val vel = mutableListOf<Pair<Int, Int>>()
    val img = Helper.getResourceAsStream("ok.png").use { ImageIO.read(it) }
    for (x in 0 until img.width) {
        for (y in 0 until img.height) {
            if (img.getRGB(x, y) == 0xFF000000.toInt()) {
                pos.add(x to y)
                val xv = random.nextInt(10).let { if (it < 5) it + 1 else it - 10 }
                val yv = random.nextInt(10).let { if (it < 5) it + 1 else it - 10 }
                vel.add(xv to yv)
            }
        }
    }

    for (i in 0..pos.lastIndex) {
        val p = pos[i]
        val v = vel[i]
        println(
            "position=<%-6d, %6d> velocity=<%2d, %2d>".format(
                p.first - v.first * d,
                p.second - v.second * d,
                v.first,
                v.second
            )
        )
    }


}
