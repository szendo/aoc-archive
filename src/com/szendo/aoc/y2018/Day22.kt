package com.szendo.aoc.y2018

import com.szendo.aoc.Coord
import com.szendo.aoc.plus
import com.szendo.aoc.x
import com.szendo.aoc.y
import java.util.*

fun main() {
    val depth = 9465
    val target = 13 to 704

    val erosionLevel = mutableMapOf<Coord, Int>()

    val answer1 = (0..target.x).sumOf { x -> (0..target.y).sumOf { y -> erosionLevel.calc(x to y, target, depth) % 3 } }
    println("Answer 1: $answer1")

    val reached = mutableMapOf<Pair<Coord, Tool>, Int>()
    val queue = PriorityQueue<ItemD22>().apply {
        add(ItemD22(0 to 0, Tool.TORCH, 0))
    }

    while ((target to Tool.TORCH) !in reached) {
        val (coord, tool, time) = queue.poll()

        if (reached.putIfAbsent(coord to tool, time) == null) {
            Tool.values().forEach { newTool ->
                if ((coord to newTool) !in reached
                    && (erosionLevel.calc(coord, target, depth) % 3) in newTool.allowedTypes
                ) {
                    queue.add(ItemD22(coord, newTool, time + 7))
                }
            }

            if (coord.y > 0) {
                (coord + (0 to -1)).let { newCoord ->
                    if ((newCoord to tool) !in reached
                        && (erosionLevel.calc(newCoord, target, depth) % 3) in tool.allowedTypes
                    ) {
                        queue.add(ItemD22(newCoord, tool, time + 1))
                    }
                }
            }

            (coord + (0 to 1)).let { newCoord ->
                if ((newCoord to tool) !in reached
                    && (erosionLevel.calc(newCoord, target, depth) % 3) in tool.allowedTypes
                ) {
                    queue.add(ItemD22(newCoord, tool, time + 1))
                }
            }

            if (coord.x > 0) {
                (coord + (-1 to 0)).let { newCoord ->
                    if ((newCoord to tool) !in reached
                        && (erosionLevel.calc(newCoord, target, depth) % 3) in tool.allowedTypes
                    ) {
                        queue.add(ItemD22(newCoord, tool, time + 1))
                    }
                }
            }

            (coord + (1 to 0)).let { newCoord ->
                if ((newCoord to tool) !in reached
                    && (erosionLevel.calc(newCoord, target, depth) % 3) in tool.allowedTypes
                ) {
                    queue.add(ItemD22(newCoord, tool, time + 1))
                }
            }
        }
    }

    val answer2 = reached[target to Tool.TORCH]!!
    println("Answer 2: $answer2")
}

enum class Tool(val allowedTypes: Set<Int>) {
    NEITHER(setOf(1, 2)),
    CLIMBING_GEAR(setOf(0, 1)),
    TORCH(setOf(0, 2));
}

data class ItemD22(val coord: Coord, val tool: Tool, val time: Int) : Comparable<ItemD22> {
    override fun compareTo(other: ItemD22): Int = time.compareTo(other.time)
}

private fun MutableMap<Coord, Int>.calc(c: Coord, target: Coord, depth: Int): Int = computeIfAbsent(c) { (x, y) ->
    when {
        (x to y) in setOf(0 to 0, target) -> 0
        x == 0 -> (y * 48271 + depth) % 20183
        y == 0 -> (x * 16807 + depth) % 20183
        else -> (calc(x - 1 to y, target, depth) * calc(x to y - 1, target, depth) + depth) % 20183
    }
}