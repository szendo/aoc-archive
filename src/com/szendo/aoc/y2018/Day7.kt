package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {

    val regex = Regex("Step (.) must be finished before step (.) can begin.")

    val input: List<Pair<String, String>> = Helper.getResourceAsStream("2018/7.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.mapNotNull { line -> regex.matchEntire(line)?.groupValues }
                .map { it[1] to it[2] }
                .toList()
        }

    val answer1 = calculate1(input)
    println("Answer 1: $answer1")
    val answer2 = calculate2(input)
    println("Answer 2: $answer2")

}

private fun calculate1(input: List<Pair<String, String>>): String {
    val stepsWithDeps = mutableMapOf<String, MutableSet<String>>()

    for ((dep, step) in input) {
        stepsWithDeps.getOrPut(dep, ::mutableSetOf)
        stepsWithDeps.getOrPut(step, ::mutableSetOf).add(dep)
    }

    val result = StringBuilder()
    while (stepsWithDeps.isNotEmpty()) {
        val next = stepsWithDeps.keys
            .filter { stepsWithDeps[it].isNullOrEmpty() }
            .sorted()
            .first()
        stepsWithDeps.remove(next)

        result.append(next)
        stepsWithDeps.forEach { _, deps -> deps.remove(next) }
    }

    return result.toString()
}

private fun calculate2(input: List<Pair<String, String>>): Int {
    val workers = mutableListOf<Pair<String, Int>?>(null, null, null, null, null) // step -> doneOn

    val stepsWithDeps = mutableMapOf<String, MutableSet<String>>()

    for ((dep, step) in input) {
        stepsWithDeps.getOrPut(dep, ::mutableSetOf)
        stepsWithDeps.getOrPut(step, ::mutableSetOf).add(dep)
    }

    var currentTime = 0;
    while (stepsWithDeps.isNotEmpty()) {
        val next = stepsWithDeps.keys
            .filter { stepsWithDeps[it].isNullOrEmpty() }
            .sorted()
            .firstOrNull()
        stepsWithDeps.remove(next)

        if (next == null) {
            currentTime = workers.filterNotNull().minOf { it.second }

            workers.withIndex()
                .mapNotNull { if (it.value == null) null else IndexedValue(it.index, it.value!!) }
                .filter { it.value.second == currentTime }
                .forEach { (index, w) ->
                    stepsWithDeps.forEach { _, deps -> deps.remove(w.first) }
                    workers[index] = null
                }
        } else {
            val freeWorkerIndex = workers.indexOfFirst { it == null }
            if (freeWorkerIndex == -1) {
                val nextWorkerIndex = workers.withIndex().minByOrNull { (_, value) -> value!!.second }!!.index

                val (step, time) = workers[nextWorkerIndex]!!
                currentTime = time
                stepsWithDeps.forEach { _, v -> v.remove(step) }
                workers[nextWorkerIndex] = next to (time + 61 + (next[0] - 'A'))
            } else {
                workers[freeWorkerIndex] = next to (currentTime + 61 + (next[0] - 'A'))
            }
        }
    }

    return workers.mapNotNull { it?.second }.maxOrNull() ?: currentTime
}
