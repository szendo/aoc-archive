package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    fun react(s: String): String {
        val result = StringBuilder()
        var skipNext = false
        for (i in 0 until s.length) {
            if (skipNext) {
                skipNext = false
                continue
            }

            if (i == s.length - 1 || s[i] == s[i + 1] || s[i].lowercaseChar() != s[i + 1].lowercaseChar()) {
                result.append(s[i])
            } else {
                skipNext = true
            }
        }
        return result.toString()
    }

    fun fullyReact(p: String): Int {
        var polymer = p
        while (true) {
            val react = react(polymer)
            if (polymer == react) {
                break
            }
            polymer = react
        }
        return polymer.length
    }

    val input = Helper.getResourceAsStream("2018/5.txt").bufferedReader().readText().trim()

    println("Answer 1: ${fullyReact(input)}")

    var char = 'a'
    var len = Int.MAX_VALUE
    for (c in 'a'..'z') {
        val l = fullyReact(input.replace(c.toString(), "", true))
        if (l < len) {
            char = c
            len = l
        }
    }

    println("Answer 2: $len ($char)")


}

