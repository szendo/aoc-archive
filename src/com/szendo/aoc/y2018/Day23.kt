package com.szendo.aoc.y2018

import com.szendo.aoc.*
import kotlin.math.abs

fun main() {
    val regex = Regex("-?\\d+")

    val input = Helper.getResourceAsStream("2018/23.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { line ->
                val (x, y, z, r) = regex.findAll(line).map { it.value.toInt() }.toList()
                NanobotD23(Coord3D(x, y, z), r)
            }.toList()
        }

    val strongest = input.maxByOrNull { it.r }!!

    val answer1 = input.count { strongest.r >= strongest.coord.distance(it.coord) }
    println("Answer 1: $answer1")

    val friendliest = input.maxByOrNull { bot ->
        input.count { it.r >= bot.coord.distance(it.coord) }
    }!!

    var a2Coord = friendliest.coord
    var a2Count = input.count { it.r >= friendliest.coord.distance(it.coord) }
    var a2Dist = Coord3D(0, 0, 0).distance(a2Coord)
    println("(1ST) Answer 2: $a2Coord, $a2Count, $a2Dist")

    for (rx in 1..(friendliest.r * 100)) {
        for (dx in -rx..rx) {
            val ry = rx - abs(dx)
            for (dy in -ry..ry) {
                (ry - dy).let { dz ->
                    val coord = Coord3D(a2Coord.x + dx, a2Coord.y + dy, a2Coord.z + dz)
                    val count = input.count { it.r >= it.coord.distance(coord) }
                    val dist = Coord3D(0, 0, 0).distance(coord)
                    if (count > a2Count || count == a2Count && dist < a2Dist) {
                        a2Coord = coord
                        a2Count = count
                        a2Dist = dist
                        println("(NEW) Answer 2: $a2Dist")
                    }
                }
            }
        }
    }


}

data class NanobotD23(val coord: Coord3D, val r: Int)