package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    val regex = Regex("#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)")

    val input: List<Rect> = Helper.getResourceAsStream("2018/3.txt")
        .bufferedReader()
        .useLines {
            it.mapNotNull { line ->
                regex.matchEntire(line)?.let { matchResult ->
                    Rect(
                        matchResult.groupValues[1].toInt(),
                        matchResult.groupValues[2].toInt(),
                        matchResult.groupValues[3].toInt(),
                        matchResult.groupValues[4].toInt(),
                        matchResult.groupValues[5].toInt()
                    )
                }
            }.toList()
        }

    val minX = input.minOf { it.x }
    val minY = input.minOf { it.y }
    val maxX = input.maxOf { it.x + it.w }
    val maxY = input.maxOf { it.y + it.h }

    println(minX)
    println(maxX)
    println(minY)
    println(maxY)

    var answer1 = 0
    for (x in minX..maxX) {
        for (y in minY..maxY) {
            if (input.count { x in it.xRange && y in it.yRange } > 1) {
                answer1++
            }
        }
    }

    println("Answer 1: $answer1")


}

data class Rect(
    val id: Int,
    val x: Int,
    val y: Int,
    val w: Int,
    val h: Int
) {
    val xRange: IntRange
        get() = x until (x + w)
    val yRange: IntRange
        get() = y until (y + h)
}