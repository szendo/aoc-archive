package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    val regex = Regex("#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)")

    val input: List<Rect> = Helper.getResourceAsStream("2018/3.txt")
        .bufferedReader()
        .useLines {
            it.mapNotNull { line ->
                regex.matchEntire(line)?.let { matchResult ->
                    Rect(
                        matchResult.groupValues[1].toInt(),
                        matchResult.groupValues[2].toInt(),
                        matchResult.groupValues[3].toInt(),
                        matchResult.groupValues[4].toInt(),
                        matchResult.groupValues[5].toInt()
                    )
                }
            }.toList()
        }

    for (claim1 in input) {
        if (input.count { claim2 ->
                claim1.id != claim2.id
                        && claim1.xRange overlaps claim2.xRange
                        && claim1.yRange overlaps claim2.yRange
            } == 0) {
            println("Answer 2: ${claim1.id}")
            return
        }
    }

}

infix fun <T : Comparable<T>> ClosedRange<T>.overlaps(other: ClosedRange<T>): Boolean {
    return (this.start >= other.start && this.start <= other.endInclusive
            || other.start >= this.start && other.start <= this.endInclusive)
}
