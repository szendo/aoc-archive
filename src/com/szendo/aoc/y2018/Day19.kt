package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2018/19.txt")
        .bufferedReader()
        .useLines { it.toList() }

    val ops = mapOf<String, Op>(
        "addr" to { i1, i2, r -> r[i1] + r[i2] },
        "addi" to { i1, i2, r -> r[i1] + i2 },
        "mulr" to { i1, i2, r -> r[i1] * r[i2] },
        "muli" to { i1, i2, r -> r[i1] * i2 },
        "banr" to { i1, i2, r -> r[i1] and r[i2] },
        "bani" to { i1, i2, r -> r[i1] and i2 },
        "borr" to { i1, i2, r -> r[i1] or r[i2] },
        "bori" to { i1, i2, r -> r[i1] or i2 },
        "setr" to { i1, _, r -> r[i1] },
        "seti" to { i1, _, _ -> i1 },
        "gtir" to { i1, i2, r -> if (i1 > r[i2]) 1 else 0 },
        "gtri" to { i1, i2, r -> if (r[i1] > i2) 1 else 0 },
        "gtrr" to { i1, i2, r -> if (r[i1] > r[i2]) 1 else 0 },
        "eqir" to { i1, i2, r -> if (i1 == r[i2]) 1 else 0 },
        "eqri" to { i1, i2, r -> if (r[i1] == i2) 1 else 0 },
        "eqrr" to { i1, i2, r -> if (r[i1] == r[i2]) 1 else 0 }
    )

    var registers = List(6) { 0 }
    val instructions = input.drop(1)
    val ip = input.first().split(' ')[1].toInt()
    var rp: Int = -1

    while (registers[ip] != 1) {
        val (opcode, a, b, c) = instructions.getOrNull(registers[ip])?.split(' ') ?: break
        if (registers[ip] == 17) {
            rp = c.toInt()
        }
        registers = eval(a.toInt(), b.toInt(), c.toInt(), registers, ops[opcode]!!)
        registers = registers.mapIndexed { index, i -> if (index == ip) (i + 1) else i }
    }

    val answer1 = registers[rp].let { k -> (1..k).filter { k % it == 0 }.sum() }
    println("Answer 1: $answer1")

    registers = List(6) { if (it == 0) 1 else 0 }
    while (registers[ip] != 1) {
        val (opcode, a, b, c) = instructions.getOrNull(registers[ip])?.split(' ') ?: break
        registers = eval(a.toInt(), b.toInt(), c.toInt(), registers, ops[opcode]!!)
        registers = registers.mapIndexed { index, i -> if (index == ip) (i + 1) else i }
    }

    val answer2 = registers[rp].let { k -> (1..k).filter { k % it == 0 }.sum() }
    println("Answer 2: $answer2")
}

typealias Op = (Int, Int, List<Int>) -> Int

fun eval(i1: Int, i2: Int, o: Int, registers: List<Int>, op: Op): List<Int> {
    return registers.mapIndexed { index, value -> if (o == index) op(i1, i2, registers) else value }
}