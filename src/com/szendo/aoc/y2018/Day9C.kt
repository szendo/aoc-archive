package com.szendo.aoc.y2018

fun main() {
    val players = 468
    val lastMarble = 71843

    var time = System.currentTimeMillis()
    println("Answer 1: " + calculate(players, lastMarble))
    println("Time: ${System.currentTimeMillis() - time}ms")
    time = System.currentTimeMillis()
    println("Answer 2: " + calculate(players, lastMarble * 100))
    println("Time: ${System.currentTimeMillis() - time}ms")
}

private fun calculate(players: Int, lastMarble: Int): Long {
    class Dll {
        val value: Long
        var next: Dll
        var prev: Dll

        constructor(value: Long) {
            this.value = value
            this.next = this
            this.prev = this
        }

        constructor(value: Long, next: Dll, prev: Dll) {
            this.value = value
            this.next = next
            this.prev = prev
        }
    }

    val scores = MutableList(players) { 0L }
    var currentMarble = Dll(0)

    for (m in 1..lastMarble) {
        if (m % 23 == 0) {
            val marbleToRemove = currentMarble.prev.prev.prev.prev.prev.prev.prev
            scores[m % scores.size] += m + marbleToRemove.value
            marbleToRemove.prev.next = marbleToRemove.next
            marbleToRemove.next.prev = marbleToRemove.prev
            currentMarble = marbleToRemove.next

        } else {
            val insertPos = currentMarble.next
            val newMarble = Dll(m.toLong(), insertPos.next, insertPos)
            insertPos.next.prev = newMarble
            insertPos.next = newMarble
            currentMarble = newMarble
        }
    }

    return scores.maxOf { it }
}