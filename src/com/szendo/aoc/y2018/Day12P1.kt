package com.szendo.aoc.y2018

fun main() {
    val initialState =
        "#.#.#...#..##..###.##.#...#.##.#....#..#.#....##.#.##...###.#...#######.....##.###.####.#....#.#..##"
    val rules = mapOf(
        "#...#" to "#",
        "....#" to ".",
        "##..#" to "#",
        ".#.##" to "#",
        "##.##" to ".",
        "###.#" to "#",
        "....." to ".",
        "...#." to ".",
        ".#.#." to "#",
        "#.##." to "#",
        "..#.#" to "#",
        ".#..." to "#",
        "#.#.." to ".",
        "##.#." to ".",
        ".##.." to "#",
        "#..#." to ".",
        ".###." to ".",
        "..#.." to ".",
        "#.###" to ".",
        "..##." to ".",
        ".#..#" to "#",
        ".##.#" to ".",
        ".####" to ".",
        "...##" to "#",
        "#.#.#" to "#",
        "..###" to ".",
        "#..##" to ".",
        "####." to "#",
        "#####" to ".",
        "###.." to "#",
        "##..." to "#",
        "#...." to "."
    )

    var state = initialState.mapIndexedNotNull { index, c -> if (c == '#') index else null }.toSet()
    var minIndex = state.minOrNull() ?: 0
    var maxIndex = state.maxOrNull() ?: 0

    repeat(20) {
        state = ((minIndex - 2)..(maxIndex + 2)).mapNotNull { i ->
            if (rules[(-2..2).joinToString("") { off -> if (state.contains(i + off)) "#" else "." }] == "#") i else null
        }.toSet()
        minIndex = state.minOrNull() ?: 0
        maxIndex = state.maxOrNull() ?: 0
    }

    val answer1 = state.sum()
    println("Answer 1: $answer1")

}