package com.szendo.aoc.y2018;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day1 {

    public static void main(String[] args) throws Exception {

        List<Long> input = getInput();
        Long answer1 = null, answer2 = null;

        Set<Long> reachedFrequencies = new HashSet<>();
        reachedFrequencies.add(0L);

        long currentFreq = 0;
        while (answer1 == null || answer2 == null) {
            for (long freqChange : input) {
                currentFreq += freqChange;

                if (answer2 == null && !reachedFrequencies.add(currentFreq)) {
                    answer2 = currentFreq;
                    System.out.println("Answer 2: " + answer2);
                }
            }
            if (answer1 == null) {
                answer1 = currentFreq;
                System.out.println("Answer 1: " + answer1);
            }
        }
    }

    private static List<Long> getInput() throws IOException {
        List<Long> input = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(Day1.class.getResourceAsStream("1.txt"), StandardCharsets.UTF_8))) {
            String line;
            while (null != (line = reader.readLine())) {
                input.add(Long.parseLong(line));
            }
        }
        return input;
    }
}
