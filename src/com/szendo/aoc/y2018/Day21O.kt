package com.szendo.aoc.y2018

import com.szendo.aoc.Helper
import com.szendo.aoc.y2018.day16.Op
import com.szendo.aoc.y2018.day16.eval

fun main() {
    val input = Helper.getResourceAsStream("2018/21.txt")
        .bufferedReader()
        .useLines { it.toList() }

    val ops = mapOf<String, Op>(
        "addr" to { i1, i2, r -> r[i1] + r[i2] },
        "addi" to { i1, i2, r -> r[i1] + i2 },
        "mulr" to { i1, i2, r -> r[i1] * r[i2] },
        "muli" to { i1, i2, r -> r[i1] * i2 },
        "banr" to { i1, i2, r -> r[i1] and r[i2] },
        "bani" to { i1, i2, r -> r[i1] and i2 },
        "borr" to { i1, i2, r -> r[i1] or r[i2] },
        "bori" to { i1, i2, r -> r[i1] or i2 },
        "setr" to { i1, _, r -> r[i1] },
        "seti" to { i1, _, _ -> i1 },
        "gtir" to { i1, i2, r -> if (i1 > r[i2]) 1 else 0 },
        "gtri" to { i1, i2, r -> if (r[i1] > i2) 1 else 0 },
        "gtrr" to { i1, i2, r -> if (r[i1] > r[i2]) 1 else 0 },
        "eqir" to { i1, i2, r -> if (i1 == r[i2]) 1 else 0 },
        "eqri" to { i1, i2, r -> if (r[i1] == i2) 1 else 0 },
        "eqrr" to { i1, i2, r -> if (r[i1] == r[i2]) 1 else 0 }
    )

    var registers = List(6) { 0 }
    val instructions = input.drop(1)
    val ip = input.first().split(' ')[1].toInt()

    var reg0 = 0
    while (true) {
        reg0++
        registers = List(6) { if (it == 0) reg0 else 0 }
        while (true) {
            val ipx = registers[ip]
            val (opcode, a, b, c) = instructions.getOrNull(ipx)?.split(' ') ?: break
            registers = eval(a.toInt(), b.toInt(), c.toInt(), registers, ops[opcode]!!)
            registers = registers.mapIndexed { index, i -> if (index == ip) (i + 1) else i }
        }
    }

}
