package com.szendo.aoc.y2018

import com.szendo.aoc.Helper

fun main() {
    val input = Helper.getResourceAsStream("2018/18.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { it.toList() }.toList()
        }

    val seenStates = mutableSetOf(input)
    var firstRepeatingState: List<List<Char>>? = null
    var firstRepeatAt = 0L

    // open ground (.), trees (|), or a lumberyard (#)
    var state = input
    var step = 0L
    while (step < 1000000000) {
        step++
        state = state.mapIndexed { y, row ->
            row.mapIndexed { x, c ->
                when (c) {
                    '.' -> {
                        val t =
                            (-1..1).sumOf { dy -> (-1..1).count { dx -> state.getOrNull(y + dy)?.getOrNull(x + dx) == '|' } }
                        if (t >= 3) '|' else '.'
                    }

                    '|' -> {
                        val l =
                            (-1..1).sumOf { dy -> (-1..1).count { dx -> state.getOrNull(y + dy)?.getOrNull(x + dx) == '#' } }
                        if (l >= 3) '#' else '|'
                    }

                    '#' -> {
                        val l =
                            (-1..1).sumOf { dy -> (-1..1).count { dx -> state.getOrNull(y + dy)?.getOrNull(x + dx) == '#' } }
                        val t =
                            (-1..1).sumOf { dy -> (-1..1).count { dx -> state.getOrNull(y + dy)?.getOrNull(x + dx) == '|' } }
                        if (l >= 2 && t >= 1) '#' else '.'
                    }

                    else -> throw RuntimeException()
                }
            }
        }

        if (step == 10L) {
            val answer1 =
                state.sumOf { row -> row.count { it == '#' } } * state.sumOf { row -> row.count { it == '|' } }
            println("Answer 1: $answer1")
        }

        if (firstRepeatingState != null) {
            if (firstRepeatingState == state) {
                val cycleLength = step - firstRepeatAt
                step += ((1000000000 - step) / cycleLength) * cycleLength
            }

        } else if (!seenStates.add(state)) {
            firstRepeatingState = state
            firstRepeatAt = step
        }
    }

    val answer2 = state.sumOf { row -> row.count { it == '#' } } * state.sumOf { row -> row.count { it == '|' } }
    println("Answer 2: $answer2")
}