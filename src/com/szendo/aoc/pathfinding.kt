package com.szendo.aoc

import java.util.*

inline fun <S> shortestPathBfs(
    source: S, target: S,
    crossinline getNeighbors: (s: S) -> Set<S>,
): Int {
    val queue = LinkedList(listOf(source to 0))
    val visited = mutableSetOf<S>()
    while (queue.isNotEmpty()) {
        val (state, cost) = queue.removeFirst()
        if (state == target) return cost
        if (visited.add(state)) {
            getNeighbors(state).forEach {
                queue.add(it to (cost + 1))
            }
        }
    }
    error("No path to target")
}

inline fun <S> shortestPathBfs(
    source: S,
    crossinline isTarget: S.() -> Boolean,
    crossinline getNeighbors: (s: S) -> Set<S>,
): Int {
    val queue = LinkedList(listOf(source to 0))
    val visited = mutableSetOf<S>()
    while (queue.isNotEmpty()) {
        val (state, cost) = queue.removeFirst()
        if (state.isTarget()) return cost
        if (visited.add(state)) {
            getNeighbors(state).forEach {
                queue.add(it to (cost + 1))
            }
        }
    }
    error("No path to target")
}

inline fun <S> shortestPathDijkstra(
    source: S, target: S,
    crossinline getNeighbors: (s: S) -> Map<S, Int>,
): Int {
    val queue = PriorityQueue<Pair<S, Int>>(Comparator.comparingInt { it.second })
        .apply { add(source to 0) }
    val visited = mutableMapOf<S, Int>()
    while (queue.isNotEmpty()) {
        val (state, cost) = queue.remove()
        if (state == target) return cost
        if (state in visited && visited.getValue(state) <= cost) continue
        visited[state] = cost
        getNeighbors(state).forEach { (nextState, stepCost) ->
            queue.add(nextState to (cost + stepCost))
        }
    }
    error("No path to target")
}

inline fun <S> shortestPathDijkstra(
    source: S,
    crossinline isTarget: S.() -> Boolean,
    crossinline getNeighbors: (s: S) -> Map<S, Int>,
): Int {
    val queue = PriorityQueue<Pair<S, Int>>(Comparator.comparingInt { it.second })
        .apply { add(source to 0) }
    val visited = mutableMapOf<S, Int>()
    while (queue.isNotEmpty()) {
        val (state, cost) = queue.remove()
        if (state.isTarget()) return cost
        if (state in visited && visited.getValue(state) <= cost) continue
        visited[state] = cost
        getNeighbors(state).forEach { (nextState, stepCost) ->
            queue.add(nextState to (cost + stepCost))
        }
    }
    error("No path to target")
}
