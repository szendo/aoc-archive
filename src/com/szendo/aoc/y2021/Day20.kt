package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/20.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .split("")

    val algorithm = input[0][0].map { it == '#' }
    val image = input[1].flatMapIndexed { y, row ->
        row.mapIndexed { x, c -> (x to y) to (c == '#') }.filter { it.second }.map { it.first }
    }.toSet()

    fun enhance(algorithm: List<Boolean>, image: Set<Pair<Int, Int>>, litBase: Boolean): Set<Pair<Int, Int>> {
        val minX = image.minOf { it.x } - 1
        val maxX = image.maxOf { it.x } + 1
        val minY = image.minOf { it.y } - 1
        val maxY = image.maxOf { it.y } + 1

        val grid = if (litBase && algorithm[0]) {
            image +
                (minX - 1..maxX + 1).flatMap { x -> listOf(x to minY, x to maxY, x to minY - 1, x to maxY + 1) } +
                (minY - 1..maxY + 1).flatMap { y -> listOf(minX to y, maxX to y, minX - 1 to y, maxX + 1 to y) }
        } else image

        return (minY..maxY).flatMap { y ->
            (minX..maxX).map { x ->
                (x to y) to algorithm[
                    (-1..1).flatMap { dy -> (-1..1).map { dx -> if ((x + dx to y + dy) in grid) 1 else 0 } }
                        .reduce { acc, bit -> 2 * acc + bit }
                ]
            }.filter { it.second }.map { it.first }
        }.toSet()
    }

    val answer1 = run {
        (1..2).fold(image) { grid, n -> enhance(algorithm, grid, n % 2 == 0) }.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        (1..50).fold(image) { grid, n -> enhance(algorithm, grid, n % 2 == 0) }.size
    }
    println("Part 2: $answer2")
}
