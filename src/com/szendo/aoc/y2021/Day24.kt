package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/24.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.split(" ") }

    fun parseParam(param: String, variables: List<Long>): Long =
        if (param[0] in "wxyz") variables[param[0] - 'w'] else param.toLong()

    fun execute(program: List<List<String>>, inputs: List<Long>): List<Long> {
        val variables = mutableListOf(0L, 0L, 0L, 0L)
        val inputQueue = inputs.toMutableList()

        for (instr in program) {
            when (instr[0]) {
                "inp" -> variables[instr[1][0] - 'w'] = inputQueue.removeFirst()
                "add" -> variables[instr[1][0] - 'w'] += parseParam(instr[2], variables)
                "mul" -> variables[instr[1][0] - 'w'] *= parseParam(instr[2], variables)
                "div" -> variables[instr[1][0] - 'w'] /= parseParam(instr[2], variables)
                "mod" -> variables[instr[1][0] - 'w'] %= parseParam(instr[2], variables)
                "eql" -> variables[instr[1][0] - 'w'] =
                    if (variables[instr[1][0] - 'w'] == parseParam(instr[2], variables)) 1L else 0L
                else -> error("Invalid instruction: $instr")
            }
        }

        return variables
    }

    fun executeOptimized(inputs: List<Long>, consts: List<Triple<Int, Int, Int>>) =
        inputs.indices.fold(0L) { z, i ->
            // w = inputs[i] // inp w
            // x = z % 26 // mul x 0; add x z; mod x 26
            // z /= consts[i].first // div z A
            // x += consts[i].second // add x B
            // x = if (x == w) 0 else 1 // eql x w; eql x 0
            // y = 25 * x + 1 // mul y 0; add y 25; mul y x; add y 1
            // z *= y // mul z y
            // y = (w + consts[i].third) * x // mul y 0; add y w; add y C; mul y x
            // z += y // add z y

            val w = inputs[i]
            val (a, b, c) = consts[i]
            if (z % 26 + b != w) {
                26 * (z / a) + w + c
            } else {
                z / a
            }
        }

    fun checkModelNumber(modelNumber: String, consts: List<Triple<Int, Int, Int>>): String {
        val inputs = modelNumber.toList().map { (it - '0').toLong() }
        check(execute(input, inputs).last() == 0L)
        check(executeOptimized(inputs, consts) == 0L)
        return modelNumber
    }

    val consts = input.chunked(18).map { Triple(it[4][2].toInt(), it[5][2].toInt(), it[15][2].toInt()) }

    val answer1 = run {
        val modelNumber = consts.foldIndexed(listOf<Pair<Int, Int>>() to "xxxxxxxxxxxxxx") { i, (stack, modelNumber), (a, b, c) ->
            if (a == 1) {
                (stack + (i to c)) to modelNumber
            } else {
                val (i0, c0) = stack.last()
                val diff = c0 + b
                check(diff in -8..8)
                stack.dropLast(1) to modelNumber.mapIndexed { mi, d ->
                    when (mi) {
                        i0 -> if (diff > 0) (9 - diff).digitToChar() else '9'
                        i -> if (diff < 0) (9 + diff).digitToChar() else '9'
                        else -> d
                    }
                }.joinToString("")
            }
        }.second

        checkModelNumber(modelNumber, consts)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val modelNumber = consts.foldIndexed(listOf<Pair<Int, Int>>() to "xxxxxxxxxxxxxx") { i, (stack, modelNumber), (a, b, c) ->
            if (a == 1) {
                (stack + (i to c)) to modelNumber
            } else {
                val (i0, c0) = stack.last()
                val diff = c0 + b
                check(diff in -8..8)
                stack.dropLast(1) to modelNumber.mapIndexed { mi, d ->
                    when (mi) {
                        i0 -> if (diff < 0) (1 - diff).digitToChar() else '1'
                        i -> if (diff > 0) (1 + diff).digitToChar() else '1'
                        else -> d
                    }
                }.joinToString("")
            }
        }.second

        checkModelNumber(modelNumber, consts)
    }
    println("Part 2: $answer2")
}
