package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/13.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .split("").take(2)

    val dots = input[0].map { it.split(',', limit = 2) }.map { it[0].toInt() to it[1].toInt() }
    val folds = input[1].map { it.substring(11) }.map {
        when (it[0]) {
            'x' -> it.substring(2).toInt() to Int.MAX_VALUE
            'y' -> Int.MAX_VALUE to it.substring(2).toInt()
            else -> error("Invalid axis: ${it[0]}")
        }
    }

    fun foldAlongLine(dots: Set<Pair<Int, Int>>, line: Pair<Int, Int>): Set<Pair<Int, Int>> = dots.map { c ->
        (if (c.x > line.x) (2 * line.x - c.x) else c.x) to (if (c.y > line.y) (2 * line.y - c.y) else c.y)
    }.toSet()

    val answer1 = run {
        folds.take(1).fold(dots.toSet(), ::foldAlongLine).size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val finalDots = folds.fold(dots.toSet(), ::foldAlongLine)
        (0..finalDots.maxOf { it.y }).joinToString("\n") { y ->
            (0..finalDots.maxOf { it.x }).joinToString("") { x ->
                if (finalDots.contains(x to y)) "#" else " "
            }
        }
    }
    println("Part 2:\n$answer2")
}
