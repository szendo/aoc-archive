package com.szendo.aoc.y2021

import com.szendo.aoc.*
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

fun main() {
    val input = Helper.getResourceAsStream("2021/20.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .split("")

    val algorithm = input[0][0].map { it == '#' }
    val image = input[1].flatMapIndexed { y, row ->
        row.mapIndexed { x, c -> (x to y) to (c == '#') }.filter { it.second }.map { it.first }
    }.toSet()

    fun render(grid: Set<Pair<Int, Int>>, value: Boolean, n: Int) {
        val i = BufferedImage(250, 250, BufferedImage.TYPE_INT_RGB)
        (0 until i.width).forEach { x ->
            (0 until i.height).forEach { y ->
                i.setRGB(
                    x, y,
                    if (((x - 75) to (y - 75) !in grid) == value) 0 else 0xffffff
                )
            }
        }
        println("rendering $n")
        ImageIO.write(i, "png", File("stuff/2021/20/$n.png"))
    }

    fun enhance(algorithm: List<Boolean>, grid: Set<Coord>, value: Boolean): Pair<Set<Coord>, Boolean> {
        val nextValue = !algorithm[if (value) 0 else 511]

        return grid.flatMap { (x, y) ->
            (-1..1).flatMap { ny ->
                (-1..1).map { nx ->
                    (x + nx to y + ny)
                }
            }
        }.toSet()
            .map { (x, y) ->
                (x to y) to algorithm[
                    (-1..1).flatMap { dy -> (-1..1).map { dx -> if ((x + dx to y + dy) in grid == value) 1 else 0 } }
                        .reduce { acc, bit -> 2 * acc + bit }
                ]

            }
            .filter { it.second == nextValue }.map { it.first }.toSet() to nextValue
    }

    run {
        render(image, true, 0)
        (1..50).fold(image to true) { (grid, v), n ->
            val (enhanced, nv) = enhance(algorithm, grid, v)
            render(enhanced, nv, n)
            enhanced to nv
        }
    }
}
