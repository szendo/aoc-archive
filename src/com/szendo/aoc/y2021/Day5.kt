package com.szendo.aoc.y2021

import com.szendo.aoc.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sign

fun main() {
    val input = Helper.getResourceAsStream("2021/5.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { row -> row.split(" -> ").map { it.split(",").map { n -> n.toInt() } }.map { it[0] to it[1] } }
        .map { it[0] to it[1] }

    val answer1 = run {
        val floor = Array(1000 * 1000) { 0 }
        for ((s, e) in input) {
            if (s.x == e.x) {
                for (y in (min(s.y, e.y))..max(s.y, e.y)) {
                    floor[1000 * s.x + y]++
                }
            } else if (s.y == e.y) {
                for (x in (min(s.x, e.x))..max(s.x, e.x)) {
                    floor[1000 * x + s.y]++
                }
            }
        }

        floor.count { it > 1 }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val floor = Array(1000 * 1000) { 0 }
        for ((s, e) in input) {
            val d = (e.x - s.x).sign to (e.y - s.y).sign
            var p = s
            while (p != e) {
                floor[1000 * p.x + p.y]++
                p += d
            }
            floor[1000 * p.x + p.y]++
        }

        floor.count { it > 1 }
    }
    println("Part 2: $answer2")
}
