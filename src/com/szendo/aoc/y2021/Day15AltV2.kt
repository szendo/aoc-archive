package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/15.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { row -> row.toList().map { it.digitToInt() } }

    fun shortestPath(grid: Grid<Int>, repeatCount: Int = 1): Int {
        val height = grid.size
        val width = grid[0].size
        val startPos = 0 to 0
        val targetPos = (repeatCount * width - 1) to (repeatCount * height - 1)
        return shortestPathDijkstra(
            source = startPos, target = targetPos,
            getNeighbors = { pos ->
                setOf(1 to 0, 0 to 1, 0 to -1, -1 to 0).map { pos + it }
                    .filter { (x, y) -> x in 0 until (width * repeatCount) && y in 0 until (height * repeatCount) }
                    .associateWith { (x, y) -> (grid[y % height][x % width] + x / width + y / height - 1) % 9 + 1 }
            }
        )
    }

    val answer1 = run {
        shortestPath(input)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        shortestPath(input, repeatCount = 5)
    }
    println("Part 2: $answer2")
}
