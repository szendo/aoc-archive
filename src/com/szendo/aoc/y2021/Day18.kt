package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/18.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { SnailfishNumber.parse(it) }

    val answer1 = run {
        input.reduce { acc, n -> acc + n }.magnitude()
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.flatMap { a -> input.map { b -> a to b } }
            .filter { it.first !== it.second }
            .maxOf { (it.first + it.second).magnitude() }
    }
    println("Part 2: $answer2")
}

sealed class SnailfishNumber(private val depth: Int) {
    class P(var left: SnailfishNumber, var right: SnailfishNumber, depth: Int) : SnailfishNumber(depth)
    class N(var value: Int, depth: Int) : SnailfishNumber(depth)

    companion object {
        private fun parse(s: String, pos: Int, depth: Int): Pair<SnailfishNumber, Int> =
            if (s[pos] == '[') {
                val (left, postAfterLeft) = parse(s, pos + 1, depth + 1)
                assert(s[postAfterLeft] == ',')
                val (right, posAfterRight) = parse(s, postAfterLeft + 1, depth + 1)
                assert(s[posAfterRight] == ']')
                P(left, right, depth) to posAfterRight + 1
            } else {
                N(s[pos].digitToInt(), depth) to pos + 1
            }

        fun parse(s: String): SnailfishNumber = parse(s, 0, 0).first
    }

    private fun explode(path: List<Pair<P, Dir>> = emptyList()): Boolean {
        when (this) {
            is P -> {
                val left = this.left
                val right = this.right
                if (left.explode(path + (this to Dir.LEFT))) return true

                if (this.depth >= 4 && left is N && right is N) {
                    path.last().let {
                        when (it.second) {
                            Dir.LEFT -> it.first.left = N(0, this.depth)
                            Dir.RIGHT -> it.first.right = N(0, this.depth)
                        }
                    }

                    path.lastOrNull {
                        it.second == Dir.RIGHT
                    }?.let {
                        var leftTarget = it.first.left
                        while (leftTarget is P) {
                            leftTarget = leftTarget.right
                        }

                        (leftTarget as N).value += left.value
                    }

                    path.lastOrNull {
                        it.second == Dir.LEFT
                    }?.let {
                        var rightTarget = it.first.right
                        while (rightTarget is P) {
                            rightTarget = rightTarget.left
                        }

                        (rightTarget as N).value += right.value
                    }
                    return true
                }

                if (right.explode(path + (this to Dir.RIGHT))) return true
                return false
            }
            else -> {
                return false
            }
        }
    }

    private fun split(path: List<Pair<P, Dir>> = emptyList()): Boolean {
        when (this) {
            is N -> {
                if (this.value >= 10) {
                    val left = N(this.value / 2, this.depth + 1)
                    val right = N((this.value + 1) / 2, this.depth + 1)
                    val pair = P(left, right, this.depth)

                    path.last().let {
                        when (it.second) {
                            Dir.LEFT -> it.first.left = pair
                            Dir.RIGHT -> it.first.right = pair
                        }
                    }
                    return true
                }
                return false
            }
            is P -> {
                val left = this.left
                val right = this.right
                if (left.split(path + (this to Dir.LEFT))) return true
                if (right.split(path + (this to Dir.RIGHT))) return true
                return false
            }
            else -> {
                error("Invalid sailfish number: $this")
            }
        }
    }

    protected fun reduce() {
        do {
            val exploded = explode()
            val split = if (!exploded) split() else false
        } while (exploded || split)
    }

    private fun withIncreasedDepth(): SnailfishNumber =
        when (this) {
            is N -> N(this.value, this.depth + 1)
            is P -> P(this.left.withIncreasedDepth(), this.right.withIncreasedDepth(), this.depth + 1)
        }

    operator fun plus(other: SnailfishNumber): SnailfishNumber {
        return P(withIncreasedDepth(), other.withIncreasedDepth(), 0).apply { this.reduce() }
    }

    fun magnitude(): Int =
        when (this) {
            is N -> value
            is P -> (3 * left.magnitude() + 2 * right.magnitude())
        }

    enum class Dir { LEFT, RIGHT }
}
