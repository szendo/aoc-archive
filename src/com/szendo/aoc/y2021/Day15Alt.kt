package com.szendo.aoc.y2021

import com.szendo.aoc.*
import java.util.*

fun main() {
    val input = Helper.getResourceAsStream("2021/15.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { row -> row.toList().map { it.digitToInt() } }

    fun shortestPath(grid: Grid<Int>, repeatCount: Int = 1): Int {
        val height = grid.size
        val width = grid[0].size
        val startPos = 0 to 0
        val endPos = (repeatCount * width - 1) to (repeatCount * height - 1)
        val seenPos = mutableMapOf<Coord, Int>()
        val queue = PriorityQueue<Pair<Coord, Int>>(Comparator.comparing { it.second })
        queue.add(startPos to 0)
        while (queue.isNotEmpty()) {
            val (pos, cost) = queue.remove()
            if (pos == endPos) return cost
            if (pos in seenPos && seenPos.getValue(pos) <= cost) continue
            seenPos[pos] = cost
            setOf(1 to 0, 0 to 1, 0 to -1, -1 to 0).forEach { delta ->
                val (x, y) = pos + delta
                if (x in 0 until (width * repeatCount) && y in 0 until (height * repeatCount)) {
                    val stepCost = (grid[y % height][x % width] + x / width + y / height - 1) % 9 + 1
                    queue.add((x to y) to (cost + stepCost))
                }
            }
        }
        error("No path")
    }

    val answer1 = run {
        shortestPath(input)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        shortestPath(input, repeatCount = 5)
    }
    println("Part 2: $answer2")
}
