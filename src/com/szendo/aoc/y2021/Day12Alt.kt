package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/12.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { row -> row.split('-', limit = 2) }
        .flatMap { listOf(it[0] to it[1], it[1] to it[0]) }
        .groupBy({ it.first }) { it.second }

    fun countPaths(input: Map<String, List<String>>, allowSingleDoubleVisit: Boolean): Int {
        var totalCount = 0
        var queue = mapOf(Triple("start", setOf("start"), !allowSingleDoubleVisit) to 1)
        while (queue.isNotEmpty()) {
            val nextQueue = mutableMapOf<Triple<String, Set<String>, Boolean>, Int>()
            for ((key, count) in queue) {
                val (last, visited, twice) = key
                if (last == "end") {
                    totalCount += count
                } else {
                    for (next in input.getValue(last)) {
                        if (next[0].isUpperCase() || next !in visited) {
                            nextQueue.merge(Triple(next, visited + next, twice), count, Int::plus)
                        } else if (!twice && next != "start") {
                            nextQueue.merge(Triple(next, visited, true), count, Int::plus)
                        }
                    }
                }
            }
            queue = nextQueue
        }
        return totalCount
    }

    val answer1 = run {
        countPaths(input, false)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        countPaths(input, true)
    }
    println("Part 2: $answer2")
}
