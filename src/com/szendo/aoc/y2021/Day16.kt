package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/16.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .map {
            ("000" + it.digitToInt(16).toString(2)).takeLast(4)
        }.joinToString(separator = "")

    fun evaluate(type: Int, values: List<Long>): Long = when (type) {
        0 -> values.sumOf { it }
        1 -> values.reduce(Long::times)
        2 -> values.minOf { it }
        3 -> values.maxOf { it }
        5 -> if (values[0] > values[1]) 1 else 0
        6 -> if (values[0] < values[1]) 1 else 0
        7 -> if (values[0] == values[1]) 1 else 0
        else -> error("Invalid type: $type")
    }

    fun parsePacket(
        input: String,
        pos: Int,
        depth: Int,
        callback: (version: Int, type: Int, depth: Int, value: Long) -> Unit
    ): Pair<Long, Int> {
        val version = input[pos forLen 3].toInt(2)
        val type = input[pos + 3 forLen 3].toInt(2)

        if (type == 4) {
            var bits = ""
            var pos2 = pos + 6
            do {
                val hasNext = input[pos2].digitToInt(2) == 1
                bits += input[pos2 + 1 forLen 4]
                pos2 += 5
            } while (hasNext)

            val value = bits.trimStart('0').toLong(2)

            callback(version, type, depth, value)
            return value to pos2
        } else {
            val values = mutableListOf<Long>()

            val lengthTypeId = input[pos + 6].digitToInt(2)
            if (lengthTypeId == 0) {
                val totalLengthOfSubPackets = input[pos + 7 forLen 15].toInt(2)

                val subInput = input[pos + 22 forLen totalLengthOfSubPackets]
                var subPos = 0
                while (subPos < totalLengthOfSubPackets) {
                    val (value, newSubPos) = parsePacket(subInput, subPos, depth + 1, callback)
                    values += value
                    subPos = newSubPos
                }

                val value = evaluate(type, values)
                callback(version, type, depth, value)
                return value to pos + 22 + totalLengthOfSubPackets
            } else {
                val numberOfSubPackets = input[pos + 7 forLen 11].toInt(2)

                var subPos = pos + 18
                repeat(numberOfSubPackets) {
                    val (value, newSubPos) = parsePacket(input, subPos, depth + 1, callback)
                    values += value
                    subPos = newSubPos
                }

                val value = evaluate(type, values)
                callback(version, type, depth, value)
                return value to subPos
            }
        }
    }

    fun parsePacket(input: String, callback: (version: Int, type: Int, depth: Int, value: Long) -> Unit) =
        parsePacket(input, 0, 0, callback).first

    val answer1 = run {
        var versionSum = 0
        parsePacket(input) { version, _, _, _ ->
            versionSum += version
        }
        versionSum
    }
    println("Part 1: $answer1")

    val answer2 = run {
        parsePacket(input) { _, _, _, _ -> }
    }
    println("Part 2: $answer2")
}
