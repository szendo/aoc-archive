package com.szendo.aoc.y2021

import com.szendo.aoc.*
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO

fun main() {
    val input = Helper.getResourceAsStream("2021/15.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { row -> row.toList().map { it.digitToInt() } }

    fun calculateLowestTotalRisk(riskLevel: List<List<Int>>): Int {
        val totalRiskLevel = riskLevel.map { row ->
            row.map { 10000 }.toMutableList()
        }
        totalRiskLevel.last()[totalRiskLevel.last().lastIndex] = riskLevel.last()[riskLevel.lastIndex]

        val queue = LinkedList(listOf(totalRiskLevel.last().lastIndex to totalRiskLevel.lastIndex))
        while (queue.isNotEmpty()) {
            val (x, y) = queue.removeFirst()
            listOf((0 to 1), (0 to -1), (1 to 0), (-1 to 0)).forEach { (dx, dy) ->
                try {
                    if (totalRiskLevel[y][x] + riskLevel[y + dy][x + dx] < totalRiskLevel[y + dy][x + dx]) {
                        totalRiskLevel[y + dy][x + dx] = totalRiskLevel[y][x] + riskLevel[y + dy][x + dx]
                        queue.add(x + dx to y + dy)
                    }
                } catch (ignored: IndexOutOfBoundsException) {
                }
            }
        }

        val path = totalRiskLevel.map { row -> row.map { false }.toMutableList() }
        path[0][0] = true
        var coord = 0 to 0
        path[coord.y][coord.x] = true

        while (coord.x < totalRiskLevel[0].lastIndex || coord.y < totalRiskLevel.lastIndex) {
            val (x, y) = coord
            val nextRiskLevel = totalRiskLevel[y][x] - riskLevel[y][x]
            try {
                if (totalRiskLevel[y + 1][x] == nextRiskLevel) {
                    coord += (0 to 1)
                    path[coord.y][coord.x] = true
                }
            } catch (ignored: IndexOutOfBoundsException) {
            }
            try {
                if (totalRiskLevel[y - 1][x] == nextRiskLevel) {
                    coord += (0 to -1)
                    path[coord.y][coord.x] = true
                }
            } catch (ignored: IndexOutOfBoundsException) {
            }
            try {
                if (totalRiskLevel[y][x + 1] == nextRiskLevel) {
                    coord += (1 to 0)
                    path[coord.y][coord.x] = true
                }
            } catch (ignored: IndexOutOfBoundsException) {
            }
            try {
                if (totalRiskLevel[y][x - 1] == nextRiskLevel) {

                    coord += (-1 to 0)
                    path[coord.y][coord.x] = true
                }
            } catch (ignored: IndexOutOfBoundsException) {
            }
        }


        val img = BufferedImage(path[0].size, path.size, BufferedImage.TYPE_INT_RGB)
        path.forEachIndexed { y, row ->
            row.forEachIndexed { x, b ->
                val str = riskLevel[y][x] + 5
                img.setRGB(x, y, (if (b) 0x111100 else 0x000011) * str)
            }
        }
        ImageIO.write(img, "png", File("stuff/2021/15/part${if (img.width == 500) 2 else 1}.png"))

        return totalRiskLevel[0][0] - riskLevel[0][0]
    }

    val answer1 = run {
        calculateLowestTotalRisk(input)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        calculateLowestTotalRisk((0..4).flatMap { dy ->
            input.map { row ->
                (0..4).flatMap { dx ->
                    row.map { (it - 1 + dx + dy) % 9 + 1 }

                }
            }
        })
    }
    println("Part 2: $answer2")
}
