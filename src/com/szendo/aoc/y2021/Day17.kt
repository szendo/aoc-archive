package com.szendo.aoc.y2021

import com.szendo.aoc.*
import kotlin.math.sign

fun main() {
    val input = Helper.getResourceAsStream("2021/17.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .let { Regex("target area: x=(-?\\d+)..(-?\\d+), y=(-?\\d+)..(-?\\d+)").matchEntire(it)!! }
        .groupValues.drop(1).chunked(2).map { it[0].toInt()..it[1].toInt() }.let { it[0] to it[1] }

    val answer1 = run {
        val (xr, yr) = input

        var highestY = 0
        (xr.last downTo 1).forEach { velX ->
            (-yr.first downTo yr.first).forEach { velY ->
                var potentialHighestY = 0

                var pos = 0 to 0
                var vel = velX to velY

                do {
                    pos += vel
                    vel -= vel.x.sign to 1

                    if (pos.y > potentialHighestY) {
                        potentialHighestY = pos.y
                    }
                    if (pos.x in xr && pos.y in yr && potentialHighestY > highestY) {
                        highestY = potentialHighestY
                        break
                    }
                } while (pos.x < xr.last && pos.y > yr.first)
            }
        }

        highestY
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val (xr, yr) = input

        var count = 0
        (xr.last downTo 1).forEach { velX ->
            (-yr.first downTo yr.first).forEach { velY ->
                var pos = 0 to 0
                var vel = velX to velY

                do {
                    pos += vel
                    vel -= vel.x.sign to 1

                    if (pos.x in xr && pos.y in yr) {
                        count++
                        break
                    }
                } while (pos.x < xr.last && pos.y > yr.first)
            }
        }

        count
    }
    println("Part 2: $answer2")
}
