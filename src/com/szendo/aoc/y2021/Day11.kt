package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/11.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.toList().map { c -> c - '0' } }

    fun step(grid: Grid<Int>): Grid<Int> {
        val flashed = mutableSetOf<Coord>()
        val newGrid = grid.map { row -> row.map { it + 1 }.toMutableList() }

        val queue = (0..99)
            .filter { i ->
                newGrid[i / 10][i % 10] > 9
            }
            .map { (it / 10) to (it % 10) }
            .toMutableList()

        while (queue.isNotEmpty()) {
            val c = queue.removeFirst()
            if (c.x !in 0..9 || c.y !in 0..9) continue

            if (++newGrid[c.x][c.y] > 9) {
                if (c !in flashed) {
                    flashed.add(c)
                    queue.addAll(
                        listOf(
                            c + (-1 to -1),
                            c + (-1 to 0),
                            c + (-1 to 1),
                            c + (0 to -1),
                            c + (0 to 1),
                            c + (1 to -1),
                            c + (1 to 0),
                            c + (1 to 1)
                        )
                    )
                }
            }
        }

        (0..99).forEach { i ->
            if (newGrid[i / 10][i % 10] > 9) {
                newGrid[i / 10][i % 10] = 0
            }
        }

        return newGrid
    }

    val answer1 = run {
        var grid = input
        var flashes = 0L
        repeat(100) {
            grid = step(grid)
            flashes += grid.sumOf { row -> row.count { it == 0 } }
        }
        flashes
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var grid = input
        var t = 0L
        while (true) {
            t++
            grid = step(grid)
            if (grid.all { row -> row.all { it == 0 } }) {
                return@run t
            }
        }
    }
    println("Part 2: $answer2")
}
