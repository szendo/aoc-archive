package com.szendo.aoc.y2021

import com.szendo.aoc.*
import kotlin.math.max
import kotlin.math.min

fun main() {
    val input = Helper.getResourceAsStream("2021/22.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { line ->
            Regex("(on|off) x=(-?\\d+)\\.\\.(-?\\d+),y=(-?\\d+)\\.\\.(-?\\d+),z=(-?\\d+)\\.\\.(-?\\d+)")
                .matchEntire(line)!!.groupValues.drop(1).let {
                    (it[0] == "on") to Triple(
                        it[1].toInt()..it[2].toInt(),
                        it[3].toInt()..it[4].toInt(),
                        it[5].toInt()..it[6].toInt()
                    )
                }
        }

    fun IntRange.isWithin(r: IntRange) =
        this.first >= r.first && this.last <= r.last

    fun IntRange.intersects(r: IntRange) =
        this.first <= r.last && this.last >= r.first

    fun IntRange.intersect(r: IntRange): IntRange =
        max(this.first, r.first)..min(this.last, r.last)

    val answer1 = run {
        val cubes = mutableSetOf<Coord3D>()
        input.forEach { (on, dims) ->
            val (xr, yr, zr) = dims
            if (dims.toList().all { it.isWithin(-50..50) }) {
                xr.forEach { x ->
                    yr.forEach { y ->
                        zr.forEach { z ->
                            if (on) cubes.add(Coord3D(x, y, z))
                            else cubes.remove(Coord3D(x, y, z))
                        }
                    }
                }
            }
        }
        cubes.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val cubes = mutableListOf<Pair<Int, Triple<IntRange, IntRange, IntRange>>>()
        input.forEach { (on, dims) ->
            val (xr, yr, zr) = dims
            val newCubes = mutableListOf<Pair<Int, Triple<IntRange, IntRange, IntRange>>>()
            cubes.forEach { (n, dims2) ->
                val (xr2, yr2, zr2) = dims2
                if (xr.intersects(xr2) && yr.intersects(yr2) && zr.intersects(zr2)) {
                    newCubes.add(-n to Triple(xr.intersect(xr2), yr.intersect(yr2), zr.intersect(zr2)))
                }
            }
            cubes.addAll(newCubes)
            if (on) cubes.add(1 to dims)
        }

        cubes.fold(0L) { sum, (n, r) ->
            val (xr, yr, zr) = r
            sum + n.toLong() * (xr.last - xr.first + 1) * (yr.last - yr.first + 1) * (zr.last - zr.first + 1)
        }
    }
    println("Part 2: $answer2")
}
