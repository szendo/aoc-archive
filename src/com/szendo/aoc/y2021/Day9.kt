package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/9.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.toList().map { c -> c - '0' } }

    val answer1 = run {
        var risk = 0
        for (y in input.indices) {
            val row = input[y]
            for (x in row.indices) {
                val cell = row[x]
                val up = input.getOrNull(y - 1)?.get(x) ?: 9
                val down = input.getOrNull(y + 1)?.get(x) ?: 9
                val left = input[y].getOrElse(x - 1) { 9 }
                val right = input[y].getOrElse(x + 1) { 9 }

                if (cell < up && cell < down && cell < left && cell < right) {
                    risk += cell + 1
                }
            }
        }
        risk
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val heightmap = input.map { it.toMutableList() }
        val basins = mutableListOf<Int>()

        for (y in heightmap.indices) {
            val row = heightmap[y]
            for (x in row.indices) {
                val height = row[x]
                if (height == 9) continue

                var basinSize = 0
                val coordQueue = mutableListOf(x to y)
                while (coordQueue.isNotEmpty()) {
                    val c = coordQueue.removeFirst()
                    if ((heightmap.getOrNull(c.y)?.getOrNull(c.x) ?: 9) < 9) {
                        heightmap[c.y][c.x] = 9
                        basinSize++
                        coordQueue.add(c + (0 to 1))
                        coordQueue.add(c - (0 to 1))
                        coordQueue.add(c + (1 to 0))
                        coordQueue.add(c - (1 to 0))
                    }
                }
                basins.add(basinSize)
            }
        }

        basins.sortedDescending().take(3).reduce(Int::times)
    }
    println("Part 2: $answer2")
}
