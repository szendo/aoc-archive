package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/19.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .split("")
        .map { it.drop(1).map { r -> r.split(",").map { n -> n.toInt() }.let { c -> Coord3D(c[0], c[1], c[2]) } } }

    fun rotations(c: Coord3D): List<Coord3D> = listOf(
        Coord3D(+c.x, +c.y, +c.z), Coord3D(+c.z, +c.y, -c.x), Coord3D(-c.x, +c.y, -c.z), Coord3D(-c.z, +c.y, +c.x),
        Coord3D(+c.y, -c.x, +c.z), Coord3D(+c.z, -c.x, -c.y), Coord3D(-c.y, -c.x, -c.z), Coord3D(-c.z, -c.x, +c.y),
        Coord3D(-c.x, -c.y, +c.z), Coord3D(+c.z, -c.y, +c.x), Coord3D(+c.x, -c.y, -c.z), Coord3D(-c.z, -c.y, -c.x),
        Coord3D(-c.y, +c.x, +c.z), Coord3D(+c.z, +c.x, +c.y), Coord3D(+c.y, +c.x, -c.z), Coord3D(-c.z, +c.x, -c.y),
        Coord3D(+c.y, +c.z, +c.x), Coord3D(+c.x, +c.z, -c.y), Coord3D(-c.y, +c.z, -c.x), Coord3D(-c.x, +c.z, +c.y),
        Coord3D(+c.y, -c.z, -c.x), Coord3D(+c.x, -c.z, +c.y), Coord3D(-c.y, -c.z, +c.x), Coord3D(-c.x, -c.z, -c.y),
    )

    fun rotations(beacons: List<Coord3D>) =
        (0 until 24).map { i ->
            beacons.map { rotations(it)[i] }
        }

    val (scanners, beacons) = run {
        val unknownScanners = input.toMutableList()
        val knownScanners = mutableListOf(unknownScanners.removeFirst() to Coord3D(0, 0, 0))
        val queue = knownScanners.toMutableList()

        while (queue.isNotEmpty()) {
            val (refScanner, refOffset) = queue.removeFirst()
            val matchIndices = mutableListOf<Int>()
            for ((scannerIndex, scannerToCheck) in unknownScanners.withIndex()) {
                scanner@ for (rotatedScanner in rotations(scannerToCheck)) {
                    for (refCoord in refScanner) {
                        for (coord in rotatedScanner) {
                            val scannerOffset = refCoord - coord
                            val matchCount = rotatedScanner.count { c -> (c + scannerOffset) in refScanner }
                            if (matchCount >= 12) {
                                val match = rotatedScanner to (scannerOffset + refOffset)
                                knownScanners.add(match)
                                queue.add(match)
                                matchIndices.add(scannerIndex)
                                break@scanner
                            }
                        }
                    }
                }
            }
            matchIndices.sortedDescending().forEach { unknownScanners.removeAt(it) }
        }

        knownScanners to knownScanners.flatMap { (scanner, offset) ->
            scanner.map { it + offset }
        }.toSet()
    }

    val answer1 = run {
        beacons.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        scanners.maxOf { (_, offset1) ->
            scanners.maxOf { (_, offset2) ->
                offset1.distance(offset2)
            }
        }
    }
    println("Part 2: $answer2")
}
