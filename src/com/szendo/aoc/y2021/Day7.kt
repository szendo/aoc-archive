package com.szendo.aoc.y2021

import com.szendo.aoc.*
import kotlin.math.abs

fun main() {
    val input = Helper.getResourceAsStream("2021/7.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .split(',').map { it.toInt() }

    val minPos = input.minOf { it }
    val maxPos = input.maxOf { it }

    val answer1 = run {
        (minPos..maxPos).minOf { pos ->
            input.sumOf { abs(pos - it) }
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        (minPos..maxPos).minOf { pos ->
            input.sumOf {
                abs(pos - it).let { n -> n * (n + 1) / 2 }
            }
        }
    }
    println("Part 2: $answer2")
}
