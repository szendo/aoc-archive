package com.szendo.aoc.y2021

import com.szendo.aoc.*
import java.util.*
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.set
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

fun main() {
    val input = Helper.getResourceAsStream("2021/23.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .drop(2).dropLast(1)
        .map { it.substring(3..9).split('#').map { c -> c[0] } }
        .fold(List(4) { emptyList<Char>() }) { acc, line ->
            acc.zip(line).map { it.first + it.second }
        }

    data class State(val depth: Int, val rooms: List<List<Char>>, val hallway: Map<Int, Char>, val previous: State?) {

        fun isWinning() = rooms.withIndex().all { (i, r) ->
            hallway.isEmpty() && r.all { it - 'A' == i }
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as State

            if (rooms != other.rooms) return false
            if (hallway != other.hallway) return false

            return true
        }

        override fun hashCode(): Int {
            var result = rooms.hashCode()
            result = 31 * result + hallway.hashCode()
            return result
        }

        override fun toString(): String {
            return (previous?.toString()?.let { it + "\n" } ?: "") + "\n" +
                "".padEnd(2 * rooms.size + 5, '#') + "\n" +
                "#" + (0..(2 * rooms.size + 2)).joinToString("") { hallway[it]?.toString() ?: "." } + "#\n" +
                (0 until depth).joinToString("\n") { n ->
                    (if (n == 0) "###" else "  #") +
                        rooms.joinToString("") { r -> "${r.getOrElse(n - depth + r.size) { "." }}#" } +
                        (if (n == 0) "##" else "")
                } + "\n  " + "".padEnd(2 * rooms.size + 1, '#')
        }
    }

    fun checkSeen(seenStates: MutableMap<State, Int>, state: State, energy: Int): Boolean {
        if (state in seenStates) {
            val seenEnergy = seenStates.getValue(state)
            if (seenEnergy <= energy) {
                return false
            }
        }
        seenStates[state] = energy
        return true
    }

    val answer1 = run {
        val initialState = State(input[0].size, input, mapOf(), null)

        val seenStates = mutableMapOf<State, Int>()

        val queue = PriorityQueue<Pair<State, Int>>(Comparator.comparing { it.second })
        queue.add(initialState to 0)

        while (queue.isNotEmpty()) {
            val (state, energy) = queue.remove()
            val (depth, rooms, hallway) = state

            if (state.isWinning()) return@run "Energy: $energy\n$state"

            for ((hallwaySource, amphipod) in hallway) {
                val e = (0 until (amphipod - 'A')).fold(1) { acc, _ -> 10 * acc }

                val targetRoomIndex = amphipod - 'A'
                val hallwayBlocked =
                    (min(2 + 2 * targetRoomIndex, hallwaySource)..max(
                        2 + 2 * targetRoomIndex,
                        hallwaySource
                    )).any { i ->
                        i != hallwaySource && i in hallway
                    }

                if (!hallwayBlocked && rooms[targetRoomIndex].size < depth && rooms[targetRoomIndex].all { it == amphipod }) {
                    val newRooms = rooms.mapIndexed { i, r ->
                        when (i) {
                            targetRoomIndex -> listOf(amphipod) + r
                            else -> r
                        }
                    }

                    val newHallway = hallway - hallwaySource

                    val newState = State(depth, newRooms, newHallway, state)
                    val energyCost =
                        e * (abs(2 + 2 * targetRoomIndex - hallwaySource) + depth - rooms[targetRoomIndex].size)
                    if (checkSeen(seenStates, newState, energy + energyCost)) {
                        queue.add(newState to (energy + energyCost))
                    }
                }
            }

            for ((sourceRoomIndex, sourceRoom) in rooms.withIndex()) {
                val amphipod = sourceRoom.firstOrNull() ?: continue

                val e = (0 until (amphipod - 'A')).fold(1) { acc, _ -> 10 * acc }

                val targetRoomIndex = amphipod - 'A'

                if (sourceRoomIndex != targetRoomIndex) {
                    val hallwayBlocked =
                        ((2 + 2 * min(sourceRoomIndex, targetRoomIndex))..(2 + 2 * max(
                            sourceRoomIndex,
                            targetRoomIndex
                        ))).any { i ->
                            i in hallway
                        }

                    if (!hallwayBlocked && rooms[targetRoomIndex].size < depth && rooms[targetRoomIndex].all { it == amphipod }) {
                        val newRooms = rooms.mapIndexed { i, r ->
                            when (i) {
                                sourceRoomIndex -> r.minusElement(amphipod)
                                targetRoomIndex -> listOf(amphipod) + r
                                else -> r
                            }
                        }

                        val newState = State(depth, newRooms, hallway, state)
                        val energyCost =
                            e * (2 * abs(sourceRoomIndex - targetRoomIndex) + 2 * depth + 1 - sourceRoom.size - rooms[targetRoomIndex].size)
                        if (checkSeen(seenStates, newState, energy + energyCost)) {
                            queue.add(newState to (energy + energyCost))
                        }
                    }
                }

                for (hallwayTarget in 0..(rooms.size * 2 + 2)) {
                    if (hallwayTarget in setOf(2, 4, 6, 8)) continue

                    val hallwayBlocked =
                        (min(2 + 2 * sourceRoomIndex, hallwayTarget)..max(
                            2 + 2 * sourceRoomIndex,
                            hallwayTarget
                        )).any { i ->
                            i in hallway
                        }

                    if (hallwayBlocked) continue

                    val newRooms = rooms.mapIndexed { i, r ->
                        when (i) {
                            sourceRoomIndex -> r.minusElement(amphipod)
                            else -> r
                        }
                    }

                    val newHallway = hallway + (hallwayTarget to amphipod)

                    val newState = State(depth, newRooms, newHallway, state)
                    val energyCost = e * (abs(2 + 2 * sourceRoomIndex - hallwayTarget) + depth + 1 - sourceRoom.size)
                    if (checkSeen(seenStates, newState, energy + energyCost)) {
                        queue.add(newState to (energy + energyCost))
                    }
                }
            }
        }
    }
    println(answer1)

}
