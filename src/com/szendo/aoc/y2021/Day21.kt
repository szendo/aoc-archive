package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/21.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.split(": ")[1].toInt() }

    infix fun Int.mod(n: Int) = ((this - 1) % n) + 1

    val answer1 = run {
        val players = input.map { it to 0L }.toMutableList()

        var turn = 0
        var dice = 1
        while (players.all { it.second < 1000 }) {
            val roll = dice + ((dice + 1) mod 100) + ((dice + 2) mod 100)
            dice = (dice + 3) mod 100

            val (pos, score) = players[turn % 2]
            val newPos = (pos + roll) mod 10
            val newScore = score + newPos
            players[turn % 2] = newPos to newScore
            turn++
        }

        players.map { it.second }.first { it < 1000 } * turn * 3
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val quantumRolls = (1..3).flatMap { r1 -> (1..3).flatMap { r2 -> (1..3).map { r3 -> r1 + r2 + r3 } } }
            .groupingBy { it }.eachCount()
        val wins = mutableListOf(0L, 0L)
        var universes = mutableMapOf(input.map { it to 0L } to 0 to 1L)
        do {
            val newUniverses = mutableMapOf<Pair<List<Pair<Int, Long>>, Int>, Long>()
            for ((universe, universeCount) in universes) {
                val (players, turn) = universe

                val winner = players.indexOfFirst { it.second >= 21 }
                if (winner != -1) {
                    wins[winner] += universeCount
                    continue
                }

                quantumRolls.forEach { (roll, rollCount) ->
                    val (pos, score) = players[turn % 2]
                    val newPos = (pos + roll) mod 10
                    val newScore = score + newPos
                    val newUniverse = players.mapIndexed { i, player ->
                        if (i == turn % 2) newPos to newScore else player
                    } to turn + 1
                    newUniverses.merge(newUniverse, universeCount * rollCount, Long::plus)
                }
            }
            universes = newUniverses
        } while (universes.isNotEmpty())

        wins.maxOf { it }
    }
    println("Part 2: $answer2")
}
