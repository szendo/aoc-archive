package com.szendo.aoc.y2021

import com.szendo.aoc.*
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

fun main() {
    val input = Helper.getResourceAsStream("2021/23.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .drop(2).dropLast(1)
        .map { it.substring(3..9).split('#').map { c -> c[0] } }
        .fold(List(4) { emptyList<Char>() }) { acc, line ->
            acc.zip(line).map { it.first + it.second }
        }

    data class State(val depth: Int, val rooms: List<List<Char>>, val hallway: Map<Int, Char>) {
        fun isWinning() = rooms.withIndex().all { (i, r) -> r.size == depth && r.all { it - 'A' == i } }
    }

    fun calculateCost(initialState: State): Int {
        val seenStates = mutableMapOf<State, Int>()

        val queue = PriorityQueue<Pair<State, Int>>(Comparator.comparing { it.second })
        queue.add(initialState to 0)

        while (queue.isNotEmpty()) {
            val (state, energy) = queue.remove()
            val (depth, rooms, hallway) = state
            if (state in seenStates) {
                val seenEnergy = seenStates.getValue(state)
                if (seenEnergy <= energy) {
                    continue
                }
            }
            seenStates[state] = energy

            if (state.isWinning()) return energy

            for ((hallwaySource, amphipod) in hallway) {
                val e = when (amphipod) {
                    'A' -> 1
                    'B' -> 10
                    'C' -> 100
                    'D' -> 1000
                    else -> error("Unknown amphipod: $amphipod")
                }

                val targetRoomIndex = amphipod - 'A'
                val hallwayBlocked =
                    (min(2 + 2 * targetRoomIndex, hallwaySource)..max(2 + 2 * targetRoomIndex, hallwaySource)).any { i ->
                        i != hallwaySource && i in hallway
                    }

                if (!hallwayBlocked && rooms[targetRoomIndex].size < depth && rooms[targetRoomIndex].all { it == amphipod }) {
                    val newRooms = rooms.mapIndexed { i, r ->
                        when (i) {
                            targetRoomIndex -> listOf(amphipod) + r
                            else -> r
                        }
                    }

                    val newHallway = hallway - hallwaySource

                    val newState = State(depth, newRooms, newHallway)
                    val energyCost = e * (abs(2 + 2 * targetRoomIndex - hallwaySource) + depth - rooms[targetRoomIndex].size)
                    if (newState !in seenStates) {
                        queue.add(newState to (energy + energyCost))
                    }
                }
            }

            for ((sourceRoomIndex, sourceRoom) in rooms.withIndex()) {
                val amphipod = sourceRoom.firstOrNull() ?: continue

                val e = when (amphipod) {
                    'A' -> 1
                    'B' -> 10
                    'C' -> 100
                    'D' -> 1000
                    else -> error("Unknown amphipod: $amphipod")
                }

                val targetRoomIndex = amphipod - 'A'

                if (sourceRoomIndex != targetRoomIndex) {
                    val hallwayBlocked =
                        ((2 + 2 * min(sourceRoomIndex, targetRoomIndex))..(2 + 2 * max(sourceRoomIndex, targetRoomIndex))).any { i ->
                            i in hallway
                        }

                    if (!hallwayBlocked && rooms[targetRoomIndex].size < depth && rooms[targetRoomIndex].all { it == amphipod }) {
                        val newRooms = rooms.mapIndexed { i, r ->
                            when (i) {
                                sourceRoomIndex -> r.minusElement(amphipod)
                                targetRoomIndex -> listOf(amphipod) + r
                                else -> r
                            }
                        }

                        val newState = State(depth, newRooms, hallway)
                        val energyCost =
                            e * (2 * abs(sourceRoomIndex - targetRoomIndex) + 2 * depth + 1 - sourceRoom.size - rooms[targetRoomIndex].size)
                        if (newState !in seenStates) {
                            queue.add(newState to (energy + energyCost))
                        }
                    }
                }

                for (hallwayTarget in 0..10) {
                    if (hallwayTarget in setOf(2, 4, 6, 8)) continue

                    val hallwayBlocked =
                        (min(2 + 2 * sourceRoomIndex, hallwayTarget)..max(2 + 2 * sourceRoomIndex, hallwayTarget)).any { i ->
                            i in hallway
                        }

                    if (hallwayBlocked) continue

                    val newRooms = rooms.mapIndexed { i, r ->
                        when (i) {
                            sourceRoomIndex -> r.minusElement(amphipod)
                            else -> r
                        }
                    }

                    val newHallway = hallway + (hallwayTarget to amphipod)

                    val newState = State(depth, newRooms, newHallway)
                    val energyCost = e * (abs(2 + 2 * sourceRoomIndex - hallwayTarget) + depth + 1 - sourceRoom.size)
                    if (newState !in seenStates) {
                        queue.add(newState to (energy + energyCost))
                    }
                }
            }
        }

        error("Solution not found")
    }

    val answer1 = run {
        calculateCost(State(2, input, mapOf()))
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val realInput = input
            .zip(listOf(listOf('D', 'D'), listOf('C', 'B'), listOf('B', 'A'), listOf('A', 'C')))
            .map { it.first.take(1) + it.second + it.first.drop(1) }
        calculateCost(State(4, realInput, mapOf()))
    }
    println("Part 2: $answer2")
}
